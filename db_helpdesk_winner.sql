-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Dec 14, 2019 at 02:13 PM
-- Server version: 10.1.16-MariaDB
-- PHP Version: 7.0.9

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `db_helpdesk_winner`
--

-- --------------------------------------------------------

--
-- Table structure for table `biro`
--

CREATE TABLE `biro` (
  `id` int(11) NOT NULL,
  `nama_biro` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `biro`
--

INSERT INTO `biro` (`id`, `nama_biro`) VALUES
(1, 'QSHE'),
(2, 'HC & GA'),
(3, 'Pengadaan'),
(4, 'Keuangan');

-- --------------------------------------------------------

--
-- Table structure for table `kategori`
--

CREATE TABLE `kategori` (
  `id` int(11) NOT NULL,
  `nama_kategori` varchar(50) NOT NULL,
  `id_biro` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `kategori`
--

INSERT INTO `kategori` (`id`, `nama_kategori`, `id_biro`) VALUES
(1, 'AC', 2),
(2, 'Jaringan Internet', 1);

-- --------------------------------------------------------

--
-- Table structure for table `settings`
--

CREATE TABLE `settings` (
  `id` int(11) NOT NULL,
  `no_telp` varchar(20) NOT NULL,
  `no_broadcast` varchar(20) NOT NULL,
  `email_perusahaan` varchar(20) NOT NULL,
  `alamat_perusahaan` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `settings`
--

INSERT INTO `settings` (`id`, `no_telp`, `no_broadcast`, `email_perusahaan`, `alamat_perusahaan`) VALUES
(1, '(021) 86863293', '085648435363', 'info@wikaenergi.com', 'Bogor, Kompleks Industri WIKA, Jl. Raya Narogong No.Km. 26, Kembang Kuning, Kec. Klapanunggal, Bogor, Jawa Barat 16810');

-- --------------------------------------------------------

--
-- Table structure for table `staff`
--

CREATE TABLE `staff` (
  `id` int(11) NOT NULL,
  `nip` varchar(11) NOT NULL,
  `nm_peg` varchar(50) NOT NULL,
  `password` varchar(60) NOT NULL,
  `status` varchar(10) NOT NULL,
  `jns_kelamin_peg` enum('PRIA','WANITA') NOT NULL,
  `alamat` text NOT NULL,
  `kodepos` int(11) NOT NULL,
  `telepon` varchar(25) DEFAULT NULL,
  `handphone` varchar(25) NOT NULL,
  `email` varchar(25) NOT NULL,
  `kd_jabatan` int(11) NOT NULL,
  `nm_jabatan` text NOT NULL,
  `kd_unit_org` text NOT NULL,
  `nm_unit_org` text NOT NULL,
  `id_biro` int(11) NOT NULL,
  `admin` enum('yes','no') NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tickets`
--

CREATE TABLE `tickets` (
  `id` int(11) NOT NULL,
  `id_user` int(11) NOT NULL,
  `id_biro` int(11) NOT NULL,
  `id_kategori` int(11) NOT NULL,
  `tanggal_open` date NOT NULL,
  `tanggal_close` date DEFAULT NULL,
  `priority` enum('low','normal','urgent') NOT NULL,
  `status` enum('open','work_progress','closed','done') NOT NULL,
  `deleted_at` date DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tickets`
--

INSERT INTO `tickets` (`id`, `id_user`, `id_biro`, `id_kategori`, `tanggal_open`, `tanggal_close`, `priority`, `status`, `deleted_at`) VALUES
(1, 3, 1, 2, '2019-12-11', NULL, 'low', 'open', NULL),
(2, 2, 2, 1, '2019-12-11', NULL, 'low', 'work_progress', NULL),
(3, 2, 2, 1, '2019-12-12', NULL, 'low', 'open', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `ticket_message`
--

CREATE TABLE `ticket_message` (
  `id` int(11) NOT NULL,
  `id_ticket` int(11) NOT NULL,
  `pesan` text NOT NULL,
  `id_user` int(11) NOT NULL,
  `id_staff` int(11) DEFAULT NULL,
  `tanggal` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `ticket_message`
--

INSERT INTO `ticket_message` (`id`, `id_ticket`, `pesan`, `id_user`, `id_staff`, `tanggal`) VALUES
(1, 1, 'lemot woy', 3, NULL, '2019-12-11'),
(2, 2, 'ac mati', 2, NULL, '2019-12-11'),
(3, 3, 'ac kurang dingin', 2, NULL, '2019-12-12');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `nip` varchar(11) NOT NULL,
  `nm_peg` varchar(50) NOT NULL,
  `password` varchar(60) NOT NULL,
  `status` varchar(10) NOT NULL,
  `jns_kelamin_peg` enum('PRIA','WANITA') NOT NULL,
  `alamat` text NOT NULL,
  `kodepos` int(11) NOT NULL,
  `telepon` varchar(25) DEFAULT NULL,
  `handphone` varchar(25) NOT NULL,
  `email` varchar(25) NOT NULL,
  `kd_jabatan` int(11) NOT NULL,
  `nm_jabatan` text NOT NULL,
  `kd_unit_org` text NOT NULL,
  `nm_unit_org` text NOT NULL,
  `admin` enum('yes','no') NOT NULL,
  `superadmin` enum('yes','no') NOT NULL,
  `id_biro` int(11) DEFAULT NULL,
  `fcm_token` text
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `nip`, `nm_peg`, `password`, `status`, `jns_kelamin_peg`, `alamat`, `kodepos`, `telepon`, `handphone`, `email`, `kd_jabatan`, `nm_jabatan`, `kd_unit_org`, `nm_unit_org`, `admin`, `superadmin`, `id_biro`, `fcm_token`) VALUES
(1, '1', 'admin', 'e6e061838856bf47e1de730719fb2609', 'aktif', 'PRIA', 'winner', 1, '001', '001', 'admin@admin.com', 1, 'admin', 'admin', 'admin', 'yes', 'yes', 1, NULL),
(2, 'WIENO1905', 'AVRIYASENDY RAMADIYAN', '3811847612be8eb7cc9238c3861e9ff3', 'aktif', 'PRIA', 'JL.GANDARIA II RT.002/002 DESA PEKAYON , PASR REBO ', 0, '', '085640911007', 'sendy@wika.co.id', 609, 'KOORDINATOR', '200008009005003000000000000000', 'BAGIAN PENGEMBANGAN SISTEM WINNER (19)', 'yes', 'no', 1, NULL),
(3, 'WIENO1308', 'FAJARIN LAOLI', '3811847612be8eb7cc9238c3861e9ff3', 'aktif', 'PRIA', 'PERUMAHAN PESONA PRIMA KARANGGAN BLOK B6/10 RT/RW 005/004 KEL. KARANGGAN, KEC. GUNUNG PUTRI, BOGOR JAWA BARAT 16960', 16960, '', '081513142980', 'fajarin_laoli@wikaenergi.', 603, 'KEPALA SEKSI PROYEK KECIL', '200008011019003000000000000000', 'SEKSI ADMINISTRASI DAN KEUANGAN', 'yes', 'no', 2, NULL);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `biro`
--
ALTER TABLE `biro`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `kategori`
--
ALTER TABLE `kategori`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_biro` (`id_biro`);

--
-- Indexes for table `settings`
--
ALTER TABLE `settings`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `staff`
--
ALTER TABLE `staff`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_biro` (`id_biro`);

--
-- Indexes for table `tickets`
--
ALTER TABLE `tickets`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_user` (`id_user`),
  ADD KEY `id_biro` (`id_biro`),
  ADD KEY `id_kategori` (`id_kategori`);

--
-- Indexes for table `ticket_message`
--
ALTER TABLE `ticket_message`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_ticket` (`id_ticket`),
  ADD KEY `id_user` (`id_user`),
  ADD KEY `id_staff` (`id_staff`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_biro` (`id_biro`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `biro`
--
ALTER TABLE `biro`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `kategori`
--
ALTER TABLE `kategori`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `staff`
--
ALTER TABLE `staff`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tickets`
--
ALTER TABLE `tickets`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `ticket_message`
--
ALTER TABLE `ticket_message`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `kategori`
--
ALTER TABLE `kategori`
  ADD CONSTRAINT `kategori_ibfk_1` FOREIGN KEY (`id_biro`) REFERENCES `biro` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `staff`
--
ALTER TABLE `staff`
  ADD CONSTRAINT `staff_ibfk_1` FOREIGN KEY (`id_biro`) REFERENCES `biro` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `tickets`
--
ALTER TABLE `tickets`
  ADD CONSTRAINT `tickets_ibfk_1` FOREIGN KEY (`id_user`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `tickets_ibfk_2` FOREIGN KEY (`id_biro`) REFERENCES `biro` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `tickets_ibfk_3` FOREIGN KEY (`id_kategori`) REFERENCES `kategori` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

DELIMITER $$
--
-- Events
--
CREATE DEFINER=`root`@`localhost` EVENT `statusDone` ON SCHEDULE EVERY 1 HOUR STARTS '2019-10-10 16:02:59' ON COMPLETION NOT PRESERVE ENABLE DO update tickets set status="done" where tanggal_close < now() and status="closed"$$

CREATE DEFINER=`root`@`localhost` EVENT `statusdonemenit` ON SCHEDULE EVERY 1 SECOND STARTS '2019-10-10 16:07:32' ON COMPLETION NOT PRESERVE ENABLE DO update tickets set status="done" where tanggal_close < now() and status="closed"$$

CREATE DEFINER=`root`@`localhost` EVENT `statusdoneday` ON SCHEDULE EVERY 1 DAY STARTS '2019-10-10 16:17:09' ON COMPLETION NOT PRESERVE ENABLE DO update tickets 
set status="done" 
where tanggal_close < now() 
  and status="closed"$$

DELIMITER ;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
