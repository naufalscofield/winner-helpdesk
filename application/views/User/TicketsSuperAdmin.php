<style>
*,
*:before,
*:after {
    box-sizing: border-box;
}
.step { position: relative; min-height: 45px /* circle-size */ ; }
.step > div:first-child { position: static; height: 0; }
.step > div:last-child { margin-left: 45px ; padding-left: 22px; }
.circle-open { background: #4285f4; width: 45px; height: 45px; line-height: 45px; border-radius: 22px; position: relative; color: white; text-align: center; }
.circle-wp { background: green; width: 45px; height: 45px; line-height: 45px; border-radius: 22px; position: relative; color: white; text-align: center; }
.circle-closed { background: orange; width: 45px; height: 45px; line-height: 45px; border-radius: 22px; position: relative; color: white; text-align: center; }
.circle-done { background: red; width: 45px; height: 45px; line-height: 45px; border-radius: 22px; position: relative; color: white; text-align: center; }
.line { position: absolute; border-left: 5px solid gainsboro; left: 20px; bottom: 10px; top: 50px; }
.step:last-child .line { display: none; }
.title { line-height: 45px; font-weight: bold; }
</style>
<body>
  <!-- The core Firebase JS SDK is always required and must be listed first -->
<script src="https://www.gstatic.com/firebasejs/7.2.1/firebase-app.js"></script>

<!-- TODO: Add SDKs for Firebase products that you want to use
     https://firebase.google.com/docs/web/setup#available-libraries -->

<script>
  // Your web app's Firebase configuration
  var firebaseConfig = {
    apiKey: "AIzaSyB-xRLq_7ZIPJmxylwogqHX-1b9jYgZdsc",
    authDomain: "winnerhelpdesk-ce0ae.firebaseapp.com",
    databaseURL: "https://winnerhelpdesk-ce0ae.firebaseio.com",
    projectId: "winnerhelpdesk-ce0ae",
    storageBucket: "winnerhelpdesk-ce0ae.appspot.com",
    messagingSenderId: "828976051194",
    appId: "1:828976051194:web:46b21bcc938e95e99c6a43"
  };
  // Initialize Firebase
  firebase.initializeApp(firebaseConfig);
</script>
<div class="main-panel">
      <!-- Navbar -->
      <nav class="navbar navbar-expand-lg navbar-transparent navbar-absolute fixed-top ">
        <div class="container-fluid">
          <div class="navbar-wrapper">
 <li id="btn_back" class="fa fa-chevron-circle-left fa-2x"></li>
            <a class="navbar-brand" href="#pablo">Tickets</a>
          </div>
          <button class="navbar-toggler" type="button" data-toggle="collapse" aria-controls="navigation-index" aria-expanded="false" aria-label="Toggle navigation">
            <span class="sr-only">Toggle navigation</span>
            <span class="navbar-toggler-icon icon-bar"></span>
            <span class="navbar-toggler-icon icon-bar"></span>
            <span class="navbar-toggler-icon icon-bar"></span>
          </button>
          <div class="collapse navbar-collapse justify-content-end">
            <form class="navbar-form">
              <?= $this->session->userdata('nm_peg');?>
              <input type="hidden" id="id_biro_sess" value="<?= $this->session->userdata('id_biro');?>">
            </form> 
            <ul class="navbar-nav">
              <li class="nav-item dropdown">
                <a class="nav-link" href="#pablo" id="navbarDropdownProfile" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                  <i class="material-icons">person</i>
                  <p class="d-lg-none d-md-block">
                    Account
                  </p>
                </a>
                 <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdownProfile">
                  <a class="dropdown-item" href="<?=base_url();?>index.php/user/logout">Log out</a>
                  <a class="dropdown-item" href="<?=base_url();?>index.php/User/getProfile">Profil out</a>
                </div>
                <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdownProfile">
                  <a class="dropdown-item" href="<?=base_url();?>index.php/User/getProfile">Profil</a>
                </div>
              </li>
            </ul>
          </div>
        </div>
      </nav>
      <!-- End Navbar -->
      <div class="content">
        <div class="container-fluid">
          <div class="row">
            <div class="col-md-12">
              <div class="card">
                <div class="card-header card-header-primary">
                  <h4 class="card-title ">Daftar Ticket</h4>
                </div>
                    <div class="col-md-12">
                      <div class="form-group">
                        <select name="" class="form-control" id="myInput" onchange="myFunction()">
                          <option value="">-- Filter Status --</option>
                          <option value="open">Open</option>
                          <option value="work_progress">Work Progress</option>
                          <option value="closed">Closed</option>
                          <option value="done">Done</option>
                        </select>
                      </div>
                    </div>
                    <div class="col-md-12">
                      <div class="form-group">
                        <select name="" class="form-control" id="myInput2" onchange="myFunction2()">
                          <option value="">-- Filter Priority --</option>
                          <option value="low">Low</option>
                          <option value="normal">Normal</option>
                          <option value="urgent">Urgent</option>
                        </select>
                      </div>
                      <?php 
                      $i = $this->session->userdata('id_biro');
                      $q = $this->db->get_where('tickets',array('status' => 'open','id_biro' => $i, 'deleted_at' => null));
                      $notif = $q->num_rows();
                      ?>
                    <!--  -->
                    </div>
                <div class="card-body">
                  <div class="table-responsive">
                    <table id="example" class="table">
                      <thead class=" text-primary">
                        <tr>
                            <th width="5"><center><b>No</b></center></th>
                            <th><center><b>Pegawai</b></center></th>                  
                            <th><center><b>Sub Kategori</b></center></th>                   
                            <th><center><b>Priority</b></center></th>                  
                            <th><center><b>Tanggal Open</b></center></th>                  
                            <th><center><b>Tanggal Close</b></center></th>                  
                            <th><center><b>Status</b></center></th>                  
                            <th><center><b>Lama Jam Penanganan</b></center></th>
                        </tr>
                    </thead>
                    <tbody id="table-row">
                       
                    </tbody>
                    </table>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>

      <div id="stepperTicket" class="modal" tabindex="-1" role="dialog">
        <div class="modal-dialog modal-md" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Timeline Ticket</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                  <div class="step">
                      <div>
                          <div class="circle-open"><i class="fa fa-external-link"></i></div>
                          <div class="line"></div>
                      </div>
                      <div>
                          <div class="title">Ticket Open</div>
                          <div class="body" id="step_tgl_open"></div>
                          <div class="body" id="step_oleh_open"></div>
                      </div>
                  </div>
                  <div class="step">
                      <div>
                          <div class="circle-wp"><i class="fa fa-spinner"></i></div>
                          <div class="line"></div>
                      </div>
                      <div>
                          <div class="title">Ticket On Work Progress</div>
                          <div class="body" id="step_tgl_wp"></div>
                          <div class="body" id="step_oleh_wp"></div>
                      </div>
                  </div>
                  <div class="step">
                      <div>
                          <div class="circle-closed"><i class="fa fa-times-circle-o"></i></div>
                          <div class="line"></div>
                      </div>
                      <div>
                          <div class="title">Ticket Closed</div>
                          <div class="body" id="step_tgl_closed"></div>
                          <div class="body" id="step_oleh_closed"></div>
                      </div>
                  </div>
                  <div class="step">
                      <div>
                          <div class="circle-done"><i class="fa fa-check"></i></div>
                          <div class="line"></div>
                      </div>
                      <div>
                          <div class="title">Ticket Done</div>
                          <div class="body" id="step_tgl_done"></div>
                          <div class="body" id="step_oleh_done"></div>
                      </div>
                  </div>
                </div>
            </div>
        </div>
    </div>
      
      <div id="goReport" class="modal" tabindex="-1" role="dialog">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Feedback Report</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                  <div class="row">
                    <div class="col-md-12">
                        <div class="form-group">
                            <input type="hidden" id="id_ticket_report">
                            <label>Feedback Admin Biro Untuk Ticket Ini</label>
                            <textarea class="form-control" name="feedback" id="feedback" cols="30" rows="10" placeholder="Feedback / Suku Cadang Yang Diganti Diperbaiki"></textarea>
                        </div>
                      </div>
                    <div class="col-md-12">
                        <div class="form-group">
                            <label>Jumlah Barang / Suku Cadang Yang Diperbaiki</label>
                            <input type="text" class="form-control" id="diperbaiki">
                        </div>
                      </div>
                    <div class="col-md-12">
                        <div class="form-group">
                            <label>Jumlah Barang / Suku Cadang Yang Diganti</label>
                            <input type="text" class="form-control" id="diganti">
                        </div>
                      </div>
                    <div class="col-md-12">
                        <div class="form-group">
                            <label>Catatan</label>
                            <textarea class="form-control" name="catatan" id="catatan" cols="30" rows="10" placeholder="Catatan"></textarea>
                        </div>
                      </div>
                    </div>

                    <div class="clearfix"></div>
                      <div class="modal-footer">
                          <button id="btn_submitreport" type="" class="btn btn-success btn-fill btn-wd">
                              Buat Report
                          </button>
                          <button type="submit" class="btn btn-secondary" data-dismiss="modal">Tutup</button>
                          </div>
                      </div>
                  </div>
                </div>
            </div>
        </div>
    </div>
      
      <div id="detailTicket" class="modal" tabindex="-1" role="dialog">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Detail Ticket</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                  <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label>Pegawai Pengadu</label>
                            <input class="form-control" name="pegawai_detail" id="pegawai_detail"></input>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label>Sub Kategori</label>
                            <input class="form-control" name="sub_kategori_detail" id="sub_kategori_detail"></input>
                        </div>
                    </div>
                  </div>
                  <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label>No Inventaris</label>
                            <input class="form-control" name="no_inventaris_detail" id="no_inventaris_detail"></input>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label>Tanggal Open</label>
                            <input class="form-control" name="tanggal_open_detail" id="tanggal_open_detail"></input>
                        </div>
                    </div>
                  </div>
                  <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label>Tanggal Work Progress</label>
                            <input class="form-control" name="tanggal_wp_detail" id="tanggal_wp_detail"></input>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label>PIC Work Progress</label>
                            <input class="form-control" name="pic_wp_detail" id="pic_wp_detail"></input>
                        </div>
                    </div>
                  </div>
                  <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label>Tanggal Close</label>
                            <input class="form-control" name="tanggal_close_detail" id="tanggal_close_detail"></input>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label>PIC Close</label>
                            <input class="form-control" name="pic_close_detail" id="pic_close_detail"></input>
                        </div>
                    </div>
                  </div>
                  <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label>Tanggal Done</label>
                            <input class="form-control" name="tanggal_done_detail" id="tanggal_done_detail"></input>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label>PIC Done</label>
                            <input class="form-control" name="pic_done_detail" id="pic_done_detail"></input>
                        </div>
                    </div>
                  </div>
                  <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label>Selesai Dalam</label>
                            <input class="form-control" name="selesai_dalam_detail" id="selesai_dalam_detail"></input>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label>Priority</label>
                            <input class="form-control" name="priority_detail" id="priority_detail"></input>
                        </div>
                    </div>
                  </div>
                  <div class="row">
                    <div class="col-md-12">
                        <div class="form-group">
                            <label>Status</label>
                            <input class="form-control" name="status_detail" id="status_detail"></input>
                        </div>
                    </div>
                  </div>
                    

                    <div class="clearfix"></div>
                      <div class="modal-footer">
                          <button type="submit" class="btn btn-secondary" data-dismiss="modal">Tutup</button>
                      </div>
                    </div>
                </div>
              </div>
          </div>
      </div>
  </div>

      <div id="ticketMessage" class="modal" tabindex="-1" role="dialog">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                <div class="modal-header">
                <h5 class="modal-title">Ticket Message</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
                    <div class="col-md-12">
                                <div class="form-group" id="modal_body">
                <div class>
                        <div class="row">
                          <input type="hidden" name="id_user" id="id_user" value="<?= $this->session->userdata('id');?>">
                          <input type="hidden" name="id_ticket_input" id="id_ticket_input" value="<?= $this->session->userdata('id');?>">
                            <div class="col-md-12">
                                <div class="form-group" id="body_modal">
                                 </div>
                            </div>
                            
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label>Pesan</label>
                                    <textarea class="form-control border-input" name="pesan" id="pesan" cols="30" rows="10"></textarea>
                                 </div>
                            </div>
                        </div>
                    
                    <div class="clearfix"></div>
                        <div class="modal-footer">
                            <button id="btn_submit" type="" class="btn btn-success btn-fill btn-wd">
                                Kirim Pesan
                            </button>
                            <button type="submit" class="btn btn-secondary" data-dismiss="modal">Tutup</button>
                            </div>
                        </div>
                </div>
            </div>
        </div>
        </body>
        <script>
            function myFunction() {
            // Declare variables
            var input, filter, table, tr, td, i, txtValue;
            input = document.getElementById("myInput");
            filter = input.value.toUpperCase();
            table = document.getElementById("example");
            tr = table.getElementsByTagName("tr");

            // Loop through all table rows, and hide those who don't match the search query
                for (i = 0; i < tr.length; i++) {
                    td = tr[i].getElementsByTagName("td")[8];
                    if (td) {
                    txtValue = td.textContent || td.innerText;
                    if (txtValue.toUpperCase().indexOf(filter) > -1) {
                        tr[i].style.display = "";
                    } else {
                        tr[i].style.display = "none";
                        }
                    }
                }
            }
            function myFunction2() {  
            // Declare variables
            var input, filter, table, tr, td, i, txtValue;
            input = document.getElementById("myInput2");
            filter = input.value.toUpperCase();
            table = document.getElementById("example");
            tr = table.getElementsByTagName("tr");

            // Loop through all table rows, and hide those who don't match the search query
                for (i = 0; i < tr.length; i++) {
                    td = tr[i].getElementsByTagName("td")[4];
                    if (td) {
                    txtValue = td.textContent || td.innerText;
                    if (txtValue.toUpperCase().indexOf(filter) > -1) {
                        tr[i].style.display = "";
                    } else {
                        tr[i].style.display = "none";
                        }
                    }
                }
            }
        </script>
      <script>
      function getTickets() {
        $('#sopen').val('a')
          var id_biro = $('#id_biro_sess').val();
          console.log(id_biro)
            $.get("http://localhost/winnerhelpdesk/index.php/WEB/TicketsSuperAdmin/", function(data, status){
                console.log(data, status)
                if (status) {
                    $('#table-row').html("")
                    let no = 0;
                    data.forEach(element => {
                    var t = $('#example').DataTable();
                    no++;
                    if (element.tanggal_close == null) {
                      var tgl_close = '-'
                    } else {
                      var tgl_close = element.tanggal_close
                    }

                    if (element.tanggal_wp == null) {
                      var tgl_wp = '-'
                    } else {
                      var tgl_wp = element.tanggal_wp
                    }

                    if (element.tanggal_done == null) {
                      var tgl_done = '-'
                    } else {
                      var tgl_done = element.tanggal_done
                    }

                    if (element.selesai_dalam == null) {
                      var selesai_dalam = '-'
                    } else {
                      var selesai_dalam = element.selesai_dalam + ' Jam'
                    }

                    if (element.priority == 'low') {
                        var priority = "<button class='btn btn-sm btn-success'>LOW</button></center>"
                    } else if (element.priority == 'normal') {
                        var priority = "<button class='btn btn-sm btn-warning'>NORMAL</button></center>"
                    } else {
                        var priority = "<button class='btn btn-sm btn-danger'>URGENT</button></center>"
                    }

                    if(element.status_ticket == 'open') {
                      t.row.add( [
                          "<center>"+no+"</center>",
                          "<center>"+element.nm_peg+"</center>",
                          "<center>"+element.nama_sub_kategori+"</center>",
                          "<center>"+priority+"</center>",
                          "<center>"+moment(element.tanggal_open).format('ddd, DD MMMM YYYY, h:mm:ss a')+"</center>",
                          "<center>"+tgl_close+"</center>",
                          "<center><button data-toggle='tooltip' data-placement='top' title='Ticket Ini Berstatus Open' class='btn btn-sm btn-info'>Open</button></center>",
                          "<center>"+selesai_dalam+"</center>",
                          // "<center><button data-toggle='tooltip' data-placement='top' title='Ubah Status Ticket Ini Ke Work Progress' id='btn_wp' data-idwp="+element.id_ticket+" class='btn btn-sm btn-success'><i class='fa fa-spinner'></i></button><button data-toggle='tooltip' data-placement='top' title='Lihat Daftar Pesan Ticket Ini' id='btn_message' data-idmessage="+element.id_ticket+" class='btn btn-sm btn-primary' data-target='#ticketMessage'><i class='fa fa-comments-o'></i></button><button data-toggle='tooltip' data-placement='top' title='Lihat Timeline Ticket Ini' id='btn_stepper' data-idstepper=" + element.id_ticket + " class='btn btn-sm btn-warning' data-target='#stepperTicket'><i class='fa fa-history'></i></button><button data-toggle='tooltip' data-placement='top' title='Detail Ticket' id='btn_detail' data-iddetail=" + element.id_ticket + " class='btn btn-sm btn-danger' data-target='#detailTicket'><i class='fa fa-eye'></i></button>"
                      ] ).draw( false );
                      } else if (element.status_ticket == 'work_progress') {
                        t.row.add( [
                          "<center>"+no+"</center>",
                          "<center>"+element.nm_peg+"</center>",
                          "<center>"+element.nama_sub_kategori+"</center>",
                          "<center>"+priority+"</center>",
                          "<center>"+moment(element.tanggal_open).format('ddd, DD MMMM YYYY, h:mm:ss a')+"</center>",
                          "<center>"+tgl_close+"</center>",
                          "<center><button data-toggle='tooltip' data-placement='top' title='Ticket Ini Berstatus Work Progress' class='btn btn-sm btn-success'>Work Progress</button></center>",
                          "<center>"+selesai_dalam+"</center>",
                          // "<center><button data-toggle='tooltip' data-placement='top' title='Ubah Status Ticket Ini Ke Closed' id='btn_closed' data-idclosed="+element.id_ticket+" class='btn btn-sm btn-warning'><i class='fa fa-window-close'></i></button><button id='btn_message' data-toggle='tooltip' data-placement='top' title='Lihat Daftar Pesan Ticket Ini' data-idmessage="+element.id_ticket+" class='btn btn-sm btn-primary' data-target='#ticketMessage'><i class='fa fa-comments-o'></i></button><button id='btn_stepper' data-toggle='tooltip' data-placement='top' title='Lihat Timeline Ticket Ini' data-idstepper=" + element.id_ticket + " class='btn btn-sm btn-warning' data-target='#stepperTicket'><i class='fa fa-history'></i></button><button data-toggle='tooltip' data-placement='top' title='Detail Ticket' id='btn_detail' data-iddetail=" + element.id_ticket + " class='btn btn-sm btn-danger' data-target='#detailTicket'><i class='fa fa-eye'></i></button>"
                        ] ).draw( false );
                      } else if (element.status_ticket == 'closed') { 
                        t.row.add( [
                          "<center>"+no+"</center>",
                          "<center>"+element.nm_peg+"</center>",
                          "<center>"+element.nama_sub_kategori+"</center>",
                          "<center>"+priority+"</center>",
                          "<center>"+moment(element.tanggal_open).format('ddd, DD MMMM YYYY, h:mm:ss a')+"</center>",
                          "<center>"+moment(element.tanggal_close).format('ddd, DD MMMM YYYY, h:mm:ss a')+"</center>",
                          "<center><button data-toggle='tooltip' data-placement='top' title='Ticket Ini Berstatus Closed' class='btn btn-sm btn-warning'>Closed</button></center>",
                          "<center>"+selesai_dalam+"</center>",
                          // "<center><button id='btn_message' data-toggle='tooltip' data-placement='top' title='Lihat Daftar Pesan Ticket Ini' data-idmessage="+element.id_ticket+" class='btn btn-sm btn-primary' data-target='#ticketMessage'><i class='fa fa-comments-o'></i></button><button id='btn_stepper' data-toggle='tooltip' data-placement='top' title='Lihat Timeline Ticket Ini' data-idstepper=" + element.id_ticket + " class='btn btn-sm btn-warning' data-target='#stepperTicket'><i class='fa fa-history'></i></button><button data-toggle='tooltip' data-placement='top' title='Detail Ticket' id='btn_detail' data-iddetail=" + element.id_ticket + " class='btn btn-sm btn-danger' data-target='#detailTicket'><i class='fa fa-eye'></i></button>"
                      ] ).draw( false );
                    } else { 
                      if (element.id_kategori ==  1) {
                        t.row.add( [
                          "<center>"+no+"</center>",
                          "<center>"+element.nm_peg+"</center>",
                          "<center>"+element.nama_sub_kategori+"</center>",
                          "<center>"+priority+"</center>",
                          "<center>"+moment(element.tanggal_open).format('ddd, DD MMMM YYYY, h:mm:ss a')+"</center>",
                          "<center>"+moment(element.tanggal_close).format('ddd, DD MMMM YYYY, h:mm:ss a')+"</center>",
                          "<center><button class='btn btn-sm btn-danger'>Done</button></center>",
                          "<center>"+selesai_dalam+"</center>",
                          // "<center><button data-toggle='tooltip' data-placement='top' title='Hapus Ticket Ini' id='btn_delete' data-id="+element.id_ticket+" class='btn btn-sm btn-dark'><i class='fa fa-trash'></i></><button data-toggle='tooltip' data-placement='top' title='Lihat Timeline Ticket Ini' id='btn_stepper' data-idstepper=" + element.id_ticket + " class='btn btn-sm btn-warning' data-target='#stepperTicket'><i class='fa fa-history'></i></button><button data-toggle='tooltip' data-placement='top' title='Buat Report Ticket Ini' id='btn_report' data-idreport=" + element.id_ticket + " class='btn btn-sm btn-primary' data-target='#goReport'><i class='fa fa-file-text'></i></button><button data-toggle='tooltip' data-placement='top' title='Detail Ticket' id='btn_detail' data-iddetail=" + element.id_ticket + " class='btn btn-sm btn-danger' data-target='#detailTicket'><i class='fa fa-eye'></i></button>"
                      ] ).draw( false );
                      } else {
                        t.row.add( [
                          "<center>"+no+"</center>",
                          "<center>"+element.nm_peg+"</center>",
                          "<center>"+element.nama_sub_kategori+"</center>",
                          "<center>"+priority+"</center>",
                          "<center>"+moment(element.tanggal_open).format('ddd, DD MMMM YYYY, h:mm:ss a')+"</center>",
                          "<center>"+moment(element.tanggal_close).format('ddd, DD MMMM YYYY, h:mm:ss a')+"</center>",
                          "<center><button class='btn btn-sm btn-danger'>Done</button></center>",
                          "<center>"+selesai_dalam+"</center>",
                          // "<center><button data-toggle='tooltip' data-placement='top' title='Hapus Ticket Ini' id='btn_delete' data-id="+element.id_ticket+" class='btn btn-sm btn-dark'><i class='fa fa-trash'></i></><button data-toggle='tooltip' data-placement='top' title='Lihat Timeline Ticket Ini' id='btn_stepper' data-idstepper=" + element.id_ticket + " class='btn btn-sm btn-warning' data-target='#stepperTicket'><i class='fa fa-history'></i></button><button data-toggle='tooltip' data-placement='top' title='Detail Ticket' id='btn_detail' data-iddetail=" + element.id_ticket + " class='btn btn-sm btn-danger' data-target='#detailTicket'><i class='fa fa-eye'></i></button>"
                      ] ).draw( false );

                    }
                    }
                      
                    });
                
                } else {
                    alert('Load Data Failed')
                }
            });
        }

            function clearTable() {
                var table = $('#example').DataTable();
                table
                    .clear()
                    .draw();
            }
            
            function clearModalMessage(){
                // $('#nama_Tickets_u').val('');
            }


   ////////////////////////////////////document ready/////////////////////////////////////////////////////////////     
        $(document).ready(function() {
            $('body').tooltip({
                selector: '[data-toggle=tooltip]'
            });

            $(document).on('click', '#btn_report', function(){
              $('#goReport').modal('show')
              var id_ticket_report = $(this).data('idreport')
              $('#id_ticket_report').val(id_ticket_report)
              // console.log('tes')
            });

            $(document).on('click', '#btn_detail', function(){
              $('#detailTicket').modal('show')
              var id_ticket = $(this).data('iddetail')
              $('#id_ticket_detail').val(id_ticket)

              $.get("http://localhost/winnerhelpdesk/index.php/WEB/DetailTicket/" + id_ticket, function(data, status){
                  console.log(data, status)
                  if (status){
                    data.forEach(element => {
                    if (element.tanggal_open == null){
                      var tgl_open = '-'
                    } else {
                      var tgl_open = moment(element.tanggal_open).format('ddd, DD MMMM YYYY, h:mm:ss a')
                    }

                    if (element.tanggal_wp == null){
                      var tgl_wp = '-'
                    } else {
                      var tgl_wp = moment(element.tanggal_wp).format('ddd, DD MMMM YYYY, h:mm:ss a')
                    }

                    if (element.tanggal_close == null){
                      var tgl_close = '-'
                    } else {
                      var tgl_close = moment(element.tanggal_close).format('ddd, DD MMMM YYYY, h:mm:ss a')
                    }

                    if (element.tanggal_done == null){
                      var tgl_done = '-'
                    } else {
                      var tgl_done = moment(element.tanggal_done).format('ddd, DD MMMM YYYY, h:mm:ss a')
                    }

                      $('#pegawai_detail').val(element.pic_open)
                      $('#sub_kategori_detail').val(element.nama_sub_kategori)
                      $('#no_inventaris_detail').val(element.no_inventaris)
                      $('#tanggal_open_detail').val(tgl_open)
                      $('#tanggal_wp_detail').val(tgl_wp)
                      $('#pic_wp_detail').val(element.pic_wp)
                      $('#tanggal_close_detail').val(tgl_close)
                      $('#pic_close_detail').val(element.pic_close)
                      $('#tanggal_done_detail').val(tgl_done)
                      $('#pic_done_detail').val(element.pic_done)
                      $('#selesai_dalam_detail').val(element.selesai_dalam)
                      $('#priority_detail').val(element.priority)
                      $('#status_detail').val(element.status_ticket)

                    })
                  }
              });

              // console.log('tes')
            });

            $(document).on('click', '#btn_submitreport', function(){
              var id = $('#id_ticket_report').val()
              var feedback = $('#feedback').val()
              var diperbaiki = $('#diperbaiki').val()
              var diganti = $('#diganti').val()
              var catatan = $('#catatan').val()
              $.post("http://localhost/winnerhelpdesk/index.php/WEB/ReportTicket", {
                    id                     : id,
                    feedback               : feedback,
                    diperbaiki               : diperbaiki,
                    diganti               : diganti,
                    catatan               : catatan,
                    
                    },
                    function(data, status){
                    if (data.status == true) {
                      toastr.options = {
                        "closeButton": false,
                        "debug": false,
                        "newestOnTop": false,
                        "progressBar": false,
                        "positionClass": "toast-top-full-width",
                        "preventDuplicates": false,
                        "onclick": null,
                        "showDuration": "300",
                        "hideDuration": "1000",
                        "timeOut": "5000",
                        "extendedTimeOut": "1000",
                        "showEasing": "swing",
                        "hideEasing": "linear",
                        "showMethod": "fadeIn",
                        "hideMethod": "fadeOut"
                      }
                        toastr.success(data.message)
                        // clearTable()
                        $('#goReport').modal('hide')
                        $('#id_ticket_report').val('')
                        $('#feedback').val('')
                        $('#diganti').val('')
                        $('#diperbaiki').val('')
                        $('#catatan').val('')
                        // clearModa
                    }
                    else {
                        toastr.error('Reports Failed!')
                    }
                });
            });
          
          
          $(document).on('click', '#btn_back', function(){
             window.history.back();
            })

            $(document).on('click', '#btn_stepper', function() {
              $('#stepperTicket').modal('show')
              var id = $(this).data("idstepper")
              // console.log('tes',id)
              $.get("http://localhost/winnerhelpdesk/index.php/WEB/Stepper/" + id, function(data, status) {
                if (status) {
                console.log(data)
                  data.forEach(element => {
                    if(element.statusticket == "open"){
                    // $('#step_check_open').append("<i class='fa fa-check'></i>")
                    var tgl_open_m = moment(element.tanggal_open).format('ddd, DD MMMM YYYY, h:mm:ss a')
                    $('#step_tgl_open').html(tgl_open_m)
                    $('#step_oleh_open').html(element.pic_open)
                    
                    $('#step_tgl_wp').html('-')
                    $('#step_oleh_wp').html('-')

                    $('#step_tgl_closed').html('-')
                    $('#step_oleh_closed').html('-')

                    $('#step_tgl_done').html('-')
                    $('#step_oleh_done').html('-')
                    
                    } else if(element.statusticket == "work_progress") {
                      
                      var tgl_open_m = moment(element.tanggal_open).format('ddd, DD MMMM YYYY, h:mm:ss a')
                      $('#step_tgl_open').html(tgl_open_m)
                      $('#step_oleh_open').html(element.pic_open)
                      
                      var tgl_wp_m = moment(element.tanggal_wp).format('ddd, DD MMMM YYYY, h:mm:ss a')
                      $('#step_tgl_wp').html(tgl_wp_m)
                      $('#step_oleh_wp').html(element.pic_wp)

                      $('#step_tgl_closed').html('-')
                      $('#step_oleh_closed').html('-')

                      $('#step_tgl_done').html('-')
                      $('#step_oleh_done').html('-')

                    } else if(element.statusticket == "closed") {
                      var tgl_open_m = moment(element.tanggal_open).format('ddd, DD MMMM YYYY, h:mm:ss a')
                      $('#step_tgl_open').html(tgl_open_m)
                      $('#step_oleh_open').html(element.pic_open)
                      
                      var tgl_wp_m = moment(element.tanggal_wp).format('ddd, DD MMMM YYYY, h:mm:ss a')
                      $('#step_tgl_wp').html(tgl_wp_m)
                      $('#step_oleh_wp').html(element.pic_wp)

                      var tgl_close_m = moment(element.tanggal_close).format('ddd, DD MMMM YYYY, h:mm:ss a')
                      $('#step_tgl_closed').html(tgl_close_m)
                      $('#step_oleh_closed').html(element.pic_closed)

                      $('#step_tgl_done').html('-')
                      $('#step_oleh_done').html('-')
                    } else {

                      var tgl_open_m = moment(element.tanggal_open).format('ddd, DD MMMM YYYY, h:mm:ss a')
                      $('#step_tgl_open').html(tgl_open_m)
                      $('#step_oleh_open').html(element.pic_open)
                      
                      var tgl_wp_m = moment(element.tanggal_wp).format('ddd, DD MMMM YYYY, h:mm:ss a')
                      $('#step_tgl_wp').html(tgl_wp_m)
                      $('#step_oleh_wp').html(element.pic_wp)

                      
                      var tgl_close_m = moment(element.tanggal_close).format('ddd, DD MMMM YYYY, h:mm:ss a')
                      $('#step_tgl_closed').html(tgl_close_m)
                      $('#step_oleh_closed').html(element.pic_closed)

                      var tgl_done_m = moment(element.tanggal_done).format('ddd, DD MMMM YYYY, h:mm:ss a')
                      $('#step_tgl_done').html(tgl_done_m)
                      $('#step_oleh_done').html(element.pic_done)
                    }
                  });
                } else {
                    alert('Load Data Failed')
                }
              });
})

            $('#example').DataTable( {
                dom: 'lBfrtip',
                buttons: [
                    'copy', 'csv', 'excel', 'pdf', 'print'
                ],
                "columnDefs": [
                  { "width": "30%", "targets": 7 },
                  // { "width": "20%", "targets": 1 }
                ]
            } );

            getTickets()

        } );

            $(document).on('click', '#btn_submit' ,function(){
                var id_user                   = $('#id_user').val();
                var pesan                      = $('#pesan').val();
                var id_ticket                  = $('#id_ticket_input').val();

            
                $.post("http://localhost/winnerhelpdesk/index.php/WEB/Message", {
                    id_user                   : id_user,
                    pesan                      : pesan,
                    id_ticket                  : id_ticket,
                    },
                    function(data, status){
                    if (status) {
                        toastr.success('Submit Pesan Success')
                        clearTable()
                        $('#ticketMessage').modal('hide')
                        // clearModalAdd()
                        getTickets()
                    }
                    else {
                        toastr.error('Submit Pesan Failed!')
                    }
                });
                
            });

            $(document).on('click', '#btn_message' ,function(){
                  $('#ticketMessage').modal('show')
                  var $el = $("#body_modal");
                  $el.empty(); // remove old options
                // clearModalMessage()
                var id =  $(this).data("idmessage");
                $("#id_ticket_input").val(id)
                $.ajax({
                type: "POST",
                data: { id_ticket: id },
                url: '<?php echo base_url()."index.php/users/getMessage" ?>',
                dataType: 'text',
                success: function(resp) {
                  console.log(resp)
                  var json = JSON.parse(resp.replace(',.', ''))
                  var $el = $("#body_modal");
                  // $el.empty(); // remove old options
                  $.each(json, function(key, value) {
                    if (value.nama_user == null) {
                    $el.append($("<textarea readonly class='form-control border-input'></textarea><br>").text(value.pesan + ' //' + value.nama_staff+ '-'+ moment(value.tanggal).format('ddd, DD MMMM YYYY, h:mm:ss a')));
                    } else {
                    $el.append($("<textarea readonly class='form-control border-input'></textarea><br>").text(value.pesan + ' //' + value.nama_user+ '-'+ moment(value.tanggal).format('ddd, DD MMMM YYYY, h:mm:ss a')));
                    }
                  });
                },
                error: function (jqXHR, exception) {
                  console.log(jqXHR, exception)
                }
              });
            });

            $(document).on('click', '#btn_wp' ,function(){
                var id =  $(this).data("idwp");
                console.log(id)
                var data = {
                    status : 'work_progress',
                    }

                jQuery.ajax({
                url: 'http://localhost/winnerhelpdesk/index.php/WEB/Tickets/' + id,
                type: 'PUT',
                data : data,
                success: function(data) {
                    clearTable()  
                    getTickets();
                    toastr.success('Update status ticket success!')
                    location.reload();
                    }
                });

            });

            $(document).on('click', '#btn_closed' ,function(){
                var id =  $(this).data("idclosed");
                console.log(id)
                var data = {
                    status : 'closed',
                    }

                jQuery.ajax({
                url: 'http://localhost/winnerhelpdesk/index.php/WEB/Tickets/' + id,
                type: 'PUT',
                data : data,
                success: function(data) {
                    clearTable()  
                    getTickets();
                    toastr.success('Update status ticket success!')
                    location.reload();
                    }
                });

            });

            $(document).on('click', '#btn_delete' ,function(){
                var id =  $(this).data("id");
                console.log(id)
                var data = {
                    status : 'deleted',
                    }

                jQuery.ajax({
                url: 'http://localhost/winnerhelpdesk/index.php/WEB/Tickets/' + id,
                type: 'PUT',
                data : data,
                success: function(data) {
                    clearTable()  
                    getTickets();
                    toastr.success('Delete ticket success!')
                    location.reload();
                    }
                });

            });
            // $(document).on('click', '#btn_status' ,function(){
            //     var id =  $(this).data("idstatus");
            //     console.log(id)
            //     var data = {
            //         status : closed,
            //         }

            //     jQuery.ajax({
            //     url: 'http://localhost/winnerhelpdesk/index.php/WEB/Tickets/' + id,
            //     type: 'PUT',
            //     data : data,
            //     success: function(data) {
            //         $('.modal').modal('hide')
            //         clearModalMessage()
            //         clearTable()
            //         getTickets();
            //         toastr.success('Update Data Success!')
            //         }
            //     });

            // });
           

        
      </script>