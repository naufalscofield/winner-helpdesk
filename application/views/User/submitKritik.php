<style>
 #toast-container{
     position: absolute;
     width: 300px;
   }
   .toast-top-center,.toast-bottom-center{
    left:50%;
    margin-left: -150px;
   }
   .toast-top-full-width,.toast-bottom-full-width{
    width: 100%!important;
   }
</style>
<div class="main-panel">
      <!-- Navbar -->
      <nav class="navbar navbar-expand-lg navbar-transparent navbar-absolute fixed-top ">
        <div class="container-fluid">
          <div class="navbar-wrapper">
 <li id="btn_back" class="fa fa-chevron-circle-left fa-2x"></li>
            <a class="navbar-brand" href="#pablo">Kritik</a>
          </div>
          <button class="navbar-toggler" type="button" data-toggle="collapse" aria-controls="navigation-index" aria-expanded="false" aria-label="Toggle navigation">
            <span class="sr-only">Toggle navigation</span>
            <span class="navbar-toggler-icon icon-bar"></span>
            <span class="navbar-toggler-icon icon-bar"></span>
            <span class="navbar-toggler-icon icon-bar"></span>
          </button>
          <div class="collapse navbar-collapse justify-content-end">
            <form class="navbar-form">
              <?= $this->session->userdata('nm_peg');?>
            </form>
            <ul class="navbar-nav">
              <li class="nav-item dropdown">
                <a class="nav-link" href="#pablo" id="navbarDropdownProfile" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                  <i class="material-icons">person</i>
                  <p class="d-lg-none d-md-block">
                    Account
                  </p>
                </a>
                 <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdownProfile">
                  <a class="dropdown-item" href="<?=base_url();?>index.php/user/logout">Log out</a>
                  <a class="dropdown-item" href="<?=base_url();?>index.php/User/getProfile">Profil out</a>
                </div>
                <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdownProfile">
                  <a class="dropdown-item" href="<?=base_url();?>index.php/User/getProfile">Profil</a>
                </div>
              </li>
            </ul>
          </div>
        </div>
      </nav>
<div class="content">
  <div class="container-fluid">
    <div class="row">
      <div class="col-md-12">
        <div class="card">
          <div class="card-header card-header-primary">
            <h4 class="card-title">Kritik Biro</h4>
          </div>
          <div class="card-body">
              <div class="row">
                <div class="col-md-12">
                  <div class="form-group">
                    <label class="bmd-label-floating">Biro Tujuan</label>
                    <select class="form-control border-input" name="id_biro_tujuan" id="id_biro_tujuan">
                      <option value="">-- Pilih Biro Tujuan --</option>
                      <?php
                      $q = $this->db->get('biro')->result_array();
                      foreach ($q as $q) { ?>
                        <option value="<?= $q['id'];?>"><?= $q['nama_biro'];?></option>
                      <?php } ?>
                    </select>
                    <input type="hidden" value="<?= $this->session->userdata('id');?>" id="id_user">
                  </div>
                </div>
                <div class="col-md-12">
                  <div class="form-group">
                    <label class="bmd-label-floating">Kritik / Pesan</label>
                        <textarea name="pesan" class="form-control border-input" id="pesan" cols="30" rows="10"></textarea>
                  </div>
                </div>
              </div>
              <center><button id="btn_submit" type="click" class="btn btn-primary pull-center">Submit Kritik</button></center>
              <div class="clearfix"></div>
          </div>
        </div>
      </div>
<script>
$(document).on('click', '#btn_submit' ,function(){
  var id_user                  = $("#id_user").val();
  var id_biro_tujuan           = $("#id_biro_tujuan").val();
  var pesan                    = $("#pesan").val();

  if ( (id_biro_tujuan == '') || (pesan == '') ){
    toastr.error("Harap Lengkapi data")
  } else {
    var nama_biro                 = $('#nama_biro').val();
    // console.log('tes')

    $.post("http://localhost/winnerhelpdesk/index.php/WEB/Kritik", {
        id_user                 : id_user,
        id_biro_tujuan          : id_biro_tujuan,
        pesan                   : pesan,
        },
        function(data, status){
          console.log(data, status)
        if (data.status) {
          console.log('sukses')
          // toastr.success('Anda mengirimkan kritik bernada '+data.sentiment)
          toastr.options = {
            "closeButton": true,
            "debug": false,
            "newestOnTop": false,
            "progressBar": false,
            "positionClass": "toast-top-full-width",
            "preventDuplicates": false,
            "onclick": null,
            "showDuration": "30000",
            "hideDuration": "20000",
            "timeOut": "5000",
            "extendedTimeOut": "1000",
            "showEasing": "swing",
            "hideEasing": "linear",
            "showMethod": "fadeIn",
            "hideMethod": "fadeOut"
          }
          Command: toastr["success"]("Dengan Skor Sentiment Sebesar "+data.skor, "Anda Telah Mengirimkan Kritik Bernada "+data.sentiment)

          $('#id_biro_tujuan').val('')
          $('#pesan').val('')
        }
        else {
          console.log('gagal')
            toastr.error('Gagal submit kritik')
        }
    });
  }
});
</script>