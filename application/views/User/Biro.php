<div class="main-panel">
      <!-- Navbar -->
      <nav class="navbar navbar-expand-lg navbar-transparent navbar-absolute fixed-top ">
        <div class="container-fluid">
          <div class="navbar-wrapper">
 <li id="btn_back" class="fa fa-chevron-circle-left fa-2x"></li>
            <a class="navbar-brand" href="#pablo">Biro</a>
          </div>
          <button class="navbar-toggler" type="button" data-toggle="collapse" aria-controls="navigation-index" aria-expanded="false" aria-label="Toggle navigation">
            <span class="sr-only">Toggle navigation</span>
            <span class="navbar-toggler-icon icon-bar"></span>
            <span class="navbar-toggler-icon icon-bar"></span>
            <span class="navbar-toggler-icon icon-bar"></span>
          </button>
          <div class="collapse navbar-collapse justify-content-end">
            <form class="navbar-form">
              <?= $this->session->userdata('nm_peg');?>
            </form>
            <ul class="navbar-nav">
              <li class="nav-item dropdown">
                <a class="nav-link" href="#pablo" id="navbarDropdownProfile" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                  <i class="material-icons">person</i>
                  <p class="d-lg-none d-md-block">
                    Account
                  </p>
                </a>
                 <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdownProfile">
                  <a class="dropdown-item" href="<?=base_url();?>index.php/user/logout">Log out</a>
                  <a class="dropdown-item" href="<?=base_url();?>index.php/User/getProfile">Profil out</a>
                </div>
                <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdownProfile">
                  <a class="dropdown-item" href="<?=base_url();?>index.php/User/getProfile">Profil</a>
                </div>
              </li>
            </ul>
          </div>
        </div>
      </nav>
      <!-- End Navbar -->
      <div class="content">
        <div class="container-fluid">
          <div class="row">
            <div class="col-md-12">
              <div class="card">
                <div class="card-header card-header-primary">
                  <h4 class="card-title ">Daftar Biro</h4>
                  <button data-toggle="modal" id="" data-target="#addBiro" class="btn btn-success btn-round"><i class="fa fa-plus" aria-hidden="true"></i> Tambah Biro</button>
                </div>
                <div class="card-body">
                  <div class="table-responsive">
                    <table id="example" class="table">
                      <thead class=" text-primary">
                        <tr>
                            <th width="5"><center><b>No</b></center></th>
                            <th ><center><b>Nama Biro</b></center></th>                  
                            <!-- <th><center><b>Jumlah Admin Biro</b></center></th>                   -->
                            <th><center><b>Aksi</b></center></th>
                        </tr>
                    </thead>
                    <tbody id="table-row">
                       
                    </tbody>
                    </table>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>

        <div id="addBiro" class="modal" tabindex="-1" role="dialog">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                        <div class="modal-header">
                        <h5 class="modal-title">Tambah Biro</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                <div class="modal-body">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label>Nama Biro</label>
                                    <input type="text" name="nama_biro" id="nama_biro" class="form-control">
                                 </div>
                            </div>
                        </div>
                    
                    <div class="clearfix"></div>
                        <div class="modal-footer">
                            <button id="btn_add" type="" class="btn btn-success btn-fill btn-wd">
                                Tambah Biro
                            </button>
                            <button type="submit" class="btn btn-secondary" data-dismiss="modal">Tutup</button>
                            </div>
                        </div>
                </div>
            </div>
        </div>
       
        <div id="editBiro" class="modal" tabindex="-1" role="dialog">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                        <div class="modal-header">
                        <h5 class="modal-title">Edit Biro</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                <div class="modal-body">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group" id="">
                                    <Label>Nama Biro</Label><br>
                                    <input type="hidden" required name="id_u" id="id_u" class="form-control">
                                    <input type="text" required name="nama_biro_u" id="nama_biro_u" class="form-control">
                                 </div>
                            </div>
                        </div>
                    
                    <div class="clearfix"></div>
                        <div class="modal-footer">
                            <button id="btn_update" type="" class="btn btn-success btn-fill btn-wd">
                                Update Biro
                            </button>
                            <button type="submit" class="btn btn-secondary" data-dismiss="modal">Tutup</button>
                            </div>
                        </div>
                </div>
            </div>
        </div>

      <script>
      function getBiro() {
            $.get("http://localhost/winnerhelpdesk/index.php/WEB/Biro", function(data, status){
                console.log(data, status)
                if (status) {
                    $('#table-row').html("")
                    let no = 0;
                    data.forEach(element => {
                    var t = $('#example').DataTable();
                    no++;
                    t.row.add( [
                        "<center>"+no+"</center>",
                        "<center>"+element.nama_biro+"</center>",
                        // "<center>"+element.jml_staff+"</center>",
                        "<center><button data-toggle='tooltip' data-placement='top' title='Hapus Biro Ini' id='btn_delete' data-id="+element.id+" class='btn btn-danger btn-sm'><i class='fa fa-trash'></i></button><button data-toggle='tooltip' data-placement='top' title='Edit Biro' id='btn_edit' data-idedit="+element.id+" class='btn btn-info btn-sm' data-target='#editUser'><i class='fa fa-pencil'></i></button></center>"
                    ] ).draw( false );
                    });
                }
                else {
                    alert('Load Data Failed')
                }
            });
        }

            function clearTable() {
                var table = $('#example').DataTable();
                table
                    .clear()
                    .draw();
            }

            function clearModalAdd(){
                $('#nama_biro').val('');
            }
            
            function clearModalEdit(){
                $('#nama_biro_u').val('');
            }

   ////////////////////////////////////document ready/////////////////////////////////////////////////////////////     
        $(document).ready(function() {
            $('body').tooltip({
                selector: '[data-toggle=tooltip]'
            });
$(document).on('click', '#btn_back', function(){
             window.history.back();
            })

            $('#example').DataTable( {
                dom: 'lBfrtip',
                buttons: [
                    'copy', 'csv', 'excel', 'pdf', 'print'
                ]
            } );

            getBiro()

        } );

            $(document).on('click', '#btn_add' ,function(){
                var nama_biro                 = $('#nama_biro').val();
                // console.log(nama_biro)
            
                $.post("http://localhost/winnerhelpdesk/index.php/WEB/Biro", {
                    nama_biro                 : nama_biro,
                    },
                    function(data, status){
                    if (status) {
                        toastr.success('Insert Data Success')
                        clearTable()
                        $('#addBiro').modal('hide')
                        clearModalAdd()
                        getBiro()
                    }
                    else {
                        toastr.error('Insert Data Failed!')
                    }
                });
                
            });

            $(document).on('click', '#btn_delete' ,function(){
                var id = $(this).data("id");
                jQuery.ajax({
                url: 'http://localhost/winnerhelpdesk/index.php/WEB/Biro/' + id,
                type: 'DELETE',
                success: function(data) {
                    toastr.info('Delete Data Success!')
                    clearTable()
                    getBiro();
                    }
                });
            });

            $(document).on('click', '#btn_edit' ,function(){
                clearModalEdit()
                $('#editBiro').modal('show')
                var id =  $(this).data("idedit");

                $.ajax({
                    type: "GET",
                    url: 'http://localhost/winnerhelpdesk/index.php/WEB/Biro/'+id,
                    dataType: 'json',
                    success: function(resp) {
                    // console.log(resp)
                    $("#nama_biro_u").val(resp.nama_biro);
                    $("#id_u").val(resp.id);

                    },
                    error: function (jqXHR, exception) {
                    }
                });
            });

            $(document).on('click', '#btn_update' ,function(){
                var id                          = $("#id_u").val();
                var nama_biro                   = $("#nama_biro_u").val();
                console.log(id)
                var data = {
                    nama_biro : nama_biro,
                    }

                jQuery.ajax({
                url: 'http://localhost/winnerhelpdesk/index.php/WEB/Biro/' + id,
                type: 'PUT',
                data : data,
                success: function(data) {
                    $('#editBiro').modal('hide')
                    clearModalEdit()
                    clearTable()
                    getBiro();
                    toastr.success('Update Data Success!')
                    }
                });

            });

        
      </script>