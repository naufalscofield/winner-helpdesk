<div class="main-panel">
      <!-- Navbar -->
      <nav class="navbar navbar-expand-lg navbar-transparent navbar-absolute fixed-top ">
        <div class="container-fluid">
          <div class="navbar-wrapper">
 <li id="btn_back" class="fa fa-chevron-circle-left fa-2x"></li>
            <a class="navbar-brand" href="#pablo">Inventaris</a>
          </div>
          <button class="navbar-toggler" type="button" data-toggle="collapse" aria-controls="navigation-index" aria-expanded="false" aria-label="Toggle navigation">
            <span class="sr-only">Toggle navigation</span>
            <span class="navbar-toggler-icon icon-bar"></span>
            <span class="navbar-toggler-icon icon-bar"></span>
            <span class="navbar-toggler-icon icon-bar"></span>
          </button>
          <div class="collapse navbar-collapse justify-content-end">
            <form class="navbar-form">
              <?= $this->session->userdata('nm_peg');?>
            </form>
            <ul class="navbar-nav">
              <li class="nav-item dropdown">
                <a class="nav-link" href="#pablo" id="navbarDropdownProfile" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                  <i class="material-icons">person</i>
                  <p class="d-lg-none d-md-block">
                    Account
                  </p>
                </a>
                <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdownProfile">
                  <a class="dropdown-item" href="<?=base_url();?>index.php/user/logout">Log out</a>
                  <a class="dropdown-item" href="<?=base_url();?>index.php/User/getProfile">Profil out</a>
                </div>
                
              </li>
            </ul>
          </div>
        </div>
      </nav>
      <!-- End Navbar -->
      <div class="content">
        <div class="container-fluid">
          <div class="row">
            <div class="col-md-12">
              <div class="card">
                <div class="card-header card-header-primary">
                  <h4 class="card-title ">Daftar Inventaris</h4>
                  <button data-toggle="modal" id="" data-target="#addInventaris" class="btn btn-success btn-round"><i class="fa fa-plus" aria-hidden="true"></i> Tambah Inventaris</button>
                </div>
                <div class="card-body">
                  <div class="table-responsive">
                    <table id="example" class="table">
                      <thead class=" text-primary">
                        <tr>
                            <th width="5"><center><b>No</b></center></th>
                            <th><center><b>No Inventaris</b></center></th>                  
                            <th><center><b>Sarana</b></center></th>                  
                            <th><center><b>Lokasi</b></center></th>                    
                            <th><center><b>Aksi</b></center></th>
                        </tr>
                    </thead>
                    <tbody id="table-row">
                       
                    </tbody>
                    </table>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>

        <div id="addInventaris" class="modal" tabindex="-1" role="dialog">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                        <div class="modal-header">
                        <h5 class="modal-title">Tambah Inventaris</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label>Sarana</label>
                                    <select class="form-control" name="id_sarana" id="id_sarana">
                                        <option value="">-- Pilih Sarana --</option>
                                    </select>
                                 </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label>Lokasi</label>
                                    <select class="form-control" name="id_lokasi" id="id_lokasi">
                                        <option value="">-- Pilih Lokasi --</option>
                                    </select>
                                 </div>
                            </div>
                        </div>
                    </div>
                    
                    <div class="clearfix"></div>
                        <div class="modal-footer">
                            <button id="btn_add" type="" class="btn btn-success btn-fill btn-wd">
                                Tambah Inventaris
                            </button>
                            <button type="submit" class="btn btn-secondary" data-dismiss="modal">Tutup</button>
                        </div>
                    </div>

                </div>
            </div>
        </div>
       
        <div id="editInventaris" class="modal" tabindex="-1" role="dialog">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                        <div class="modal-header">
                        <h5 class="modal-title">Edit Inventaris</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                </div>
                <div class="modal-body">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group" id="">
                                    <Label>No Inventaris</Label><br>
                                    <input type="text" required name="id_u" id="id_u" class="form-control">
                                    <input type="text" required name="no_inventaris_u" id="no_inventaris_u" class="form-control">
                                 </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group" id="">
                                    <Label>Sarana</Label><br>
                                    <select required name="id_sarana_u" id="id_sarana_u" class="form-control">
                                    </select>
                                 </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group" id="">
                                    <Label>Lokasi</Label><br>
                                    <select required name="id_lokasi_u" id="id_lokasi_u" class="form-control">
                                    </select>
                                 </div>
                            </div>
                        </div>
                    
                    <div class="clearfix"></div>
                        <div class="modal-footer">
                            <button id="btn_update" type="" class="btn btn-success btn-fill btn-wd">
                                Update Inventaris
                            </button>
                            <button type="submit" class="btn btn-secondary" data-dismiss="modal">Tutup</button>
                        </div>
                    </div>

                </div>
            </div>
        </div>

      <script>
      function getInventaris() {
            $.get("http://localhost/winnerhelpdesk/index.php/WEB/Inventaris", function(data, status){
                console.log(data, status)
                if (status) {
                    $('#table-row').html("")
                    let no = 0;
                    data.forEach(element => {
                    var t = $('#example').DataTable();
                    no++;
                    t.row.add( [
                        "<center>"+no+"</center>",
                        "<center>"+element.no_inventaris+"</center>",
                        "<center>"+element.nama_sarana+"</center>",
                        "<center>"+element.nama_lokasi+"</center>",
                        "<center><button id='btn_delete' data-toggle='tooltip' data-placement='top' title='Hapus Inventaris Ini' data-id="+element.idinventaris+" class='btn btn-danger btn-sm'><i class='fa fa-trash'></i></button><button data-toggle='tooltip' data-placement='top' title='Detail & Edit' id='btn_edit' data-idedit="+element.idinventaris+" class='btn btn-info btn-sm' data-target='#editInventaris'><i class='fa fa-eye'></i></button></center>"
                    ] ).draw( false );
                    });
                }
                else {
                    alert('Load Data Failed')
                }
            });
        }

            function clearTable() {
                var table = $('#example').DataTable();
                table
                    .clear()
                    .draw();
            }

            function clearModalAdd(){
                $('#id_lokasi').val('');
                $('#id_sarana').val('');
            }
            
            function clearModalEdit(){
                $('#id_lokasi_u').val('');
                $('#id_sarana_u').val('');
            }

   ////////////////////////////////////document ready/////////////////////////////////////////////////////////////     
        $(document).ready(function() {
            $('body').tooltip({
                selector: '[data-toggle=tooltip]'
            });
$(document).on('click', '#btn_back', function(){
             window.history.back();
            })

            $('#example').DataTable( {
                dom: 'lBfrtip',
                buttons: [
                    'copy', 'csv', 'excel', 'pdf', 'print'
                ]
            } );

            getInventaris()

            $.get("http://localhost/winnerhelpdesk/index.php/WEB/Sarana", function(data, status){
                console.log(data, status)
                if (status) {
                    // data.forEach(element => {
                    var $el = $("#id_sarana");
                    var $ej = $("#id_sarana_u");
                        $el.empty(); // remove old options
                            $ej.empty(); // remove old options
                            $el.append($("<option></option>")
                        .attr("value", '').text('-- Pilih Sarana --'));
                        $ej.append($("<option></option>")
                        .attr("value", '').text('-- Pilih Sarana --'));
                        $.each(data, function(key, value) {
                            console.log(value)
                            $el.append($("<option></option>")
                            .attr("value", value.id).text(value.nama_sarana));
                            $ej.append($("<option></option>")
                            .attr("value", value.id).text(value.nama_sarana));
                        });
                    // });
                }
                else {
                    alert('Load Data Failed')
                }
            });
            
            $.get("http://localhost/winnerhelpdesk/index.php/WEB/Lokasi", function(data, status){
                console.log(data, status)
                if (status) {
                    // data.forEach(element => {
                    var $el = $("#id_lokasi");
                    var $ej = $("#id_lokasi_u");
                        $el.empty(); // remove old options
                            $ej.empty(); // remove old options
                            $el.append($("<option></option>")
                        .attr("value", '').text('-- Pilih Lokasi --'));
                        $ej.append($("<option></option>")
                        .attr("value", '').text('-- Pilih Lokasi --'));
                        $.each(data, function(key, value) {
                            console.log(value)
                            $el.append($("<option></option>")
                            .attr("value", value.id).text(value.nama_lokasi));
                            $ej.append($("<option></option>")
                            .attr("value", value.id).text(value.nama_lokasi));
                        });
                    // });
                }
                else {
                    alert('Load Data Failed')
                }
            });

        } );

            $(document).on('click', '#btn_add' ,function(){
                var id_lokasi                   = $('#id_lokasi').val();
                var id_sarana                         = $('#id_sarana').val(); 
                console.log(id_sarana)
                
                if ( (id_lokasi == '') || (id_sarana == '') )
                {
                    toastr.error('Harap lengkapi data!')
                } else {
                    $.post("http://localhost/winnerhelpdesk/index.php/WEB/Inventaris", {
                        id_lokasi                   : id_lokasi,
                        id_sarana                         : id_sarana,
                        },
                        function(data, status){
                        if (status) {
                            toastr.success('Insert Data Success')
                            clearTable()
                            $('#addInventaris').modal('hide')
                            clearModalAdd()
                            getInventaris()
                        }
                        else {
                            toastr.error('Insert Data Failed!')
                        }
                    });
                }
            
                
            });

            $(document).on('click', '#btn_delete' ,function(){
                var id = $(this).data("id");
                jQuery.ajax({
                url: 'http://localhost/winnerhelpdesk/index.php/WEB/Inventaris/' + id,
                type: 'DELETE',
                success: function(data) {
                    toastr.info('Delete Data Success!')
                    clearTable()
                    getInventaris();
                    }
                });
            });

            $(document).on('click', '#btn_edit' ,function(){
                clearModalEdit()
                $('#editInventaris').modal('show')
                var id =  $(this).data("idedit");

                $.ajax({
                    type: "GET",
                    url: 'http://localhost/winnerhelpdesk/index.php/WEB/Inventaris/'+id,
                    dataType: 'json',
                    success: function(resp) {
                    console.log(resp)
                    $("#id_lokasi_u").val(resp.id_lokasi);
                    $("#id_sarana_u").val(resp.id_sarana);
                    $("#no_inventaris_u").val(resp.no_inventaris);
                    $("#id_u").val(resp.idinventaris);

                    },
                    error: function (jqXHR, exception) {
                    }
                });
            });

            $(document).on('click', '#btn_update' ,function(){
                var id                              = $("#id_u").val();
                var id_lokasi                       = $("#id_lokasi_u").val();
                var id_sarana                       = $("#id_sarana_u").val();
                var no_inventaris                   = $("#no_inventaris_u").val();
                console.log(id)

                if( (id == '') || (id_lokasi == '') || (id_sarana == '') || (no_inventaris == '') )
                {
                    toastr.error('Harap lengkap data!')
                } else {
                    var data = {
                        id_lokasi : id_lokasi,
                        id_sarana : id_sarana,
                        no_inventaris : no_inventaris,
                        }

                    jQuery.ajax({
                    url: 'http://localhost/winnerhelpdesk/index.php/WEB/Inventaris/' + id,
                    type: 'PUT',
                    data : data,
                    success: function(data) {
                        $('#editInventaris').modal('hide')
                        clearModalEdit()
                        clearTable()
                        getInventaris();
                        toastr.success('Update Data Success!')
                        }
                    });
                }
                

            });

        
      </script>