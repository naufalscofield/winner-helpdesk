<style>
    *,
    *:before,
    *:after {
        box-sizing: border-box;
    }
    
    .step {
        position: relative;
        min-height: 45px/* circle-size */
        ;
    }
    
    .step > div:first-child {
        position: static;
        height: 0;
    }
    
    .step > div:last-child {
        margin-left: 45px;
        padding-left: 22px;
    }
    
    .circle-open {
        background: #4285f4;
        width: 45px;
        height: 45px;
        line-height: 45px;
        border-radius: 22px;
        position: relative;
        color: white;
        text-align: center;
    }
    
    .circle-wp {
        background: green;
        width: 45px;
        height: 45px;
        line-height: 45px;
        border-radius: 22px;
        position: relative;
        color: white;
        text-align: center;
    }
    
    .circle-closed {
        background: orange;
        width: 45px;
        height: 45px;
        line-height: 45px;
        border-radius: 22px;
        position: relative;
        color: white;
        text-align: center;
    }
    
    .circle-done {
        background: red;
        width: 45px;
        height: 45px;
        line-height: 45px;
        border-radius: 22px;
        position: relative;
        color: white;
        text-align: center;
    }
    
    .line {
        position: absolute;
        border-left: 5px solid gainsboro;
        left: 20px;
        bottom: 10px;
        top: 50px;
    }
    
    .step:last-child .line {
        display: none;
    }
    
    .title {
        line-height: 45px;
        font-weight: bold;
    }
</style>
<div class="main-panel">
    <!-- Navbar -->
    <nav class="navbar navbar-expand-lg navbar-transparent navbar-absolute fixed-top ">
        <div class="container-fluid">
            <div class="navbar-wrapper">
                <li id="btn_back" class="fa fa-chevron-circle-left fa-2x"></li>
                <a class="navbar-brand" href="#pablo">Tickets</a>
            </div>
            <button class="navbar-toggler" type="button" data-toggle="collapse" aria-controls="navigation-index" aria-expanded="false" aria-label="Toggle navigation">
                <span class="sr-only">Toggle navigation</span>
                <span class="navbar-toggler-icon icon-bar"></span>
                <span class="navbar-toggler-icon icon-bar"></span>
                <span class="navbar-toggler-icon icon-bar"></span>
            </button>
            <div class="collapse navbar-collapse justify-content-end">
                <form class="navbar-form">
                    <?= $this->session->userdata('nm_peg');?>
                        <input type="hidden" id="id_kategori_sess" value="<?= $this->session->userdata('id_kategori');?>">
                </form>
                <ul class="navbar-nav">
                    <li class="nav-item dropdown">
                        <a class="nav-link" href="#pablo" id="navbarDropdownProfile" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            <i class="material-icons">person</i>
                            <p class="d-lg-none d-md-block">
                                Account
                            </p>
                        </a>
                        <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdownProfile">
                            <a class="dropdown-item" href="<?=base_url();?>index.php/admin/logout">Log out</a>
                        </div>
                    </li>
                </ul>
            </div>
        </div>
    </nav>
    <!-- End Navbar -->
    <div class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-header card-header-primary">
                            <h4 class="card-title ">Daftar Ticket Saya</h4>
                            <button data-toggle="modal" id="" data-target="#submitTicket" class="btn btn-success btn-round"><i class="fa fa-plus" aria-hidden="true"></i> Submit Ticket</button>
                        </div>

                        <div class="card-body">
                            <div class="table-responsive">
                                <table id="example" class="table">
                                    <thead class=" text-primary">
                                        <tr>
                                            <th width="5">
                                                <center><b>No</b></center>
                                            </th>
                                            <th>
                                                <center><b>Biro Tujuan</b></center>
                                            </th>
                                            <!-- <th>
                                                <center><b>Kategori</b></center>
                                            </th> -->
                                            <th>
                                                <center><b>Priority</b></center>
                                            </th>
                                            <th>
                                                <center><b>Tanggal Open</b></center>
                                            </th>
                                            <th>
                                                <center><b>Tanggal Close</b></center>
                                            </th>
                                            <th>
                                                <center><b>Status</b></center>
                                            </th>
                                            <th>
                                                <center><b>Aksi</b></center>
                                            </th>
                                        </tr>
                                    </thead>
                                    <tbody id="table-row">

                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div id="submitTicket" class="modal" tabindex="-1" role="dialog">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Submit Ticket</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <input type="hidden" name="id_user" id="id_user" value="<?= $this->session->userdata('id');?>">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label>Kategori Masalah</label>
                                <select class="form-control border-input" name="id_kategori" required id="id_kategori">
                                    <option value="">-- Pilih Kategori Masalah --</option>

                                </select>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="form-group">
                                <label>Sub Kategori</label>
                                <select class="form-control border-input" name="id_sub_kategori" required id="id_sub_kategori">
                                    <option value="">-- Pilih Sub Kategori --</option>
                                </select>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="form-group">
                                <label>No Inventaris</label>
                                <input type="text" value="" id="no_inventaris" disabled class="form-control border-input">
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="form-group">
                                <label>Priority</label>
                                <select class="form-control border-input" name="priority" required id="priority">
                                    <option value="">-- Pilih Priority --</option>
                                    <option value="low">Low</option>
                                    <option value="normal">Normal</option>
                                    <option value="urgent">Urgent</option>
                                </select>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="form-group">
                                <label>Pesan</label>
                                <textarea class="form-control border-input" name="pesan" id="pesan" cols="30" rows="10"></textarea>
                            </div>
                        </div>
                    </div>

                    <div class="clearfix"></div>
                    <div class="modal-footer">
                        <!-- <button id="btn_submit" type="" class="btn btn-success btn-fill btn-wd">
                            Submit Ticket
                        </button> -->
                        <center>
                            <button id="btn_submit" class="btn btn-success btn-fill btn-wd ladda-button" data-style="slide-down"><span class="ladda-label">Submit</span></button>
                        </center>
                        <button type="submit" id="btn_tutup" class="btn btn-secondary" data-dismiss="modal">Tutup</button>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div id="tableInventaris" class="modal" tabindex="-1" role="dialog">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Daftar Inventaris</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="table-responsive">
                        <table id="example2" class="table">
                            <thead class=" text-primary">
                                <tr>
                                    <th width="5">
                                        <center><b>No</b></center>
                                    </th>
                                    <th>
                                        <center><b>No Inventaris</b></center>
                                    </th>
                                    <th>
                                        <center><b>Sarana</b></center>
                                    </th>
                                    <th>
                                        <center><b>Lokasi</b></center>
                                    </th>
                                    <th>
                                        <center><b>Aksi</b></center>
                                    </th>
                                </tr>
                            </thead>
                            <tbody id="table-row-inventaris">

                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div id="stepperTicket" class="modal" tabindex="-1" role="dialog">
        <div class="modal-dialog modal-md" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Timeline Ticket</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="step">
                        <div>
                            <div class="circle-open"><i class="fa fa-external-link"></i></div>
                            <div class="line"></div>
                        </div>
                        <div>
                            <div class="title">Ticket Open</div>
                            <div class="body" id="step_tgl_open"></div>
                            <div class="body" id="step_oleh_open"></div>
                        </div>
                    </div>
                    <div class="step">
                        <div>
                            <div class="circle-wp"><i class="fa fa-spinner"></i></div>
                            <div class="line"></div>
                        </div>
                        <div>
                            <div class="title">Ticket On Work Progress</div>
                            <div class="body" id="step_tgl_wp"></div>
                            <div class="body" id="step_oleh_wp"></div>
                        </div>
                    </div>
                    <div class="step">
                        <div>
                            <div class="circle-closed"><i class="fa fa-times-circle-o"></i></div>
                            <div class="line"></div>
                        </div>
                        <div>
                            <div class="title">Ticket Closed</div>
                            <div class="body" id="step_tgl_closed"></div>
                            <div class="body" id="step_oleh_closed"></div>
                        </div>
                    </div>
                    <div class="step">
                        <div>
                            <div class="circle-done"><i class="fa fa-check"></i></div>
                            <div class="line"></div>
                        </div>
                        <div>
                            <div class="title">Ticket Done</div>
                            <div class="body" id="step_tgl_done"></div>
                            <div class="body" id="step_oleh_done"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div id="ticketMessage" class="modal" tabindex="-1" role="dialog">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Ticket Message</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="col-md-12">
                    <div class="form-group" id="modal_body">
                        <div class>
                            <div class="row">
                                <input type="hidden" name="id_user" id="id_user" value="<?= $this->session->userdata('id');?>">
                                <input type="hidden" name="id_ticket_input" id="id_ticket_input" value="<?= $this->session->userdata('id');?>">
                                <div class="col-md-12">
                                    <div class="form-group" id="body_modal">
                                    </div>
                                </div>

                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label>Pesan</label>
                                        <textarea class="form-control border-input" name="pesan" id="pesan_input" cols="30" rows="10"></textarea>
                                    </div>
                                </div>
                            </div>

                            <div class="clearfix"></div>
                            <div class="modal-footer">
                                <button id="btn_submit_message" type="" class="btn btn-success btn-fill btn-wd">
                                    Kirim Pesan
                                </button>
                                <button type="submit" class="btn btn-secondary" data-dismiss="modal">Tutup</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

            <div id="detailTicket" class="modal" tabindex="-1" role="dialog">
                <div class="modal-dialog modal-lg" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title">Detail Ticket</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="modal-body">
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>Pegawai Pengadu</label>
                                        <input class="form-control" name="pegawai_detail" id="pegawai_detail"></input>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>Sub Kategori</label>
                                        <input class="form-control" name="sub_kategori_detail" id="sub_kategori_detail"></input>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>No Inventaris</label>
                                        <input class="form-control" name="no_inventaris_detail" id="no_inventaris_detail"></input>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>Tanggal Open</label>
                                        <input class="form-control" name="tanggal_open_detail" id="tanggal_open_detail"></input>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>Tanggal Work Progress</label>
                                        <input class="form-control" name="tanggal_wp_detail" id="tanggal_wp_detail"></input>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>PIC Work Progress</label>
                                        <input class="form-control" name="pic_wp_detail" id="pic_wp_detail"></input>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>Tanggal Close</label>
                                        <input class="form-control" name="tanggal_close_detail" id="tanggal_close_detail"></input>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>PIC Close</label>
                                        <input class="form-control" name="pic_close_detail" id="pic_close_detail"></input>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>Tanggal Done</label>
                                        <input class="form-control" name="tanggal_done_detail" id="tanggal_done_detail"></input>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>PIC Done</label>
                                        <input class="form-control" name="pic_done_detail" id="pic_done_detail"></input>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>Selesai Dalam</label>
                                        <input class="form-control" name="selesai_dalam_detail" id="selesai_dalam_detail"></input>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>Priority</label>
                                        <input class="form-control" name="priority_detail" id="priority_detail"></input>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label>Status</label>
                                        <input class="form-control" name="status_detail" id="status_detail"></input>
                                    </div>
                                </div>
                            </div>

                            <div class="clearfix"></div>
                            <div class="modal-footer">
                                <button type="submit" class="btn btn-secondary" data-dismiss="modal">Tutup</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        <div id="ticketMessage" class="modal" tabindex="-1" role="dialog">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title">Ticket Message</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="col-md-12">
                        <div class="form-group" id="modal_body">
                            <div class>
                                <div class="row">
                                    <input type="hidden" name="id_user" id="id_user" value="<?= $this->session->userdata('id');?>">
                                    <input type="hidden" name="id_ticket_input" id="id_ticket_input" value="<?= $this->session->userdata('id');?>">
                                    <div class="col-md-12">
                                        <div class="form-group" id="body_modal">
                                        </div>
                                    </div>

                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label>Pesan</label>
                                            <textarea class="form-control border-input" name="pesan" id="pesan_input" cols="30" rows="10"></textarea>
                                        </div>
                                    </div>
                                </div>

                                <div class="clearfix"></div>
                                <div class="modal-footer">
                                    <button id="btn_submit_message" type="" class="btn btn-success btn-fill btn-wd">
                                        Kirim Pesan
                                    </button>
                                    <button type="submit" class="btn btn-secondary" data-dismiss="modal">Tutup</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <script src="<?=base_url();?>assets/ladda/dist/spin.min.js"></script>
                <script src="<?=base_url();?>assets/ladda/dist/ladda.min.js"></script>
                <script>
                    $(document).on('click', '#btn_submit_message', function() {
                        var id_user = $('#id_user').val();
                        var pesan = $('#pesan_input').val();
                        var id_ticket = $('#id_ticket_input').val();

                        $.post("http://localhost/winnerhelpdesk/index.php/WEB/MessageUsers", {
                                id_user: id_user,
                                pesan: pesan,
                                id_ticket: id_ticket,
                            },
                            function(data, status) {
                                if (status) {
                                    toastr.success('Submit Pesan Success')
                                    clearTable()
                                    $('#ticketMessage').modal('hide')
                                        // clearModalAdd()
                                    getTickets()
                                } else {
                                    toastr.error('Submit Pesan Failed!')
                                }
                            });

                    });

                    $(document).on('click', '#no_inventaris', function() {
                        $('#tableInventaris').modal('show');

                    });

                    $('#id_kategori').on('change', function() {
                        console.log($('#id_kategori').val());
                        $.ajax({
                            type: "POST",
                            data: {
                                id_kategori: $('#id_kategori').val()
                            },
                            url: '<?php echo base_url()."index.php/users/getSubKategori" ?>',
                            dataType: 'text',
                            success: function(resp) {
                                var json = JSON.parse(resp.replace(',.', ''))
                                var $el = $("#id_sub_kategori");
                                $el.empty(); // remove old options
                                $el.append($("<option></option>")
                                    .attr("value", '').text('-- Pilih Sub Kategori --'));
                                $.each(json, function(key, value) {
                                    $el.append($("<option></option>")
                                        .attr("value", value.id).text(value.nama_sub_kategori));
                                });
                            },
                            error: function(jqXHR, exception) {
                                console.log(jqXHR, exception)
                            }
                        });

                        if ($('#id_kategori').val() == 1) {
                            $('#no_inventaris').prop("disabled", false)
                        } else {
                            $('#no_inventaris').prop("disabled", true)
                            $('#no_inventaris').val('Jaringan-Koneksi')
                        }
                    });
                </script>
                <script>
                    function getInventaris() {
                        $.get("http://localhost/winnerhelpdesk/index.php/WEB/Inventaris/", function(data, status) {
                            if (status) {
                                $('#table-row-inventaris').html("")
                                let no = 0;
                                data.forEach(element => {
                                    var t = $('#example2').DataTable();
                                    no++;
                                    t.row.add([
                                        "<center>" + no + "</center>",
                                        "<center>" + element.no_inventaris + "</center>",
                                        "<center>" + element.nama_sarana + "</center>",
                                        "<center>" + element.nama_lokasi + "</center>",
                                        "<center><button id='btn_inventaris' data-idinventaris=" + element.no_inventaris + " class='btn btn-sm btn-success' data-toggle='modal'><i class='fa fa-check'></i></button></center>"
                                    ]).draw(false);
                                });
                            }
                        });
                    }

                    function getTickets() {
                        var id_user = $('#id_user').val();
                        $.get("http://localhost/winnerhelpdesk/index.php/WEB/TicketsUsers/" + id_user, function(data, status) {
                            console.log(data, status)
                            if (status) {
                                $('#table-row').html("")
                                let no = 0;
                                data.forEach(element => {
                                    var t = $('#example').DataTable();
                                    no++;
                                    if (element.tanggal_close == null) {
                                        var tgl_close = '-'
                                    } else {
                                        var tgl_close = element.tanggal_close
                                    }

                                    if (element.priority == 'low') {
                                        var priority = "<button class='btn btn-sm btn-success'>LOW</button></center>"
                                    } else if (element.priority == 'normal') {
                                        var priority = "<button class='btn btn-sm btn-warning'>NORMAL</button></center>"
                                    } else {
                                        var priority = "<button class='btn btn-sm btn-danger'>URGENT</button></center>"
                                    }
                                    if (element.status_ticket == 'open') {
                                        t.row.add([
                                            "<center>" + no + "</center>",
                                            "<center>" + element.nama_biro + "</center>",
                                            // "<center>" + element.nama_kategori + "</center>",
                                            "<center>" + priority + "</center>",
                                            "<center>" + element.tanggal_open + "</center>",
                                            "<center>" + tgl_close + "</center>",
                                            "<center><button class='btn btn-sm btn-info'>Open</button></center>",
                                            "<center><button id='btn_message' data-idmessage=" + element.id_ticket + " class='btn btn-sm btn-primary' data-toggle='modal' data-target='#ticketMessage'><i class='fa fa-commenting'></i></button><button id='btn_stepper' data-idstepper=" + element.id_ticket + " class='btn btn-sm btn-warning' data-toggle='modal' data-target='#stepperTicket'><i class='fa fa-history'></i></button><button data-toggle='tooltip' data-placement='top' title='Detail Ticket' id='btn_detail' data-iddetail=" + element.id_ticket + " class='btn btn-sm btn-danger' data-target='#detailTicket'><i class='fa fa-eye'></i></button></center>"
                                        ]).draw(false);
                                    } else if (element.status_ticket == 'work_progress') {
                                        t.row.add([
                                            "<center>" + no + "</center>",
                                            "<center>" + element.nama_biro + "</center>",
                                            // "<center>" + element.nama_kategori + "</center>",
                                            "<center>" + priority + "</center>",
                                            "<center>" + element.tanggal_open + "</center>",
                                            "<center>" + tgl_close + "</center>",
                                            "<center><button class='btn btn-sm btn-success'>Work Progress</button></center>",
                                            "<center><button id='btn_message' data-idmessage=" + element.id_ticket + " class='btn btn-sm btn-primary' data-toggle='modal' data-target='#ticketMessage'><i class='fa fa-commenting'></i></button><button id='btn_stepper' data-idstepper=" + element.id_ticket + " class='btn btn-sm btn-warning' data-toggle='modal' data-target='#stepperTicket'><i class='fa fa-history'></i></button><button data-toggle='tooltip' data-placement='top' title='Detail Ticket' id='btn_detail' data-iddetail=" + element.id_ticket + " class='btn btn-sm btn-danger' data-target='#detailTicket'><i class='fa fa-eye'></i></button></center>"
                                        ]).draw(false);
                                    } else if (element.status_ticket == 'closed') {
                                        t.row.add([
                                            "<center>" + no + "</center>",
                                            "<center>" + element.nama_biro + "</center>",
                                            // "<center>" + element.nama_kategori + "</center>",
                                            "<center>" + priority + "</center>",
                                            "<center>" + element.tanggal_open + "</center>",
                                            "<center>" + tgl_close + "</center>",
                                            "<center><button class='btn btn-sm btn-warning'>Closed</button></center>",
                                            "<center><button id='btn_done' data-iddone=" + element.id_ticket + " class='btn btn-sm btn-danger'><i class='fa fa-check'></i></button><button id='btn_message' data-idmessage=" + element.id_ticket + " class='btn btn-sm btn-primary' data-toggle='modal' data-target='#ticketMessage'><i class='fa fa-commenting'></i></><button id='btn_stepper' data-idstepper=" + element.id_ticket + " class='btn btn-sm btn-warning' data-toggle='modal' data-target='#stepperTicket'><i class='fa fa-history'></i></button><button data-toggle='tooltip' data-placement='top' title='Detail Ticket' id='btn_detail' data-iddetail=" + element.id_ticket + " class='btn btn-sm btn-danger' data-target='#detailTicket'><i class='fa fa-eye'></i></button></center>"
                                        ]).draw(false);
                                    } else {
                                        t.row.add([
                                            "<center>" + no + "</center>",
                                            "<center>" + element.nama_biro + "</center>",
                                            // "<center>" + element.nama_kategori + "</center>",
                                            "<center>" + priority + "</center>",
                                            "<center>" + element.tanggal_open + "</center>",
                                            "<center>" + tgl_close + "</center>",
                                            "<center><button class='btn btn-sm btn-danger'>Done</button></center>",
                                            "<center><button id='btn_delete' data-id=" + element.id_ticket + " class='btn btn-sm btn-dark'><i class='fa fa-trash'></i></button><button id='btn_stepper' data-idstepper=" + element.id_ticket + " class='btn btn-sm btn-warning' data-toggle='modal' data-target='#stepperTicket'><i class='fa fa-history'></i></button><button data-toggle='tooltip' data-placement='top' title='Detail Ticket' id='btn_detail' data-iddetail=" + element.id_ticket + " class='btn btn-sm btn-danger' data-target='#detailTicket'><i class='fa fa-eye'></i></button></center>"
                                        ]).draw(false);
                                    }
                                });

                            } else {
                                alert('Load Data Failed')
                            }
                        });

                        $(document).on('click', '#btn_detail', function() {
                            $('#detailTicket').modal('show')
                            var id_ticket = $(this).data('iddetail')
                            $('#id_ticket_detail').val(id_ticket)

                            $.get("http://localhost/winnerhelpdesk/index.php/WEB/DetailTicket/" + id_ticket, function(data, status) {
                                console.log(data, status)
                                if (status) {
                                    data.forEach(element => {
                                        if (element.tanggal_open == null) {
                                            var tgl_open = '-'
                                        } else {
                                            var tgl_open = moment(element.tanggal_open).format('ddd, DD MMMM YYYY, h:mm:ss a')
                                        }

                                        if (element.tanggal_wp == null) {
                                            var tgl_wp = '-'
                                        } else {
                                            var tgl_wp = moment(element.tanggal_wp).format('ddd, DD MMMM YYYY, h:mm:ss a')
                                        }

                                        if (element.tanggal_close == null) {
                                            var tgl_close = '-'
                                        } else {
                                            var tgl_close = moment(element.tanggal_close).format('ddd, DD MMMM YYYY, h:mm:ss a')
                                        }

                                        if (element.tanggal_done == null) {
                                            var tgl_done = '-'
                                        } else {
                                            var tgl_done = moment(element.tanggal_done).format('ddd, DD MMMM YYYY, h:mm:ss a')
                                        }

                                        $('#pegawai_detail').val(element.pic_open)
                                        $('#sub_kategori_detail').val(element.nama_sub_kategori)
                                        $('#no_inventaris_detail').val(element.no_inventaris)
                                        $('#tanggal_open_detail').val(tgl_open)
                                        $('#tanggal_wp_detail').val(tgl_wp)
                                        $('#pic_wp_detail').val(element.pic_wp)
                                        $('#tanggal_close_detail').val(tgl_close)
                                        $('#pic_close_detail').val(element.pic_close)
                                        $('#tanggal_done_detail').val(tgl_done)
                                        $('#pic_done_detail').val(element.pic_done)
                                        $('#selesai_dalam_detail').val(element.selesai_dalam)
                                        $('#priority_detail').val(element.priority)
                                        $('#status_detail').val(element.status_ticket)

                                    })
                                }
                            });

                            // console.log('tes')
                        });

                        $(document).on('click', '#btn_done', function() {
                            var id = $(this).data("iddone");
                            console.log(id)
                            var data = {
                                status: 'done',
                            }

                            jQuery.ajax({
                                url: 'http://localhost/winnerhelpdesk/index.php/WEB/Tickets/' + id,
                                type: 'PUT',
                                data: data,
                                success: function(data) {
                                    clearTable()
                                    getTickets();
                                    toastr.success('Update status ticket success!')
                                    location.reload();
                                }
                            });

                        });

                        $.get("http://localhost/winnerhelpdesk/index.php/WEB/Kategori", function(data, status) {
                            console.log(data, status)
                            if (status) {
                                // data.forEach(element => {
                                var $el = $("#id_kategori");
                                // var $ej = $("#id_kategori_u");
                                $el.empty(); // remove old options
                                // $ej.empty(); // remove old options
                                $el.append($("<option></option>")
                                    .attr("value", '').text('-- Pilih Kategori --'));
                                // $ej.append($("<option></option>")
                                // .attr("value", '').text('-- Pilih Biro --'));
                                $.each(data, function(key, value) {
                                    console.log(value)
                                    $el.append($("<option></option>")
                                        .attr("value", value.id_kategori).text(value.nama_kategori));
                                    // $ej.append($("<option></option>")
                                    // .attr("value", value.id).text(value.nama_biro));
                                });
                                // });
                            } else {
                                alert('Load Data Failed')
                            }
                        });
                    }

                    function clearTable() {
                        var table = $('#example').DataTable();
                        var table2 = $('#example2').DataTable();
                        table
                            .clear()
                            .draw();
                        table2
                            .clear()
                            .draw();
                    }

                    ////////////////////////////////////document ready/////////////////////////////////////////////////////////////     
                    $(document).ready(function() {
                        $('body').tooltip({
                            selector: '[data-toggle=tooltip]'
                        });
                        $(document).on('click', '#btn_back', function() {
                            window.history.back();
                        })

                        $(document).on('click', '#btn_stepper', function() {

                            var id = $(this).data("idstepper")
                                // console.log('tes',id)
                            $.get("http://localhost/winnerhelpdesk/index.php/WEB/Stepper/" + id, function(data, status) {
                                console.log(data, status)
                                if (status) {
                                    data.forEach(element => {
                                        if (element.statusticket == "open") {
                                            // $('#step_check_open').append("<i class='fa fa-check'></i>")
                                            var tgl_open_m = moment(element.tanggal_open).format('ddd, DD MMMM YYYY, h:mm:ss a')
                                            $('#step_tgl_open').html(tgl_open_m)
                                            $('#step_oleh_open').html(element.pic_open)

                                            $('#step_tgl_wp').html('-')
                                            $('#step_oleh_wp').html('-')

                                            $('#step_tgl_closed').html('-')
                                            $('#step_oleh_closed').html('-')

                                            $('#step_tgl_done').html('-')
                                            $('#step_oleh_done').html('-')

                                        } else if (element.statusticket == "work_progress") {

                                            var tgl_open_m = moment(element.tanggal_open).format('ddd, DD MMMM YYYY, h:mm:ss a')
                                            $('#step_tgl_open').html(tgl_open_m)
                                            $('#step_oleh_open').html(element.pic_open)

                                            var tgl_wp_m = moment(element.tanggal_wp).format('ddd, DD MMMM YYYY, h:mm:ss a')
                                            $('#step_tgl_wp').html(tanggal_wp_m)
                                            $('#step_oleh_wp').html(element.pic_wp)

                                            $('#step_tgl_closed').html('-')
                                            $('#step_oleh_closed').html('-')

                                            $('#step_tgl_done').html('-')
                                            $('#step_oleh_done').html('-')

                                        } else if (element.statusticket == "closed") {
                                            var tgl_open_m = moment(element.tanggal_open).format('ddd, DD MMMM YYYY, h:mm:ss a')
                                            $('#step_tgl_open').html(tgl_open_m)
                                            $('#step_oleh_open').html(element.pic_open)

                                            var tgl_wp_m = moment(element.tanggal_wp).format('ddd, DD MMMM YYYY, h:mm:ss a')
                                            $('#step_tgl_wp').html(tanggal_wp_m)
                                            $('#step_oleh_wp').html(element.pic_wp)

                                            var tgl_close_m = moment(element.tanggal_close).format('ddd, DD MMMM YYYY, h:mm:ss a')
                                            $('#step_tgl_closed').html(tgl_close)
                                            $('#step_oleh_closed').html(element.pic_closed)

                                            $('#step_tgl_done').html('-')
                                            $('#step_oleh_done').html('-')
                                        } else {

                                            var tgl_open_m = moment(element.tanggal_open).format('ddd, DD MMMM YYYY, h:mm:ss a')
                                            $('#step_tgl_open').html(tgl_open_m)
                                            $('#step_oleh_open').html(element.pic_open)

                                            var tgl_wp_m = moment(element.tanggal_wp).format('ddd, DD MMMM YYYY, h:mm:ss a')
                                            $('#step_tgl_wp').html(tgl_wp_m)
                                            $('#step_oleh_wp').html(element.pic_wp)

                                            var tgl_close_m = moment(element.tanggal_close).format('ddd, DD MMMM YYYY, h:mm:ss a')
                                            $('#step_tgl_closed').html(tgl_close_m)
                                            $('#step_oleh_closed').html(element.pic_closed)

                                            var tgl_done_m = moment(element.tanggal_done).format('ddd, DD MMMM YYYY, h:mm:ss a')
                                            $('#step_tgl_done').html(tgl_done_m)
                                            $('#step_oleh_done').html(element.pic_done)
                                        }
                                    });
                                } else {
                                    alert('Load Data Failed')
                                }
                            });
                        })

                        $('#example').DataTable({
                            dom: 'lBfrtip',
                            buttons: [
                                'copy', 'csv', 'excel', 'pdf', 'print'
                            ],
                            "columnDefs": [{
                                "width": "30%",
                                "targets": 6
                            }, ]
                        });
                        $('#example2').DataTable({
                            dom: 'lBfrtip',
                            buttons: [
                                'copy', 'csv', 'excel', 'pdf', 'print'
                            ]
                        });

                        getTickets()
                        getInventaris()

                    });

                    $(document).on('click', '#btn_submit', function(e) {
                        var id_user = $('#id_user').val();
                        var id_kategori = $('#id_kategori').val();
                        var id_sub_kategori = $('#id_sub_kategori').val();
                        var no_inventaris = $('#no_inventaris').val();
                        var priority = $('#priority').val();
                        var pesan = $('#pesan').val();
                        console.log(id_kategori)
                        e.preventDefault();
                        var l = Ladda.create(this);

                        l.start();
                        $('#btn_tutup').prop('disabled', true)
                        $.post("http://localhost/winnerhelpdesk/index.php/WEB/TicketsUsers", {
                                id_user: id_user,
                                id_kategori: id_kategori,
                                id_sub_kategori: id_sub_kategori,
                                no_inventaris: no_inventaris,
                                priority: priority,
                                pesan: pesan,
                            },
                            function(data, status) {
                                console.log(data.status)
                                if (data.status == true) {
                                    toastr.options = {
                                        "closeButton": false,
                                        "debug": false,
                                        "newestOnTop": false,
                                        "progressBar": false,
                                        "positionClass": "toast-top-full-width",
                                        "preventDuplicates": false,
                                        "onclick": null,
                                        "showDuration": "10000",
                                        "hideDuration": "1000",
                                        "timeOut": "5000",
                                        "extendedTimeOut": "1000",
                                        "showEasing": "swing",
                                        "hideEasing": "linear",
                                        "showMethod": "fadeIn",
                                        "hideMethod": "fadeOut"
                                    }
                                    Command: toastr["success"](data.message)
                                    clearTable()
                                    $('#submitTicket').modal('hide')
                                    location.reload();
                                    getTickets()
                                    l.stop()
                                    $('#btn_tutup').prop('disabled', false)
                                } else {
                                    toastr.options = {
                                        "closeButton": false,
                                        "debug": false,
                                        "newestOnTop": false,
                                        "progressBar": false,
                                        "positionClass": "toast-top-full-width",
                                        "preventDuplicates": false,
                                        "onclick": null,
                                        "showDuration": "10000",
                                        "hideDuration": "1000",
                                        "timeOut": "5000",
                                        "extendedTimeOut": "1000",
                                        "showEasing": "swing",
                                        "hideEasing": "linear",
                                        "showMethod": "fadeIn",
                                        "hideMethod": "fadeOut"
                                    }
                                    Command: toastr["error"](data.message)
                                    clearModalAdd()
                                    l.stop()
                                    $('#btn_tutup').prop('disabled', false)
                                }
                            });

                    });

                    function ClearModalAdd() {
                        $('#id_user').val('');
                        $('#id_kategori').val('');
                        $('#id_sub_kategori').val('');
                        $('#no_inventaris').val('');
                        $('#priority').val('');
                        $('#pesan').val('');
                    }
                    $(document).on('click', '#btn_inventaris', function() {
                        var id_inventaris = $(this).data("idinventaris")
                        $('#no_inventaris').val(id_inventaris)
                        $('#tableInventaris').modal('hide')
                    });

                    $(document).on('click', '#btn_message', function() {
                        var $el = $("#body_modal");
                        $el.empty(); // remove old options
                        // clearModalMessage()
                        var id = $(this).data("idmessage");
                        $("#id_ticket_input").val(id)
                        $.ajax({
                            type: "POST",
                            data: {
                                id_ticket: id
                            },
                            url: '<?php echo base_url()."index.php/users/getMessage" ?>',
                            dataType: 'text',
                            success: function(resp) {
                                console.log(resp)
                                var json = JSON.parse(resp.replace(',.', ''))
                                var $el = $("#body_modal");
                                // $el.empty(); // remove old options
                                $.each(json, function(key, value) {
                                    if (value.nama_user == null) {
                                        $el.append($("<textarea readonly class='form-control border-input'></textarea><br>").text(value.pesan + ' //' + value.nama_staff + '-' + moment(value.tanggal).format('ddd, DD MMMM YYYY, h:mm:ss a')));
                                    } else {
                                        $el.append($("<textarea readonly class='form-control border-input'></textarea><br>").text(value.pesan + ' //' + value.nama_user + '-' + moment(value.tanggal).format('ddd, DD MMMM YYYY, h:mm:ss a')));
                                    }
                                });
                            },
                            error: function(jqXHR, exception) {
                                console.log(jqXHR, exception)
                            }
                        });
                    });

                    // $(document).on('click', '#btn_done', function() {
                    //     var id = $(this).data("iddone");
                    //     console.log(id)
                    //     var data = {
                    //         status: 'done',
                    //     }

                    //     jQuery.ajax({
                    //         url: 'http://localhost/winnerhelpdesk/index.php/WEB/TicketsUsers/' + id,
                    //         type: 'PUT',
                    //         data: data,
                    //         success: function(data) {
                    //             clearTable()
                    //             getTickets();
                    //             toastr.success('Update status ticket success!')
                    //             location.reload();
                    //         }
                    //     });

                    // });

                    $(document).on('click', '#btn_delete', function() {
                        var id = $(this).data("id");
                        console.log(id)
                        var data = {
                            status: 'deleted',
                        }

                        jQuery.ajax({
                            url: 'http://localhost/winnerhelpdesk/index.php/WEB/TicketsUsers/' + id,
                            type: 'PUT',
                            data: data,
                            success: function(data) {
                                clearTable()
                                getTickets();
                                toastr.success('Delete ticket success!')
                                location.reload();
                            }
                        });

                    });
                   </script>