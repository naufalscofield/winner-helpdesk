<div class="main-panel">
      <!-- Navbar -->
      <nav class="navbar navbar-expand-lg navbar-transparent navbar-absolute fixed-top ">
        <div class="container-fluid">
          <div class="navbar-wrapper">
 <li id="btn_back" class="fa fa-chevron-circle-left fa-2x"></li>
            <a class="navbar-brand" href="#pablo">Kritik Masuk</a>
          </div>
          <button class="navbar-toggler" type="button" data-toggle="collapse" aria-controls="navigation-index" aria-expanded="false" aria-label="Toggle navigation">
            <span class="sr-only">Toggle navigation</span>
            <span class="navbar-toggler-icon icon-bar"></span>
            <span class="navbar-toggler-icon icon-bar"></span>
            <span class="navbar-toggler-icon icon-bar"></span>
          </button>
          <div class="collapse navbar-collapse justify-content-end">
            <form class="navbar-form">
              <?= $this->session->userdata('nm_peg');?>
            </form>
            <ul class="navbar-nav">
              <li class="nav-item dropdown">
                <a class="nav-link" href="#pablo" id="navbarDropdownProfile" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                  <i class="material-icons">person</i>
                  <p class="d-lg-none d-md-block">
                    Account
                  </p>
                </a>
                 <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdownProfile">
                  <a class="dropdown-item" href="<?=base_url();?>index.php/user/logout">Log out</a>
                  <a class="dropdown-item" href="<?=base_url();?>index.php/User/getProfile">Profil out</a>
                </div>
                <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdownProfile">
                  <a class="dropdown-item" href="<?=base_url();?>index.php/User/getProfile">Profil</a>
                </div>
              </li>
            </ul>
          </div>
        </div>
      </nav>
      <!-- End Navbar -->
      <div class="content">
        <div class="container-fluid">
          <div class="row">
            <div class="col-md-12">
              <div class="card">
                <div class="card-header card-header-primary">
                  <!-- <h4 class="card-title ">Daftar Biro</h4> -->
                  <input type="hidden" id="id_biro_sess" value="<?=$this->session->userdata('id_biro');?>">
                  <button data-toggle="modal" id="" data-target="#addKritik" class="btn btn-success btn-round"><i class="fa fa-plus" aria-hidden="true"></i> Submit Kritik</button>
                </div>
                <div class="card-body">
                  <div class="table-responsive">
                    <table id="example" class="table">
                      <thead class=" text-primary">
                        <tr>
                            <th width="5"><center><b>No</b></center></th>
                            <th ><center><b>Nama Pegawai</b></center></th>                  
                            <th><center><b>Sentiment</b></center></th>                  
                            <th><center><b>Aksi</b></center></th>
                        </tr>
                    </thead>
                    <tbody id="table-row">
                       
                    </tbody>
                    </table>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>

        <div id="addKritik" class="modal" tabindex="-1" role="dialog">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                        <div class="modal-header">
                        <h5 class="modal-title">Submit Kritik</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                <div class="modal-body">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label>Pilih Biro</label>
                                    <input type="hidden" value="<?=$this->session->userdata('id');?>" id="id_user">
                                    <select name="id_biro" id="id_biro" class="form-control border-input">
                                      <option value="">Pilih Biro</option>
                                      <?php
                                      $q = $this->db->get('biro')->result_array();
                                      foreach($q as $q){ ?>
                                      <option value="<?= $q['id'];?>"><?= $q['nama_biro'];?></option>
                                      <?php } ?>
                                    </select>
                                 </div>
                            </div>
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label>Pesan</label>
                                    <textarea name="pesan" id="pesan" cols="30" rows="10" class="form-control border-input"></textarea>
                                 </div>
                            </div>
                        </div>
                    
                    <div class="clearfix"></div>
                        <div class="modal-footer">
                            <button id="btn_submit" type="" class="btn btn-success btn-fill btn-wd">
                                Submit Kritik
                            </button>
                            <button type="submit" class="btn btn-secondary" data-dismiss="modal">Tutup</button>
                            </div>
                        </div>
                </div>
            </div>
        </div>
        <div id="detailKritik" class="modal" tabindex="-1" role="dialog">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                        <div class="modal-header">
                        <h5 class="modal-title">Detail Kritik</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                <div class="modal-body">
                       
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label>Pesan</label>
                                    <textarea name="pesan" id="pesan_detail" cols="30" rows="10" class="form-control border-input"></textarea>
                                 </div>
                            </div>
                        </div>
                </div>
            </div>
        </div>
      
      <script>
      function getKritik() {
            var id_biro = $('#id_biro_sess').val();
            $.get("http://localhost/winnerhelpdesk/index.php/WEB/kritik/" + id_biro, function(data, status){
                console.log(data)
                if (status) {
                    $('#table-row').html("")
                    let no = 0;
                    data.forEach(element => {
                    var t = $('#example').DataTable();
                    no++;
                    if(element.arah_sentiment == 'positif'){
                      var sentiment = "<center><button data-toggle='tooltip' data-placement='top' title='Kritik Mengarah Ke Positif' class='btn btn-success btn-sm'><i class='fa fa-plus'></i></button></center>"
                    } else {
                      var sentiment = "<center><button data-toggle='tooltip' data-placement='top' title='Kritik Mengarah Ke Negatif' class='btn btn-danger btn-sm'><i class='fa fa-minus'></i></button></center>"
                    }
                    t.row.add( [
                        "<center>"+no+"</center>",
                        "<center>"+element.nm_peg+"</center>",
                        "<center>"+sentiment+"</center>",
                        "<center><button id='btn_detail' data-toggle='tooltip' data-placement='top' title='Detail Kritik' data-iddetail="+element.idkritik+" class='btn btn-info btn-sm' data-target='#detailKritik'><i class='fa fa-eye'></i></button></center>"
                    ] ).draw( false );
                    });
                }
                else {
                    alert('Load Data Failed')
                }
            });
        }

            function clearTable() {
                var table = $('#example').DataTable();
                table
                    .clear()
                    .draw();
            }

            function clearModalAdd(){
                $('#id_biro').val('');
                $('#pesan').val('');
            }
            
            function clearModalDetail(){
                $('#pesan_detail').val('');
            }

   ////////////////////////////////////document ready/////////////////////////////////////////////////////////////     
        $(document).ready(function() {
            $('body').tooltip({
                selector: '[data-toggle=tooltip]'
            });
$(document).on('click', '#btn_back', function(){
             window.history.back();
            })

            $('#example').DataTable( {
                dom: 'lBfrtip',
                buttons: [
                    'copy', 'csv', 'excel', 'pdf', 'print'
                ]
            } );

            getKritik()

        } );

            $(document).on('click', '#btn_submit' ,function(){
                var id_user                 = $('#id_user').val();
                var id_biro_tujuan          = $('#id_biro').val();
                var pesan                   = $('#pesan').val();
                // console.log(nama_biro)
            
                $.post("http://localhost/winnerhelpdesk/index.php/WEB/kritik", {
                    id_user                 : id_user,
                    id_biro_tujuan          : id_biro_tujuan,
                    pesan                   : pesan,
                    },
                    function(data, status){
                      if (data.status) {
                      console.log('sukses')
                      // toastr.success('Anda mengirimkan kritik bernada '+data.sentiment)
                      toastr.options = {
                        "closeButton": true,
                        "debug": false,
                        "newestOnTop": false,
                        "progressBar": false,
                        "positionClass": "toast-top-full-width",
                        "preventDuplicates": false,
                        "onclick": null,
                        "showDuration": "30000",
                        "hideDuration": "20000",
                        "timeOut": "5000",
                        "extendedTimeOut": "1000",
                        "showEasing": "swing",
                        "hideEasing": "linear",
                        "showMethod": "fadeIn",
                        "hideMethod": "fadeOut"
                      }
                      Command: toastr["success"]("Dengan Skor Sentiment Sebesar "+data.skor, "Anda Telah Mengirimkan Kritik Bernada "+data.sentiment)

                     clearTable()
                     clearModalAdd()

                      $('#addKritik').modal('hide')
                      getKritik()
                    }
                    else {
                      console.log('gagal')
                        toastr.error('Gagal submit kritik')
                    }
                });
                
            });

            $(document).on('click', '#btn_detail' ,function(){
              // console.log('klik')
                clearModalDetail()
                $('#detailKritik').modal('show')
                var id =  $(this).data("iddetail");
                // console.log(id)

                $.ajax({
                    type: "put",
                    url: 'http://localhost/winnerhelpdesk/index.php/WEB/kritik/'+id,
                    dataType: 'json',
                    success: function(resp) {
                      console.log(resp)
                    $("#pesan_detail").val(resp.pesan);

                    },
                    error: function (jqXHR, exception) {
                    }
                });
            });

            $(document).on('click', '#btn_update' ,function(){
                var id                          = $("#id_u").val();
                var nama_biro                   = $("#nama_biro_u").val();
                console.log(id)
                var data = {
                    nama_biro : nama_biro,
                    }

                jQuery.ajax({
                url: 'http://localhost/winnerhelpdesk/index.php/WEB/Biro/' + id,
                type: 'PUT',
                data : data,
                success: function(data) {
                    $('#editBiro').modal('hide')
                    clearModalDetail()
                    clearTable()
                    getKritik();
                    toastr.success('Update Data Success!')
                    }
                });

            });

        
      </script>