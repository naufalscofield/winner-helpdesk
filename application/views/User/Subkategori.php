<div class="main-panel">
      <!-- Navbar -->
      <nav class="navbar navbar-expand-lg navbar-transparent navbar-absolute fixed-top ">
        <div class="container-fluid">
          <div class="navbar-wrapper">
 <li id="btn_back" class="fa fa-chevron-circle-left fa-2x"></li>
            <a class="navbar-brand" href="#pablo">Sub Kategori</a>
          </div>
          <button class="navbar-toggler" type="button" data-toggle="collapse" aria-controls="navigation-index" aria-expanded="false" aria-label="Toggle navigation">
            <span class="sr-only">Toggle navigation</span>
            <span class="navbar-toggler-icon icon-bar"></span>
            <span class="navbar-toggler-icon icon-bar"></span>
            <span class="navbar-toggler-icon icon-bar"></span>
          </button>
          <div class="collapse navbar-collapse justify-content-end">
            <form class="navbar-form">
              <?= $this->session->userdata('nm_peg');?>
            </form>
            <ul class="navbar-nav">
              <li class="nav-item dropdown">
                <a class="nav-link" href="#pablo" id="navbarDropdownProfile" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                  <i class="material-icons">person</i>
                  <p class="d-lg-none d-md-block">
                    Account
                  </p>
                </a>
                 <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdownProfile">
                  <a class="dropdown-item" href="<?=base_url();?>index.php/user/logout">Log out</a>
                  <a class="dropdown-item" href="<?=base_url();?>index.php/User/getProfile">Profil out</a>
                </div>
                <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdownProfile">
                  <a class="dropdown-item" href="<?=base_url();?>index.php/User/getProfile">Profil</a>
                </div>
              </li>
            </ul>
          </div>
        </div>
      </nav>
      <!-- End Navbar -->
      <div class="content">
        <div class="container-fluid">
          <div class="row">
            <div class="col-md-12">
              <div class="card">
                <div class="card-header card-header-primary">
                  <h4 class="card-title ">Daftar Sub Kategori</h4>
                  <button data-toggle="modal" id="" data-target="#addSubKategori" class="btn btn-success btn-round"><i class="fa fa-plus" aria-hidden="true"></i> Tambah Sub Kategori</button>
                </div>
                <div class="card-body">
                  <div class="table-responsive">
                    <table id="example" class="table">
                      <thead class=" text-primary">
                        <tr>
                            <th width="5"><center><b>No</b></center></th>
                            <th><center><b>Nama Sub Kategori</b></center></th>                  
                            <th><center><b>Kategori</b></center></th>                  
                            <th><center><b>Aksi</b></center></th>
                        </tr>
                    </thead>
                    <tbody id="table-row">
                       
                    </tbody>
                    </table>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>

        <div id="addSubKategori" class="modal" tabindex="-1" role="dialog">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                        <div class="modal-header">
                        <h5 class="modal-title">Tambah Sub Kategori</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                <div class="modal-body">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label>Kategori</label>
                                    <select class="form-control" name="id_kategori" id="id_kategori">
                                        <option value="">-- Pilih Kategori --</option>
                                    </select>
                                 </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label>Nama Sub Kategori</label>
                                    <input type="text" name="nama_sub_kategori" id="nama_sub_kategori" class="form-control">
                                 </div>
                            </div>
                        </div>
                    
                    <div class="clearfix"></div>
                        <div class="modal-footer">
                            <button id="btn_add" type="" class="btn btn-success btn-fill btn-wd">
                                Tambah Sub Kategori
                            </button>
                            <button type="submit" class="btn btn-secondary" data-dismiss="modal">Tutup</button>
                            </div>
                        </div>
                </div>
            </div>
        </div>
       
        <div id="editSubKategori" class="modal" tabindex="-1" role="dialog">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                        <div class="modal-header">
                        <h5 class="modal-title">Edit Sub Kategori</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                <div class="modal-body">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group" id="">
                                    <Label>Nama Sub Kategori</Label><br>
                                    <input type="hidden" required name="id_u" id="id_u" class="form-control">
                                    <input type="text" required name="nama_sub_kategori_u" id="nama_sub_kategori_u" class="form-control">
                                 </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group" id="">
                                    <Label>Kategori</Label><br>
                                    <select class="form-control" name="id_kategori_u" id="id_kategori_u">
                                    
                                    </select>
                                </div>
                            </div>
                        </div>
                    
                    <div class="clearfix"></div>
                        <div class="modal-footer">
                            <button id="btn_update" type="" class="btn btn-success btn-fill btn-wd">
                                Update Sub Kategori
                            </button>
                            <button type="submit" class="btn btn-secondary" data-dismiss="modal">Tutup</button>
                            </div>
                        </div>
                </div>
            </div>
        </div>

      <script>
      function getSubKategori() {
            $.get("http://localhost/winnerhelpdesk/index.php/WEB/SubKategori", function(data, status){
                console.log(data, status)
                if (status) {
                    $('#table-row').html("")
                    let no = 0;
                    data.forEach(element => {
                    var t = $('#example').DataTable();
                    no++;
                    t.row.add( [
                        "<center>"+no+"</center>",
                        "<center>"+element.nama_sub_kategori+"</center>",
                        "<center>"+element.nama_kategori+"</center>",
                        "<center><button data-toggle='tooltip' data-placement='top' title='Hapus Sub Kategori Ini' id='btn_delete' data-id="+element.id_sub_kategori+" class='btn btn-danger btn-sm'><i class='fa fa-trash'></i></button><button data-toggle='tooltip' data-placement='top' title='Detail & Edit' id='btn_edit' data-idedit="+element.id_sub_kategori+" class='btn btn-info btn-sm' data-target='#editSubKategori'><i class='fa fa-eye'></i></button></center>"
                    ] ).draw( false );
                    });
                }
                else {
                    alert('Load Data Failed')
                }
            });
        }

            function clearTable() {
                var table = $('#example').DataTable();
                table
                    .clear()
                    .draw();
            }

            function clearModalAdd(){
                $('#nama_sub_kategori').val('');
                $('#id_kategori').val('');
            }
            
            function clearModalEdit(){
                $('#nama_sub_kategori_u').val('');
                $('#id_kategori_u').val('');
            }

   ////////////////////////////////////document ready/////////////////////////////////////////////////////////////     
        $(document).ready(function() {
            $('body').tooltip({
                selector: '[data-toggle=tooltip]'
            });
$(document).on('click', '#btn_back', function(){
             window.history.back();
            })

            $('#example').DataTable( {
                dom: 'lBfrtip',
                buttons: [
                    'copy', 'csv', 'excel', 'pdf', 'print'
                ]
            } );

            getSubKategori()

            $.get("http://localhost/winnerhelpdesk/index.php/WEB/Kategori", function(data, status){
                console.log(data, status)
                if (status) {
                    // data.forEach(element => {
                    var $el = $("#id_kategori");
                    var $ej = $("#id_kategori_u");
                        $el.empty(); // remove old options
                            $ej.empty(); // remove old options
                            $el.append($("<option></option>")
                        .attr("value", '').text('-- Pilih Kategori --'));
                        $ej.append($("<option></option>")
                        .attr("value", '').text('-- Pilih Kategori --'));
                        $.each(data, function(key, value) {
                            console.log(value)
                            $el.append($("<option></option>")
                            .attr("value", value.id_kategori).text(value.nama_kategori));
                            $ej.append($("<option></option>")
                            .attr("value", value.id_kategori).text(value.nama_kategori));
                        });
                    // });
                }
                else {
                    alert('Load Data Failed')
                }
            });

        } );

            $(document).on('click', '#btn_add' ,function(){
                var nama_sub_kategori                   = $('#nama_sub_kategori').val();
                var id_kategori                         = $('#id_kategori').val(); 
                console.log(id_kategori)
                
                if ( (nama_sub_kategori == '') || (id_kategori == '') )
                {
                    toastr.error('Harap lengkapi data!')
                } else {
                    $.post("http://localhost/winnerhelpdesk/index.php/WEB/Subkategori", {
                        nama_sub_kategori                   : nama_sub_kategori,
                        id_kategori                         : id_kategori,
                        },
                        function(data, status){
                        if (status) {
                            toastr.success('Insert Data Success')
                            clearTable()
                            $('#addSubKategori').modal('hide')
                            clearModalAdd()
                            getSubKategori()
                        }
                        else {
                            toastr.error('Insert Data Failed!')
                        }
                    });
                }
            
                
            });

            $(document).on('click', '#btn_delete' ,function(){
                var id = $(this).data("id");
                jQuery.ajax({
                url: 'http://localhost/winnerhelpdesk/index.php/WEB/SubKategori/' + id,
                type: 'DELETE',
                success: function(data) {
                    toastr.info('Delete Data Success!')
                    clearTable()
                    getSubKategori();
                    }
                });
            });

            $(document).on('click', '#btn_edit' ,function(){
                clearModalEdit()
                $('#editSubKategori').modal('show')
                var id =  $(this).data("idedit");

                $.ajax({
                    type: "GET",
                    url: 'http://localhost/winnerhelpdesk/index.php/WEB/SubKategori/'+id,
                    dataType: 'json',
                    success: function(resp) {
                    console.log(resp)
                    $("#nama_sub_kategori_u").val(resp.nama_sub_kategori);
                    $("#id_kategori_u").val(resp.id_kategori);
                    $("#id_u").val(resp.id_sub_kategori);

                    },
                    error: function (jqXHR, exception) {
                    }
                });
            });

            $(document).on('click', '#btn_update' ,function(){
                var id                              = $("#id_u").val();
                var nama_sub_kategori                   = $("#nama_sub_kategori_u").val();
                var id_kategori                         = $("#id_kategori_u").val();
                console.log(id)

                if( (id == '') || (nama_sub_kategori == '') || (id_kategori == '') )
                {
                    toastr.error('Harap lengkap data!')
                } else {
                    var data = {
                        nama_sub_kategori : nama_sub_kategori,
                        id_kategori : id_kategori,
                        }

                    jQuery.ajax({
                    url: 'http://localhost/winnerhelpdesk/index.php/WEB/SubKategori/' + id,
                    type: 'PUT',
                    data : data,
                    success: function(data) {
                        $('#editSubKategori').modal('hide')
                        clearModalEdit()
                        clearTable()
                        getSubKategori();
                        toastr.success('Update Data Success!')
                        }
                    });
                }
                

            });

        
      </script>