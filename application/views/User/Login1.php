<!DOCTYPE html>
<html lang="en">
<head>
	<title>Login Winner Helpdesk</title>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
<!--===============================================================================================-->	
	<link href="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/css/toastr.min.css" rel="stylesheet">
	<link rel="icon" type="image/png" href="<?=base_url();?>assets/login2_bootstrap/images/icons/favicon.ico"/>
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="<?=base_url();?>assets/login2_bootstrap/vendor/bootstrap/css/bootstrap.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="<?=base_url();?>assets/login2_bootstrap/fonts/font-awesome-4.7.0/css/font-awesome.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="<?=base_url();?>assets/login2_bootstrap/vendor/animate/animate.css">
<!--===============================================================================================-->	
	<link rel="stylesheet" type="text/css" href="<?=base_url();?>assets/login2_bootstrap/vendor/css-hamburgers/hamburgers.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="<?=base_url();?>assets/login2_bootstrap/vendor/select2/select2.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="<?=base_url();?>assets/login2_bootstrap/css/util.css">
	<link rel="stylesheet" type="text/css" href="<?=base_url();?>assets/login2_bootstrap/css/main.css">
<!--===============================================================================================-->
</head>
<body>
	
	<div class="limiter">
		<div class="container-login100">
			<div class="wrap-login100">
				<div class="login100-pic js-tilt" data-tilt>
					<img src="<?=base_url();?>assets//logo.jpg" alt="IMG">
				</div>

				<!-- <form class="login100-form"> -->
				<div class = "login100-form">
					<span class="login100-form-title">
						Login Pegawai
					</span>

					<div class="wrap-input100 validate-input data-validate = "Valid email is required: ex@abc.xyz">
						<input class="input100" type="text" id="nip" placeholder="Nip">
						<span class="focus-input100"></span>
						<span class="symbol-input100">
							<i class="fa fa-id-badge" aria-hidden="true"></i>
						</span>
					</div>

					<div class="wrap-input100 validate-input" data-validate = "Password is required">
						<input class="input100" type="password" id="password" placeholder="Password">
						<span class="focus-input100"></span>
						<span class="symbol-input100">
							<i class="fa fa-lock" aria-hidden="true"></i>
						</span>
					</div>
					
					<div class="container-login100-form-btn">
						<button id="btn_login" class="login100-form-btn">
							Login
						</button>
					</div>

					<div class="text-center p-t-12">
					</div>

					<div class="text-center p-t-136">
					</div>
					</div>

			</div>
		</div>
	</div>
	
	

	
<!--===============================================================================================-->	
	<script src="<?=base_url();?>assets/login2_bootstrap/vendor/jquery/jquery-3.2.1.min.js"></script>
<!--===============================================================================================-->
	<script src="<?=base_url();?>assets/login2_bootstrap/vendor/bootstrap/js/popper.js"></script>
	<script src="<?=base_url();?>assets/login2_bootstrap/vendor/bootstrap/js/bootstrap.min.js"></script>
<!--===============================================================================================-->
	<script src="<?=base_url();?>assets/login2_bootstrap/vendor/select2/select2.min.js"></script>

<!--===============================================================================================-->
	<script src="<?=base_url();?>assets/login2_bootstrap/vendor/tilt/tilt.jquery.min.js"></script>
	<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/js/toastr.min.js"></script>
	<script >
		$('.js-tilt').tilt({
			scale: 1.1
		});
	</script>
<!--===============================================================================================-->
	<script src="<?=base_url();?>assets/login2_bootstrap/js/main.js"></script>
	<script>

		$(document).on('click', '#btn_login' ,function(){
			var nip = $('#nip').val();
			var password = $('#password').val();

			if ((nip == '') || (password == '')){
				toastr.error('Harap isi NIP dan Password!')
			} else {
				$.post("http://localhost/winnerhelpdesk/index.php/WEB/Login", {
					nip: nip,
					password: password
				}, function(data, status){
					console.log(data, status)
					if (data.status) {
						toastr.success(data.message)
						window.location.href="http://localhost/winnerhelpdesk/index.php/user/"				
						} else {
						toastr.error(data.message)
					}
				});
			}
		});
	
</script>

</body>
</html>