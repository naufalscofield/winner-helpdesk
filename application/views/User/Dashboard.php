<div class="main-panel">
      <!-- Navbar -->
      <nav class="navbar navbar-expand-lg navbar-transparent navbar-absolute fixed-top ">
        <div class="container-fluid">
          <div class="navbar-wrapper">
 <li id="btn_back" class="fa fa-chevron-circle-left fa-2x"></li>
            <a class="navbar-brand" href="#pablo">Dashboard and Report</a>
          </div>
          <button class="navbar-toggler" type="button" data-toggle="collapse" aria-controls="navigation-index" aria-expanded="false" aria-label="Toggle navigation">
            <span class="sr-only">Toggle navigation</span>
            <span class="navbar-toggler-icon icon-bar"></span>
            <span class="navbar-toggler-icon icon-bar"></span>
            <span class="navbar-toggler-icon icon-bar"></span>
          </button>
          <div class="collapse navbar-collapse justify-content-end">
            <form class="navbar-form">
              <?= $this->session->userdata('nm_peg');?>
            </form>
            <ul class="navbar-nav">
              <li class="nav-item dropdown">
                <a class="nav-link" href="#pablo" id="navbarDropdownProfile" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                  <i class="material-icons">person</i>
                  <p class="d-lg-none d-md-block">
                    Account
                  </p>
                </a>
                <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdownProfile">
                  <a class="dropdown-item" href="<?=base_url();?>index.php/user/logout">Log out</a>
                  <a class="dropdown-item" href="<?=base_url();?>index.php/User/getProfile">Profil out</a>
                </div>
              </li>
            </ul>
          </div>
        </div>
      </nav>
      <!-- End Navbar -->
      <div class="content">
        <div class="container-fluid">
          <div class="row">
            <div class="col-lg-3 col-md-6 col-sm-6">
              <div class="card card-stats">
                <div class="card-header card-header-info card-header-icon">
                  <div class="card-icon">
                  <i class='fa fa-folder-open'></i>
                  </div>
                  <?php
                  $id_biro = $this->session->userdata('id_biro');
                  $dataBiro = $this->db->get_where('biro',['id' => $id_biro])->row();
                  ?>
                  <!-- <p class="card-category">Biro <?=$dataBiro->nama_biro;?> Today <b>Open</b> Issue</p> -->
                  <p class="card-category">Biro <?=$dataBiro->nama_biro;?> Ticket <b>Open</b></p>
                  <?php
                  $date = date('Y-m-d');
                  $q = $this->db->get_where('tickets',array('id_biro' => $id_biro, 'status' => 'open'));
                  // print_r($q); die;
                  $q = $q->num_rows();

                  ?>
                  <h3 class="card-title"><?= $q;?>
                    <small>Issue</small>
                  </h3>
                </div>
                <div class="card-footer">
                  <div class="stats">
                    
                  </div>
                </div>
              </div>
            </div>
            <div class="col-lg-3 col-md-6 col-sm-6">
              <div class="card card-stats">
                <div class="card-header card-header-success card-header-icon">
                  <div class="card-icon">
                  <i class="fa fa-spinner fa-pulse fa-3x fa-fw"></i>
                  </div>
                  <p class="card-category">Biro <?=$dataBiro->nama_biro;?> Ticket <b>Work Progress</b></p>
                  <?php
                  $date = date('Y-m-d');
                  $q = $this->db->get_where('tickets',array('status' => 'work_progress','id_biro' => $id_biro));
                  $q = $q->num_rows()
                  ?>
                  <h3 class="card-title"><?= $q;?>
                    <small>Issue</small>
                  </h3>
                </div>
                <div class="card-footer">
                  <div class="stats">
                  </div>
                </div>
              </div>
            </div>
            <div class="col-lg-3 col-md-6 col-sm-6">
              <div class="card card-stats">
                <div class="card-header card-header-warning card-header-icon">
                  <div class="card-icon">
                  <i class='fa fa-window-close-o'></i>
                  </div>
                  <p class="card-category">Biro <?=$dataBiro->nama_biro;?> Ticket <b>Closed</b></p>
                  <?php
                  $date = date('Y-m-d');
                  $q = $this->db->get_where('tickets',array('tanggal_close' => $date,'id_biro' => $id_biro));
                  $q = $q->num_rows()
                  ?>
                  <h3 class="card-title"><?= $q;?>
                    <small>Issue</small>
                  </h3>
                </div>
                <div class="card-footer">
                  <div class="stats">
                    
                  </div>
                </div>
              </div>
            </div>
            <div class="col-lg-3 col-md-6 col-sm-6">
              <div class="card card-stats">
                <div class="card-header card-header-danger card-header-icon">
                  <div class="card-icon">
                    <i class='fa fa-check'></i>
                  </div>
                  <p class="card-category">Biro <?=$dataBiro->nama_biro;?> Ticket <b>Done</b> (Bulan ini)</p>
                  <?php
                  $y = date('Y');
                  $m = date('m');
                  // print_r(date('m')); die;
                  $q = $this->db->query("SELECT * FROM tickets WHERE MONTH(tanggal_close) = $m AND YEAR(tanggal_close) = $y and status = 'done' and id_biro = $id_biro");
                  $q = $q->num_rows()
                  ?>
                  <h3 class="card-title"><?= $q; ?>
                    <small>Issue</small>
                  </h3>
                </div>
                <div class="card-footer">
                  <div class="stats">
                    <!-- <i class="material-icons">local_offer</i> Tracked from Github -->
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div class="content">
        <div class="container-fluid">
          <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12">
              <div class="card card-stats">
                <div class="card-header card-header-primary card-header-icon">
                  <div class="card-icon">
                  <i class='fa fa-handshake-o'></i>
                  </div><br>
                  <center><h4 class="card-title"><b>Sub Bagian SI:</b> Service Level Agreement "Jaringan Dan Internet"</h4>
                  <?php
                  $q = $this->db->query("select sum(datediff(tanggal_close,tanggal_open)) as jumlah from tickets")->row();
                  $q = $q->jumlah;
                  $q2 = $this->db->query("select * from tickets where tanggal_close is not null");
                  $q2 = $q2->num_rows();
                  $q3 = $q / $q2; 
                  ?>
                  <h3 class="card-title">Target : 95% Up (8.328 Jam Setahun)
                    <small>dari 8.760 Jam</small>
                  </h3></center>
                  <div id="sla" style="height: 400px"></div>
                  <center>
                  <h2 class="card-title" id="realisasi">Target : 95% Up (347 Hari)
                    <small>sampai hari ini</small>
                  </h2>
                  </center>
                </div>
                <div class="card-footer">
                  <div class="stats">
                    
                  </div>
                </div>
              </div>
            </div>
            </div>
          <div class="row">
            <div class="col-md-5">
              <div class="card card-chart">
                <div class="card-header card-header-success">
                  <div class="ct-chart" id="dailySalesChart"></div>
                </div>
                <div class="card-body">
                  <h4 class="card-title">Biro paling sering dikomplain</h4>
                  <i><h5 class="card-title">Most complained bureau</h5></i>
                  <p class="card-category">
                    <span id="reportDataBiro" class="text-success"></span> 
                  </p>
                </div>
                <div class="card-footer">
                  <div class="stats">
                  </div>
                </div>
              </div>
            </div>
            <div class="col-md-7">
              <div class="card card-chart">
                <div class="card-header card-header-danger">
                  <div class="ct-chart" id="completedTasksChart"></div>
                </div>
                <div class="card-body">
                  <h4 class="card-title">Kategori paling sering dikomplain</h4>
                  <i><h5 class="card-title">Most complained category</h5></i>
                </div>
                <div class="card-footer">
                  <div class="stats">
                    <!-- <i class="material-icons">access_time</i> campaign sent 2 days ago -->
                  </div>
                </div>
              </div>
            </div>
          </div>
      <div class="content">
        <div class="container-fluid">
          <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12">
              <div class="card card-stats">
                <div class="card-header card-header-primary card-header-icon">
                  <div class="card-icon">
                  <i class='fa fa-clock-o'></i>
                  </div><br>
                  <center><h4 class="card-title">Rata Rata<b> Penyelesaian </b>Ticket</h4>
                  <?php
                  $q = $this->db->query("select sum(selesai_dalam) as jumlah from tickets where id_biro = $id_biro")->row();
                  $q = $q->jumlah;
                  $q2 = $this->db->query("select * from tickets where tanggal_close is not null and id_biro = $id_biro");
                  $q2 = $q2->num_rows();
                  // print_r($q2); die;
                  $q3 = $q / $q2; 
                  ?>
                  <h1 class="card-title"><?= $q3;?>
                    <small>Jam</small>
                  </h1></center>
                </div>
                <div class="card-footer">
                  <div class="stats">
                    
                  </div>
                </div>
              </div>
            </div>
            </div>
            
      
       
      <script src="http://code.highcharts.com/highcharts.js"></script>
      <script>
      $(function () {
        var dataChart = [0,0,0,0,0,0,0,0,0,0,0,0]
        $.get("http://localhost/winnerhelpdesk/index.php/WEB/ReportSLA", function(data, status){
                console.log(data, status)
                if (status) {
                    dataChart = [data[0], data[1], data[2], data[3], data[4], data[5], data[6], data[7], data[8], data[9], data[10], data[11]]
                    var totalUp = 365 - data[11]
                    var persentaseUp = (totalUp/347)*100
                    var persentaseUp = Math.round(persentaseUp)
                    // console.log(persentaseUp)
                    document.getElementById("realisasi").innerHTML = "Realisasi :" + persentaseUp + "% Up (sampai hari ini)";
                    }
                $('#sla').highcharts({
                  // chart:{
                  //   backgroundColor:"#1E90FF"
                  // },
                    yAxis: {
                          min: 0,
                          max: 50,
                          plotLines: [{
                              color: '#FF0000',
                              width: 2,
                              value: 18
                          }]
                      },
                      
                    xAxis: {
                        categories: ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec']
                    },

                    title: {
                      text: 'Grafik Realisasi Total Hari Internet Down Berdasarkan Tickets'
                    },

                    series: [{
                        title: 'Total Day by Tickets',
                        type: 'line',
                        // data: [29.9, 71.5, 106.4, 129.2, 144.0, 176.0, 135.6, 148.5, 216.4, 194.1, 95.6, 54.4]
                        data : dataChart
                        // data: [data[0], data[1], data[2], data[3], data[4], data[5], data[6], data[7], data[8], data[9], data[10], data[11]]
                        // data: [jan, feb]
                    }],

                });
            });
          });

        $(document).ready(function() {
            $('body').tooltip({
                selector: '[data-toggle=tooltip]'
            });
$(document).on('click', '#btn_back', function(){
             window.history.back();
            })

          $.get("http://localhost/winnerhelpdesk/index.php/WEB/ReportBiro", function(data, status){
                console.log(data, status)
                // dataa = $.parseJSON(data);
                // console.log(dataa)
                if (status) {
                  labels = []
                  series = []
                  data.forEach(element => {
                    var nama_biro = element.nama_biro
                    var jml = element.jml
                    labels.push(nama_biro + " : " + jml)
                    series.push(jml)
                  });
                  console.log (labels, series)
                  dataDailySalesChart = {
                    labels: labels,
                    series: [
                      series
                    ]
                  };
                  
                  

                  optionsDailySalesChart = {
                    lineSmooth: Chartist.Interpolation.cardinal({
                      tension: 0
                    }),
                    low: 1,
                    high: 15, // creative tim: we recommend you to set the high sa the biggest value + something for a better look
                    chartPadding: {
                      top: 10,
                      right: 0,
                      bottom: 0,
                      left: 0
                    },
                  }

                  var dailySalesChart = new Chartist.Line('#dailySalesChart', dataDailySalesChart, optionsDailySalesChart);

                  md.startAnimationForLineChart(dailySalesChart);
                }
                else {
                    alert('Load Data Failed')
                }
            });

          $.get("http://localhost/winnerhelpdesk/index.php/WEB/ReportKategori", function(data, status){
                console.log(data, status)
                if (status) {
                  labelsK = []
                  seriesK = []
                  data.forEach(element => {
                    var nama_kategori = element.nama_kategori
                    var jml = element.jml
                    labelsK.push(nama_kategori + " : " + jml)
                    seriesK.push(jml)
                  });
                  console.log (labelsK, seriesK)
                  dataCompletedTasksChart = {
                    labels: labelsK,
                    series: [
                      seriesK
                    ]
                  };

                  optionsCompletedTasksChart = {
                    lineSmooth: Chartist.Interpolation.cardinal({
                      tension: 0
                    }),
                    low: 1,
                    high: 15, // creative tim: we recommend you to set the high sa the biggest value + something for a better look
                    chartPadding: {
                      top: 0,
                      right: 0,
                      bottom: 0,
                      left: 0
                    }
                  }

                  var completedTasksChart = new Chartist.Bar('#completedTasksChart', dataCompletedTasksChart, optionsCompletedTasksChart);

                  // start animation for the Completed Tasks Chart - Bar Chart
                  md.startAnimationForBarChart(completedTasksChart);
                }
                else {
                    alert('Load Data Failed')
                }
            });

          
        });
      </script>