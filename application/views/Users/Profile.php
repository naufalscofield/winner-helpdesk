
<div class="main-panel">
      <!-- Navbar -->
      <nav class="navbar navbar-expand-lg navbar-transparent navbar-absolute fixed-top ">
        <div class="container-fluid">
          <div class="navbar-wrapper">
 <li id="btn_back" class="fa fa-chevron-circle-left fa-2x"></li>
            <a class="navbar-brand" href="#pablo">User Profile</a>
          </div>
          <button class="navbar-toggler" type="button" data-toggle="collapse" aria-controls="navigation-index" aria-expanded="false" aria-label="Toggle navigation">
            <span class="sr-only">Toggle navigation</span>
            <span class="navbar-toggler-icon icon-bar"></span>
            <span class="navbar-toggler-icon icon-bar"></span>
            <span class="navbar-toggler-icon icon-bar"></span>
          </button>
          <div class="collapse navbar-collapse justify-content-end">
            <form class="navbar-form">
              <?= $this->session->userdata('nm_peg');?>
            </form>
            <ul class="navbar-nav">
              <li class="nav-item dropdown">
                <a class="nav-link" href="#pablo" id="navbarDropdownProfile" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                  <i class="material-icons">person</i>
                  <p class="d-lg-none d-md-block">
                    Account
                  </p>
                </a>
                <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdownProfile">
                  <a class="dropdown-item" href="<?=base_url();?>index.php/admin/logout">Log out</a>
                </div>
              </li>
            </ul>
          </div>
        </div>
      </nav>
<div class="content">
  <div class="container-fluid">
    <div class="row">
      <div class="col-md-8">
        <div class="card">
          <div class="card-header card-header-primary">
            <h4 class="card-title">Edit Profile</h4>
            <p class="card-category">Complete your profile</p>
          </div>
          <div class="card-body">
          <?php foreach ($user as $data) { 
              ?>
              <div class="row">
                <div class="col-md-3">
                  <div class="form-group">
                    <label class="bmd-label-floating">NIP</label>
                    <input id="nip" required type="text" value="<?= $data['nip'];?>" class="form-control">
                    <input id="id" required type="hidden" value="<?= $data['id'];?>" class="form-control">
                  </div>
                </div>
                <div class="col-md-5">
                  <div class="form-group">
                    <label class="bmd-label-floating">Password</label>
                    <input id="pw" required type="password" value="<?= $data['pw'];?>" class="form-control">
                  </div>
                </div>
                <div class="col-md-4">
                  <div class="form-group">
                    <label class="bmd-label-floating">Email address</label>
                    <input id="email" required type="email" class="form-control" value="<?= $data['email'];?>">
                  </div>
                </div>
              </div>
              <div class="row">
                <div class="col-md-8">
                  <div class="form-group">
                    <label class="bmd-label-floating">Nama</label>
                    <input id="nm_peg" required type="text" value="<?= $data['nm_peg'];?>" class="form-control">
                  </div>
                </div>
                <div class="col-md-4">
                  <div class="form-group">
                    <label class="bmd-label-floating">Handphone</label>
                    <input id="handphone" required type="text" value="<?= $data['handphone'];?>" class="form-control">
                  </div>
                </div>
              </div>  
              <div class="row">
                <div class="col-md-3">
                  <div class="form-group">
                    <label class="bmd-label-floating">Kodepos</label>
                    <input id="kodepos" required type="text" value="<?= $data['kodepos'];?>" class="form-control">
                  </div>
                </div>
                <div class="col-md-4">
                  <div class="form-group">
                    <label class="bmd-label-floating">Telepon</label>
                    <input id="telepon" required type="text" value="<?= $data['telepon'];?>" class="form-control">
                  </div>
                </div>
                <div class="col-md-5">
                  <div class="form-group">
                    <label class="bmd-label-floating">Nama Jabatan</label>
                    <input id="nm_jabatan" required type="text" value="<?= $data['nm_jabatan'];?>" class="form-control">
                  </div>
                </div>
              </div>
              <div class="row">
                <div class="col-md-1">
                  <div class="form-group">
                    <label class="bmd-label-floating">Kode Jabatan</label>
                    <input id="kd_jabatan" required type="text" value="<?= $data['kd_jabatan'];?>" class="form-control">
                  </div>
                </div>
                <div class="col-md-2">
                  <div class="form-group">
                    <label class="bmd-label-floating">Kode Unit Org</label>
                    <input id="kd_unit_org" required type="text" value="<?= $data['kd_unit_org'];?>" class="form-control">
                  </div>
                </div>
                <div class="col-md-9">
                  <div class="form-group">
                    <label class="bmd-label-floating">Nama Unit Org</label>
                    <input id="nm_unit_org" required type="text" value="<?= $data['nm_unit_org'];?>" class="form-control">
                  </div>
                </div>
              </div>
                
              <div class="row">
                <div class="col-md-6">
                  <div class="form-group">
                    <label class="bmd-label-floating">Status</label>
                    <select required id="status" name="status" class="form-control" id="status">
                      <option value="">-- Pilih Status--</option>
                      <option value="aktif">Aktif</option>
                      <option value="nonaktif">Nonaktif</option>
                    </select>
                  </div>
                </div>
                <div class="col-md-6">
                  <div class="form-group">
                    <label class="bmd-label-floating">Jenis Kelamin</label>
                    <select required name="jns_kelamin_peg" id="jns_kelamin_peg" class="form-control" id="jns_kelamin_peg">
                      <option value="">-- Pilih Jenis Kelamin--</option>
                      <option value="pria">Pria</option>
                      <option value="wanita">Wanita</option>
                    </select></div>
                </div>
              </div>
              <div class="row">
                <div class="col-md-12">
                  <div class="form-group">
                    <label>Alamat</label>
                    <div class="form-group">
                      <textarea id="alamat" required class="form-control" rows="5"><?= $data['alamat'];?></textarea>
                    </div>
                  </div>
                </div>
              </div>
              <button id="btn_update" type="click" class="btn btn-primary pull-right">Update Profile</button>
              <div class="clearfix"></div>
          <?php } ?>
          </div>
        </div>
      </div>
<script>
$(document).on('click', '#btn_update' ,function(){
  var nip                       = $("#nip").val();
  var id                        = $("#id").val();
  var pw                        = $("#pw").val();
  var email                     = $("#email").val();
  var nm_peg                    = $("#nm_peg").val();
  var handphone                 = $("#handphone").val();
  var jns_kelamin_peg           = $("#jns_kelamin_peg").val();
  var kodepos                   = $("#kodepos").val();
  var telepon                   = $("#telepon").val();
  var nm_jabatan                = $("#nm_jabatan").val();
  var kd_jabatan                = $("#kd_jabatan").val();
  var kd_unit_org               = $("#kd_unit_org").val();
  var nm_unit_org               = $("#nm_unit_org").val();
  var status                    = $("#status").val();
  var alamat                    = $("#alamat").val();

  // if ( (nip == '') || (id == '') || (pw == '') || (email == '') || (nm_peg == '') || (handphone == '') || (jns_kelamin_peg == '') || (kodepos == '') || (telepon == '') || (nm_jabatan == '') ||  (kd_jabatan == '') || (kd_unit_org == '') || (id_biro == '') || (status == '') || (admin == '') || (alamat == '') ){
  if ( (nip == '') || (id == '') || (pw == '') || (email == '') || (nm_peg == '') || (jns_kelamin_peg == '') || (status == '') || (alamat == '') ){
    toastr.error("Harap Lengkapi data")
  } else {
  var data = {
      nip             : nip,
      id              : id,
      pw              : pw,
      email           : email,
      nm_peg          : nm_peg,
      handphone       : handphone,
      jns_kelamin_peg : jns_kelamin_peg,
      kodepos         : kodepos,
      telepon         : telepon,
      nm_jabatan      : nm_jabatan,
      kd_jabatan      : kd_jabatan,
      kd_unit_org     : kd_unit_org,
      nm_unit_org     : nm_unit_org,
      status          : status,
      alamat          : alamat,
      }
  // console.log(data)

  jQuery.ajax({
  url: 'http://localhost/winnerhelpdesk/index.php/WEB/profileUsers/' + id,
  type: 'PUT',
  data : data,
  success: function(data, status) {
    console.log(data, status)
    if (status){
      toastr.success(data.message)
      toastr.info(data.message2)
      toastr.options = {
        "showDuration": "500"
      }
      window.location.href="http://localhost/winnerhelpdesk/index.php/login"
    } else {
      toastr.error(data.message)
    }
      }
    });
  }


});
</script>