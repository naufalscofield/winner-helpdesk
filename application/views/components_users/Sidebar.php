<?php
$uri = $this->uri->segment(2);
?>
<body class="">
  <div class="wrapper ">
    <div class="sidebar" data-color="purple" data-background-color="white" data-image="<?=base_url();?>assets/main_bootstrap/img/sidebar-1.jpg">
      <!--
        Tip 1: You can change the color of the sidebar using: data-color="purple | azure | green | orange | danger"

        Tip 2: you can also add an image using data-image tag
    -->
      <div class="logo">
        <a href="http://www.creative-tim.com" class="simple-text logo-normal">
          <img src="<?=base_url();?>assets/main_bootstrap/img/logo-wika.png" style="height:85px;width:220px" alt="">
        </a>
      </div>
      <div class="sidebar-wrapper">
        <ul class="nav">

        <?php if ($uri == ''){ ?>
              <li class="nav-item active">
          <?php } else { ?> 
              <li class="nav-item">
          <?php } ?>    
            <a class="nav-link" href="<?=base_url();?>index.php/users">
              <!-- <i class="material-icons">dashboard</i> -->
              <i class="fa fa-area-chart">dashboard</i>
              <p>Dashboard & Charts</p>
            </a>
          </li>

          <?php if ($uri == 'getProfile'){ ?>
              <li class="nav-item active">
          <?php } else { ?> 
              <li class="nav-item">
          <?php } ?>  
            <a class="nav-link" href="<?=base_url();?>index.php/users/getProfile">
              <i class="material-icons">person</i>
              <p>User Profile</p>
            </a>
          </li>
         
          <?php if ($uri == 'getTickets'){ ?>
              <li class="nav-item active">
          <?php } else { ?> 
              <li class="nav-item">
          <?php } ?>    
            <a class="nav-link" href="<?=base_url();?>index.php/users/getTickets">
              <i class="material-icons">content_paste</i>
              <?php 
              // $i = $this->session->userdata('id_biro');
              // $q = $this->db->get_where('tickets',array('status' => 'open','id_biro' => $i));
              // $notif = $q->num_rows();
              ?>
              <p>Tickets </p>
            </a>
          </li>

          <!-- <?php if ( ($this->session->userdata('admin') == 'yes') && ($this->session->userdata('superadmin') == 'yes') ) { ?> -->
            <?php if ($uri == 'getKritik'){ ?>
              <li class="nav-item active">
          <?php } else { ?> 
              <li class="nav-item">
          <?php } ?>    
            <a class="nav-link" href="<?=base_url();?>index.php/User/getKritik">
            <i class="fa fa-commenting" aria-hidden="true"></i>
              <p>Kritik</p>
            </a>
          </li>
          <!-- <?php } ?> -->

        </ul>
      </div>
    </div>