<!DOCTYPE html>
<html lang="en">
<head>
	<title>Wika Industri Energi Helpdesk</title>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
<!--===============================================================================================-->	
	<link rel="icon" type="image/png" href="<?=base_url();?>assets/login_bootstrap/images/icons/favicon.ico"/>
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="<?=base_url();?>assets/login_bootstrap/vendor/bootstrap/css/bootstrap.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="<?=base_url();?>assets/login_bootstrap/fonts/font-awesome-4.7.0/css/font-awesome.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="<?=base_url();?>assets/login_bootstrap/fonts/Linearicons-Free-v1.0.0/icon-font.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="<?=base_url();?>assets/login_bootstrap/vendor/animate/animate.css">
<!--===============================================================================================-->	
	<link rel="stylesheet" type="text/css" href="<?=base_url();?>assets/login_bootstrap/vendor/css-hamburgers/hamburgers.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="<?=base_url();?>assets/login_bootstrap/vendor/animsition/css/animsition.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="<?=base_url();?>assets/login_bootstrap/vendor/select2/select2.min.css">
<!--===============================================================================================-->	
	<link rel="stylesheet" type="text/css" href="<?=base_url();?>assets/login_bootstrap/vendor/daterangepicker/daterangepicker.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="<?=base_url();?>assets/login_bootstrap/css/util.css">
	<link rel="stylesheet" type="text/css" href="<?=base_url();?>assets/login_bootstrap/css/main.css">
	<link href="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/css/toastr.min.css" rel="stylesheet">
<!--===============================================================================================-->
</head>
<body>
	
	<div class="limiter">
		<div class="container-login100">
			<div class="wrap-login100">
				<div class="login100-form-title" style="background-image: url(<?=base_url();?>assets/login_bootstrap/images/bg-02.jpg);">
					<span class="login100-form-title-1">
						Login
					</span>
				</div>

				<!-- <form class="login100-form validate-form" id="form_login"> -->
					<div class="wrap-input100 validate-input m-b-26" data-validate="Nip is required">
						<span class="label-input100">NIP</span>
						<input class="input100" type="text" name="nip" id= "nip" placeholder="NIP">
						<span class="focus-input100"></span>
						<span class="symbol-input100">
							<i class="fa fa-envelope" aria-hidden="true"></i>
						</span>
					</div>

					<div class="wrap-input100 validate-input m-b-18" data-validate = "Password is required">
						<span class="label-input100">Password</span>
						<input class="input100" type="password" name="password" id= "password" placeholder="Password">
						<span class="focus-input100"></span>
						<span class="symbol-input100">
							<i class="fa fa-envelope" aria-hidden="true"></i>
						</span>
					</div>

					<div class="container-login100-form-btn center">
					<center><button class="login100-form-btn" id="btn_login">
							Login
						</button></center>
					</div>
						<br>
			</div>
		</div>
	</div>
	
<!--===============================================================================================-->
	<script src="<?=base_url();?>assets/login_bootstrap/vendor/jquery/jquery-3.2.1.min.js"></script>
<!--===============================================================================================-->
	<script src="<?=base_url();?>assets/login_bootstrap/vendor/animsition/js/animsition.min.js"></script>
<!--===============================================================================================-->
	<script src="<?=base_url();?>assets/login_bootstrap/vendor/bootstrap/js/popper.js"></script>
	<script src="<?=base_url();?>assets/login_bootstrap/vendor/bootstrap/js/bootstrap.min.js"></script>
<!--===============================================================================================-->
	<script src="<?=base_url();?>assets/login_bootstrap/vendor/select2/select2.min.js"></script>
<!--===============================================================================================-->
	<script src="<?=base_url();?>assets/login_bootstrap/vendor/daterangepicker/moment.min.js"></script>
	<script src="<?=base_url();?>assets/login_bootstrap/vendor/daterangepicker/daterangepicker.js"></script>
<!--===============================================================================================-->
	<script src="<?=base_url();?>assets/login_bootstrap/vendor/countdowntime/countdowntime.js"></script>
<!--===============================================================================================-->
	<script src="<?=base_url();?>assets/login_bootstrap/js/main.js"></script>
	<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/js/toastr.min.js"></script>

</body>
</html>

<script>

$(document).on('click', '#btn_login' ,function(){
	var nip = $('#nip').val();
	var password = $('#password').val();

	if ((nip == '') || (password == '')){
		toastr.error('Harap isi NIP dan Password!')
	} else {
		$.post("http://localhost/winnerhelpdesk/index.php/WEB/Login", {
			nip: nip,
			password: password
		}, function(data, status){
			console.log(data, status)
			if (data.status) {
				toastr.success(data.message)
				window.location.href="http://localhost/winnerhelpdesk/index.php/user/"				
				} else {
				toastr.error(data.message)
			}
		});
	}
});
	

</script>