<div class="main-panel">
      <!-- Navbar -->
      <nav class="navbar navbar-expand-lg navbar-transparent navbar-absolute fixed-top ">
        <div class="container-fluid">
          <div class="navbar-wrapper">
 <li id="btn_back" class="fa fa-chevron-circle-left fa-2x"></li>
            <a class="navbar-brand" href="#pablo">Tickets</a>
          </div>
          <button class="navbar-toggler" type="button" data-toggle="collapse" aria-controls="navigation-index" aria-expanded="false" aria-label="Toggle navigation">
            <span class="sr-only">Toggle navigation</span>
            <span class="navbar-toggler-icon icon-bar"></span>
            <span class="navbar-toggler-icon icon-bar"></span>
            <span class="navbar-toggler-icon icon-bar"></span>
          </button>
          <div class="collapse navbar-collapse justify-content-end">
            <form class="navbar-form">
              <?= $this->session->userdata('nm_peg');?>
              <input type="hidden" id="id_biro_sess" value="<?= $this->session->userdata('id_biro');?>">
            </form> 
            <ul class="navbar-nav">
              <li class="nav-item dropdown">
                <a class="nav-link" href="#pablo" id="navbarDropdownProfile" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                  <i class="material-icons">person</i>
                  <p class="d-lg-none d-md-block">
                    Account
                  </p>
                </a>
                <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdownProfile">
                  <a class="dropdown-item" href="<?=base_url();?>index.php/admin/logout">Log out</a>
                </div>
              </li>
            </ul>
          </div>
        </div>
      </nav>
      <!-- End Navbar -->
      <div class="content">
        <div class="container-fluid">
          <div class="row">
            <div class="col-md-12">
              <div class="card">
                <div class="card-header card-header-primary">
                  <h4 class="card-title ">Daftar Ticket</h4>
                </div>
                    <div class="col-md-12">
                      <div class="form-group">
                        <select name="" class="form-control" id="myInput" onchange="myFunction()">
                          <option value="">-- Filter Status --</option>
                          <option value="open">Open</option>
                          <option value="work_progress">Work Progress</option>
                          <option value="closed">Closed</option>
                          <option value="done">Done</option>
                        </select>
                      </div>
                    </div>
                    <div class="col-md-12">
                      <div class="form-group">
                        <select name="" class="form-control" id="myInput2" onchange="myFunction2()">
                          <option value="">-- Filter Priority --</option>
                          <option value="low">Low</option>
                          <option value="normal">Normal</option>
                          <option value="urgent">Urgent</option>
                        </select>
                      </div>
                      <?php 
                      $i = $this->session->userdata('id_biro');
                      $q = $this->db->get_where('tickets',array('status' => 'open','id_biro' => $i, 'deleted_at' => null));
                      $notif = $q->num_rows();
                      ?>
                    <button class="btn bnt-sm btn-info">Open = <?= $notif;?><span id="sopen"></span></button>
                      <?php 
                      $i = $this->session->userdata('id_biro');
                      $q = $this->db->get_where('tickets',array('status' => 'work_progress','id_biro' => $i, 'deleted_at' => null));
                      $notif = $q->num_rows();
                      ?>
                    <button class="btn bnt-sm btn-success">Work Progress = <?= $notif;?><span id="swp"></span></button>
                      <?php 
                      $i = $this->session->userdata('id_biro');
                      $q = $this->db->get_where('tickets',array('status' => 'closed','id_biro' => $i, 'deleted_at' => null));
                      $notif = $q->num_rows();
                      ?>
                    <button class="btn bnt-sm btn-warning">Closed = <?= $notif;?><span id="sclo"></span></button>
                      <?php 
                      $i = $this->session->userdata('id_biro');
                      $q = $this->db->get_where('tickets',array('status' => 'done','id_biro' => $i, 'deleted_at' => null));
                      $notif = $q->num_rows();
                      ?>
                    <button class="btn bnt-sm btn-danger">Done = <?= $notif;?><span id="sdone"></span></button>
 
                    </div>
                <div class="card-body">
                  <div class="table-responsive">
                    <table id="example" class="table">
                      <thead class=" text-primary">
                        <tr>
                            <th width="5"><center><b>No</b></center></th>
                            <th><center><b>User</b></center></th>                  
                            <th><center><b>Biro</b></center></th>                  
                            <th><center><b>Kategori</b></center></th>                  
                            <th><center><b>Priority</b></center></th>                  
                            <th><center><b>Tanggal Open</b></center></th>                  
                            <th><center><b>Tanggal Close</b></center></th>                  
                            <th><center><b>Status</b></center></th>                  
                            <th><center><b>Aksi</b></center></th>
                        </tr>
                    </thead>
                    <tbody id="table-row">
                       
                    </tbody>
                    </table>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div id="ticketMessage" class="modal" tabindex="-1" role="dialog">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                <div class="modal-header">
                <h5 class="modal-title">Ticket Message</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
                    <div class="col-md-12">
                                <div class="form-group" id="modal_body">
                <div class>
                        <div class="row">
                          <input type="hidden" name="id_staff" id="id_staff" value="<?= $this->session->userdata('id');?>">
                          <input type="hidden" name="id_ticket_input" id="id_ticket_input" value="<?= $this->session->userdata('id');?>">
                            <div class="col-md-12">
                                <div class="form-group" id="body_modal">
                                 </div>
                            </div>
                            
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label>Pesan</label>
                                    <textarea class="form-control border-input" name="pesan" id="pesan" cols="30" rows="10"></textarea>
                                 </div>
                            </div>
                        </div>
                    
                    <div class="clearfix"></div>
                        <div class="modal-footer">
                            <button id="btn_submit" type="" class="btn btn-success btn-fill btn-wd">
                                Kirim Pesan
                            </button>
                            <button type="submit" class="btn btn-secondary" data-dismiss="modal">Tutup</button>
                            </div>
                        </div>
                </div>
            </div>
        </div>
        <script>
            function myFunction() {
            // Declare variables
            var input, filter, table, tr, td, i, txtValue;
            input = document.getElementById("myInput");
            filter = input.value.toUpperCase();
            table = document.getElementById("example");
            tr = table.getElementsByTagName("tr");

            // Loop through all table rows, and hide those who don't match the search query
                for (i = 0; i < tr.length; i++) {
                    td = tr[i].getElementsByTagName("td")[7];
                    if (td) {
                    txtValue = td.textContent || td.innerText;
                    if (txtValue.toUpperCase().indexOf(filter) > -1) {
                        tr[i].style.display = "";
                    } else {
                        tr[i].style.display = "none";
                        }
                    }
                }
            }
            function myFunction2() {  
            // Declare variables
            var input, filter, table, tr, td, i, txtValue;
            input = document.getElementById("myInput2");
            filter = input.value.toUpperCase();
            table = document.getElementById("example");
            tr = table.getElementsByTagName("tr");

            // Loop through all table rows, and hide those who don't match the search query
                for (i = 0; i < tr.length; i++) {
                    td = tr[i].getElementsByTagName("td")[4];
                    if (td) {
                    txtValue = td.textContent || td.innerText;
                    if (txtValue.toUpperCase().indexOf(filter) > -1) {
                        tr[i].style.display = "";
                    } else {
                        tr[i].style.display = "none";
                        }
                    }
                }
            }
        </script>
      <script>
      function getTickets() {
        $('#sopen').val('a')
          var id_biro = $('#id_biro_sess').val();
            $.get("http://localhost/winnerhelpdesk/index.php/WEB/TicketsBiro/" + id_biro, function(data, status){
                console.log(data, status)
                if (status) {
                    $('#table-row').html("")
                    let no = 0;
                    data.forEach(element => {
                    var t = $('#example').DataTable();
                    no++;
                    if(element.status_ticket == 'open') {
                      t.row.add( [
                          "<center>"+no+"</center>",
                          "<center>"+element.nm_peg+"</center>",
                          "<center>"+element.nama_biro+"</center>",
                          "<center>"+element.nama_kategori+"</center>",
                          "<center>"+element.priority+"</center>",
                          "<center>"+element.tanggal_open+"</center>",
                          "<center>"+element.tanggal_close+"</center>",
                          "<center>"+element.status_ticket+"</center>",
                          "<center><a id='btn_delete' data-id="+element.id_ticket+" class='btn btn-sm btn-danger'><i class='fa fa-trash'></i></a><a id='btn_wp' data-idwp="+element.id_ticket+" class='btn btn-sm btn-success'><i class='fa fa-spinner'></i></a><a id='btn_message' data-idmessage="+element.id_ticket+" class='btn btn-sm btn-info' data-toggle='modal' data-target='#ticketMessage'><i class='fa fa-eye'></i></a></center>"
                      ] ).draw( false );
                      } else if (element.status_ticket == 'work_progress') {
                        t.row.add( [
                          "<center>"+no+"</center>",
                          "<center>"+element.nm_peg+"</center>",
                          "<center>"+element.nama_biro+"</center>",
                          "<center>"+element.nama_kategori+"</center>",
                          "<center>"+element.priority+"</center>",
                          "<center>"+element.tanggal_open+"</center>",
                          "<center>"+element.tanggal_close+"</center>",
                          "<center>"+element.status_ticket+"</center>",
                          "<center><a id='btn_delete' data-id="+element.id_ticket+" class='btn btn-sm btn-danger'><i class='fa fa-trash'></i></a><a id='btn_closed' data-idclosed="+element.id_ticket+" class='btn btn-sm btn-warning'><i class='fa fa-window-close'></i></a><a id='btn_message' data-idmessage="+element.id_ticket+" class='btn btn-sm btn-info' data-toggle='modal' data-target='#ticketMessage'><i class='fa fa-eye'></i></a></center>"
                      ] ).draw( false );
                      } else {
                        t.row.add( [
                          "<center>"+no+"</center>",
                          "<center>"+element.nm_peg+"</center>",
                          "<center>"+element.nama_biro+"</center>",
                          "<center>"+element.nama_kategori+"</center>",
                          "<center>"+element.priority+"</center>",
                          "<center>"+element.tanggal_open+"</center>",
                          "<center>"+element.tanggal_close+"</center>",
                          "<center>"+element.status_ticket+"</center>",
                          "<center><a id='btn_delete' data-id="+element.id_ticket+" class='btn btn-sm btn-danger'><i class='fa fa-trash'></i></a><a id='btn_message' data-idmessage="+element.id_ticket+" class='btn btn-sm btn-info' data-toggle='modal' data-target='#ticketMessage'><i class='fa fa-eye'></i></a></center>"
                      ] ).draw( false );
                      }
                    });
                
                } else {
                    alert('Load Data Failed')
                }
            });
        }

            function clearTable() {
                var table = $('#example').DataTable();
                table
                    .clear()
                    .draw();
            }
            
            function clearModalMessage(){
                // $('#nama_Tickets_u').val('');
            }

   ////////////////////////////////////document ready/////////////////////////////////////////////////////////////     
        $(document).ready(function() {
            $('body').tooltip({
                selector: '[data-toggle=tooltip]'
            });
$(document).on('click', '#btn_back', function(){
             window.history.back();
            })

            $('#example').DataTable( {
                dom: 'Bfrtip',
                buttons: [
                    'copy', 'csv', 'excel', 'pdf', 'print'
                ]
            } );

            getTickets()

        } );

            $(document).on('click', '#btn_submit' ,function(){
                var id_staff                   = $('#id_staff').val();
                var pesan                      = $('#pesan').val();
                var id_ticket                  = $('#id_ticket_input').val();

            
                $.post("http://localhost/winnerhelpdesk/index.php/WEB/Message", {
                    id_staff                   : id_staff,
                    pesan                      : pesan,
                    id_ticket                  : id_ticket,
                    },
                    function(data, status){
                    if (status) {
                        toastr.success('Submit Pesan Success')
                        clearTable()
                        $('#ticketMessage').modal('hide')
                        // clearModalAdd()
                        getTickets()
                    }
                    else {
                        toastr.error('Submit Pesan Failed!')
                    }
                });
                
            });

            $(document).on('click', '#btn_message' ,function(){
                  var $el = $("#body_modal");
                  $el.empty(); // remove old options
                // clearModalMessage()
                var id =  $(this).data("idmessage");
                $("#id_ticket_input").val(id)
                $.ajax({
                type: "POST",
                data: { id_ticket: id },
                url: '<?php echo base_url()."index.php/users/getMessage" ?>',
                dataType: 'text',
                success: function(resp) {
                  console.log(resp)
                  var json = JSON.parse(resp.replace(',.', ''))
                  var $el = $("#body_modal");
                  // $el.empty(); // remove old options
                  $.each(json, function(key, value) {
                    if (value.nama_user == null) {
                    $el.append($("<textarea readonly class='form-control border-input'></textarea><br>").text(value.pesan + ' //' + value.nama_staff+ '-'+ value.tanggal));
                    } else {
                    $el.append($("<textarea readonly class='form-control border-input'></textarea><br>").text(value.pesan + ' //' + value.nama_user+ '-'+ value.tanggal));
                    }
                  });
                },
                error: function (jqXHR, exception) {
                  console.log(jqXHR, exception)
                }
              });
            });

            $(document).on('click', '#btn_wp' ,function(){
                var id =  $(this).data("idwp");
                console.log(id)
                var data = {
                    status : 'work_progress',
                    }

                jQuery.ajax({
                url: 'http://localhost/winnerhelpdesk/index.php/WEB/Tickets/' + id,
                type: 'PUT',
                data : data,
                success: function(data) {
                    clearTable()  
                    getTickets();
                    toastr.success('Update status ticket success!')
                    location.reload();
                    }
                });

            });

            $(document).on('click', '#btn_closed' ,function(){
                var id =  $(this).data("idclosed");
                console.log(id)
                var data = {
                    status : 'closed',
                    }

                jQuery.ajax({
                url: 'http://localhost/winnerhelpdesk/index.php/WEB/Tickets/' + id,
                type: 'PUT',
                data : data,
                success: function(data) {
                    clearTable()  
                    getTickets();
                    toastr.success('Update status ticket success!')
                    location.reload();
                    }
                });

            });

            $(document).on('click', '#btn_delete' ,function(){
                var id =  $(this).data("id");
                console.log(id)
                var data = {
                    status : 'deleted',
                    }

                jQuery.ajax({
                url: 'http://localhost/winnerhelpdesk/index.php/WEB/Tickets/' + id,
                type: 'PUT',
                data : data,
                success: function(data) {
                    clearTable()  
                    getTickets();
                    toastr.success('Delete ticket success!')
                    location.reload();
                    }
                });

            });
            // $(document).on('click', '#btn_status' ,function(){
            //     var id =  $(this).data("idstatus");
            //     console.log(id)
            //     var data = {
            //         status : closed,
            //         }

            //     jQuery.ajax({
            //     url: 'http://localhost/winnerhelpdesk/index.php/WEB/Tickets/' + id,
            //     type: 'PUT',
            //     data : data,
            //     success: function(data) {
            //         $('.modal').modal('hide')
            //         clearModalMessage()
            //         clearTable()
            //         getTickets();
            //         toastr.success('Update Data Success!')
            //         }
            //     });

            // });
           

        
      </script>