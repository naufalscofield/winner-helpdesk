<div class="main-panel">
      <!-- Navbar -->
      <nav class="navbar navbar-expand-lg navbar-transparent navbar-absolute fixed-top ">
        <div class="container-fluid">
          <div class="navbar-wrapper">
 <li id="btn_back" class="fa fa-chevron-circle-left fa-2x"></li>
            <a class="navbar-brand" href="#pablo">Settings</a>
          </div>
          <button class="navbar-toggler" type="button" data-toggle="collapse" aria-controls="navigation-index" aria-expanded="false" aria-label="Toggle navigation">
            <span class="sr-only">Toggle navigation</span>
            <span class="navbar-toggler-icon icon-bar"></span>
            <span class="navbar-toggler-icon icon-bar"></span>
            <span class="navbar-toggler-icon icon-bar"></span>
          </button>
          <div class="collapse navbar-collapse justify-content-end">
            <form class="navbar-form">
              <?= $this->session->userdata('nm_peg');?>
            </form>
            <ul class="navbar-nav">
              <li class="nav-item dropdown">
                <a class="nav-link" href="#pablo" id="navbarDropdownProfile" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                  <i class="material-icons">person</i>
                  <p class="d-lg-none d-md-block">
                    Account
                  </p>
                </a>
                <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdownProfile">
                  <a class="dropdown-item" href="<?=base_url();?>index.php/admin/logout">Log out</a>
                </div>
              </li>
            </ul>
          </div>
        </div>
      </nav>
      <!-- End Navbar -->
      <div class="content">
        <div class="container-fluid">
          <div class="row">
            <div class="col-md-12">
              <div class="card">
                <div class="card-header card-header-primary">
                  <h4 class="card-title ">Settings</h4>
                </div>
                <div class="card-body">
                  <div class="table-responsive">
                    <table id="example" class="table">
                      <thead class=" text-primary">
                        <tr>
                            <th><center><b>No Telp</b></center></th>                  
                            <th><center><b>No Broadcast</b></center></th>                  
                            <th><center><b>Email Perusahaan</b></center></th>                  
                            <th><center><b>Alamat Perusahaan</b></center></th>                  
                            <th><center><b>Aksi</b></center></th>
                        </tr>
                    </thead>
                    <tbody id="table-row">
                       
                    </tbody>
                    </table>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div id="editSettings" class="modal" tabindex="-1" role="dialog">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                        <div class="modal-header">
                        <h5 class="modal-title">Edit Settings</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                <div class="modal-body">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group" id="">
                                    <Label>No Telp Perusahaan</Label><br>
                                    <input required type="hidden" required name="id_u" id="id_u" class="form-control">
                                    <input required type="text" required name="no_telp_u" id="no_telp_u" class="form-control">
                                 </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group" id="">
                                    <Label>No Broadcast</Label><br>
                                    <input required type="text" required name="no_broadcast_u" id="no_broadcast_u" class="form-control">
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group" id="">
                                    <Label>Email Perusahaan</Label><br>
                                    <input required type="email" required name="email_perusahaan_u" id="email_perusahaan_u" class="form-control">
                                 </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group" id="">
                                    <Label>Alamat Perusahaan</Label><br>
                                    <textarea required name="alamat_perusahaan_u" id="alamat_perusahaan_u" cols="30" rows="10" class="form-control">
                                    </textarea>
                                </div>
                            </div>
                        </div>
                    
                    <div class="clearfix"></div>
                        <div class="modal-footer">
                            <button id="btn_update" type="" class="btn btn-success btn-fill btn-wd">
                                Update Settings
                            </button>
                            <button type="submit" class="btn btn-secondary" data-dismiss="modal">Tutup</button>
                            </div>
                        </div>
                </div>
            </div>
        </div>

      <script>
      function getSettings() {
            $.get("http://localhost/winnerhelpdesk/index.php/WEB/Settings", function(data, status){
                console.log(data, status)
                if (status) {
                    $('#table-row').html("")
                    data.forEach(element => {
                    var t = $('#example').DataTable();
                    t.row.add( [
                        "<center>"+element.no_telp+"</center>",
                        "<center>"+element.no_broadcast+"</center>",
                        "<center>"+element.email_perusahaan+"</center>",
                        "<center>"+element.alamat_perusahaan+"</center>",
                        "<center><a id='btn_edit' data-idedit="+element.id+" class='btn btn-info' data-toggle='modal' data-target='#editSettings'><i class='fa fa-eye'></i></a></center>"
                    ] ).draw( false );
                    });
                }
                else {
                    alert('Load Data Failed')
                }
            });
        }

            function clearTable() {
                var table = $('#example').DataTable();
                table
                    .clear()
                    .draw();
            }
            
            function clearModalEdit(){
                $('#no_telp_u').val('');
                $('#no_broadcast_u').val('');
                $('#email_perusahaan_u').val('');
                $('#alamat_perusahaan_u').val('');
            }

   ////////////////////////////////////document ready/////////////////////////////////////////////////////////////     
        $(document).ready(function() {
            $('body').tooltip({
                selector: '[data-toggle=tooltip]'
            });
$(document).on('click', '#btn_back', function(){
             window.history.back();
            })

            $('#example').DataTable( {
                dom: 'Bfrtip',
                buttons: [
                    'copy', 'csv', 'excel', 'pdf', 'print'
                ]
            } );

            getSettings()

        } );

            $(document).on('click', '#btn_edit' ,function(){
                clearModalEdit()
                console.log('t')
                // $('#editSettings').modal('show')
                var id =  $(this).data("idedit");

                $.ajax({
                    type: "GET",
                    url: 'http://localhost/winnerhelpdesk/index.php/WEB/Settings/'+id,
                    dataType: 'json',
                    success: function(resp) {
                    // console.log(resp)
                    $('#id_u').val(resp.id);
                    $('#no_telp_u').val(resp.no_telp);
                    $('#no_broadcast_u').val(resp.no_broadcast);
                    $('#email_perusahaan_u').val(resp.email_perusahaan);
                    $('#alamat_perusahaan_u').val(resp.alamat_perusahaan);

                    },
                    error: function (jqXHR, exception) {
                    }
                });
            });

            $(document).on('click', '#btn_update' ,function(){
                var id                              = $('#id_u').val()
                var no_telp                         = $('#no_telp_u').val()
                var no_broadcast                    = $('#no_broadcast_u').val()
                var email_perusahaan                = $('#email_perusahaan_u').val()
                var alamat_perusahaan               = $('#alamat_perusahaan_u').val()
                console.log(id)

                if ( (id == '') || (no_telp == '') || (no_broadcast == '') || (email_perusahaan == '') || (alamat_perusahaan == '') ) {
                    toastr.error('Harap lengkapi data!')
                } else {
                    var data = {
                        no_telp : no_telp,
                        no_broadcast : no_broadcast,
                        email_perusahaan : email_perusahaan,
                        alamat_perusahaan : alamat_perusahaan,
                        }

                    jQuery.ajax({
                    url: 'http://localhost/winnerhelpdesk/index.php/WEB/Settings/' + id,
                    type: 'PUT',
                    data : data,
                    success: function(data) {
                        $('#editSettings').modal('hide')
                        clearModalEdit()
                        clearTable()
                        getSettings();
                        toastr.success('Update Data Success!')
                        }
                    });
                }
            });

        
      </script>