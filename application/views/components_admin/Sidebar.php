<?php
$uri = $this->uri->segment(2);
?>
<body class="">
  <div class="wrapper ">
    <div class="sidebar" data-color="purple" data-background-color="white" data-image="<?=base_url();?>assets/main_bootstrap/img/sidebar-1.jpg">
      <!--
        Tip 1: You can change the color of the sidebar using: data-color="purple | azure | green | orange | danger"

        Tip 2: you can also add an image using data-image tag
    -->
      <div class="logo">
        <a href="http://www.creative-tim.com" class="simple-text logo-normal">
          <img src="<?=base_url();?>assets/main_bootstrap/img/logo-wika.png" style="height:85px;width:220px" alt="">
        </a>
      </div>
      <div class="sidebar-wrapper">
        <ul class="nav">

        <?php if ($uri == ''){ ?>
              <li class="nav-item active">
          <?php } else { ?> 
              <li class="nav-item">
          <?php } ?>    
            <a class="nav-link" href="<?=base_url();?>index.php/Admin">
              <i class="material-icons">dashboard</i>
              <p>Dashboard & Reports</p>
            </a>
          </li>

          <?php if ($uri == 'getProfile'){ ?>
              <li class="nav-item active">
          <?php } else { ?> 
              <li class="nav-item">
          <?php } ?>  
            <a class="nav-link" href="<?=base_url();?>index.php/Admin/getProfile">
              <i class="material-icons">person</i>
              <p>User Profile</p>
            </a>
          </li>

          <?php if ($uri == 'getUsers'){ ?>
              <li class="nav-item active">
          <?php } else { ?> 
              <li class="nav-item">
          <?php } ?>    
            <a class="nav-link" href="<?=base_url();?>index.php/Admin/getUsers">
              <i class="fa fa-users" aria-hidden="true"></i>
              <p>Users</p>
            </a>
          </li>
          
          <?php if ($uri == 'getStaff'){ ?>
              <li class="nav-item active">
          <?php } else { ?> 
              <li class="nav-item">
          <?php } ?>    
            <a class="nav-link" href="<?=base_url();?>index.php/Admin/getStaff">
              <i class="fa fa-user-secret" aria-hidden="true"></i>
              <p>Admin Biro</p>
            </a>
          </li>

          <?php if ($uri == 'getBiro'){ ?>
              <li class="nav-item active">
          <?php } else { ?> 
              <li class="nav-item">
          <?php } ?>    
            <a class="nav-link" href="<?=base_url();?>index.php/Admin/getBiro">
              <i class="material-icons">library_books</i>
              <p>Biro</p>
            </a>
          </li>

           <?php if ($uri == 'getKategori'){ ?>
              <li class="nav-item active">
          <?php } else { ?> 
              <li class="nav-item">
          <?php } ?>    
            <a class="nav-link" href="<?=base_url();?>index.php/Admin/getKategori">
              <i class="material-icons">bubble_chart</i>
              <p>Kategori</p>
            </a>
          </li>
         
          <?php if ($uri == 'getTickets'){ ?>
              <li class="nav-item active">
          <?php } else { ?> 
              <li class="nav-item">
          <?php } ?>    
            <a class="nav-link" href="<?=base_url();?>index.php/Admin/getTickets">
              <i class="material-icons">content_paste</i>
              <?php 
              $i = $this->session->userdata('id_biro');
              $q = $this->db->get_where('tickets',array('status' => 'open','id_biro' => $i, 'deleted_at' => null));
              $notif = $q->num_rows();
              ?>
              <p>Tickets In <span class="badge"><?= $notif;?></span></p>
            </a>
          </li>
          
          <?php if ($uri == 'getMyTicket'){ ?>
              <li class="nav-item active">
          <?php } else { ?> 
              <li class="nav-item">
          <?php } ?>    
            <a class="nav-link" href="<?=base_url();?>index.php/Admin/getMyTicket">
            <i class="fa fa-star" aria-hidden="true"></i>
              <p>My Ticket</p>
            </a>
          </li>
        
        <?php if ($this->session->userdata('admin') == 'yes') { ?>
          <?php if ($uri == 'getSettings'){ ?>
              <li class="nav-item active">
          <?php } else { ?> 
              <li class="nav-item">
          <?php } ?>    
            <a class="nav-link" href="<?=base_url();?>index.php/Admin/getSettings">
            <i class="fa fa-cogs" aria-hidden="true"></i>
              <p>Settings</p>
            </a>
          </li>
          <?php } ?>

            <!-- <?php if ( ($this->session->userdata('admin') == 'yes') && ($this->session->userdata('superadmin') == 'yes') ) { ?> -->
          <?php if ($uri == 'getKritik'){ ?>
              <li class="nav-item active">
          <?php } else { ?> 
              <li class="nav-item">
          <?php } ?>    
            <a class="nav-link" href="<?=base_url();?>index.php/User/getKritik">
            <i class="fa fa-commenting" aria-hidden="true"></i>
              <p>Kritik</p>
            </a>
          </li>
          <!-- <?php } ?> -->
          
        </ul>
      </div>
    </div>