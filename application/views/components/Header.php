<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8" />
  <script src="<?=base_url();?>assets/main_bootstrap/js/core/jquery.min.js"></script>
  <link rel="stylesheet" href="<?= base_url();?>assets/ladda/dist/ladda-themeless.min.css">
  <link href="https://cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css" rel="stylesheet" />
  <link href="https://cdn.datatables.net/buttons/1.5.6/css/buttons.dataTables.min.css" rel="stylesheet" />
  <link href="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/css/toastr.min.css" rel="stylesheet">
  <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/js/toastr.min.js"></script>
  <link rel="apple-touch-icon" sizes="76x76" href="<?=base_url();?>assets/main_bootstrap/img/apple-icon.png">
  <link rel="icon" type="image/png" href="<?=base_url();?>assets/main_bootstrap/img/favicon.png">
  <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
  <title>
    Helpdesk Wika Industri Energi
  </title>
  <meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0, shrink-to-fit=no' name='viewport' />
  <!--     Fonts and icons     -->
  <link rel="stylesheet" type="text/css" href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700|Roboto+Slab:400,700|Material+Icons" />
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/latest/css/font-awesome.min.css">
  <!-- CSS Files -->
  <link href="<?=base_url();?>assets/main_bootstrap/css/material-dashboard.css?v=2.1.1" rel="stylesheet" />
  <!-- CSS Just for demo purpose, don't include it in your project -->
  <link href="<?=base_url();?>assets/main_bootstrap/demo/demo.css" rel="stylesheet" />
</head>