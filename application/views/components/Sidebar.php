<?php
$uri = $this->uri->segment(2);
?>
<body class="">
  <div class="wrapper ">
    <div class="sidebar" data-color="purple" data-background-color="white" data-image="<?=base_url();?>assets/main_bootstrap/img/sidebar-1.jpg">
      <!--
        Tip 1: You can change the color of the sidebar using: data-color="purple | azure | green | orange | danger"

        Tip 2: you can also add an image using data-image tag
    -->
      <div class="logo">
        <a href="http://www.creative-tim.com" class="simple-text logo-normal">
          <img src="<?=base_url();?>assets/main_bootstrap/img/logo-winner-login.png" style="height:85px;width:220px" alt="">
        </a>
      </div>
      <div class="sidebar-wrapper">
        <ul class="nav">

        <?php if ($uri == ''){ ?>
              <li class="nav-item active">
          <?php } else { ?> 
              <li class="nav-item">
          <?php } ?>    
            <a class="nav-link" href="<?=base_url();?>index.php/User">
              <i class="fa fa-area-chart"></i>
              <p>Dashboard & Chart's</p>
            </a>
          </li>
                    
          <?php if ($this->session->userdata('level') ==  2) { ?>
          <?php if ($uri == 'getUsers'){ ?>
              <li class="nav-item active">
          <?php } else { ?> 
              <li class="nav-item">
          <?php } ?>    
            <a class="nav-link" href="<?=base_url();?>index.php/User/getUsers">
              <i class="fa fa-users" aria-hidden="true"></i>
              <p>Staff Biro</p>
            </a>
          </li>
          <?php } ?>
          
          <?php if ($this->session->userdata('level') ==  1) { ?>
          <?php if ($uri == 'getStaff'){ ?>
              <li class="nav-item active">
          <?php } else { ?> 
              <li class="nav-item">
          <?php } ?>    
            <a class="nav-link" href="<?=base_url();?>index.php/User/getStaff">
              <i class="fa fa-user-secret" aria-hidden="true"></i>
              <p>Admin Biro</p>
            </a>
          </li>
          <?php } ?>

          <?php if ($this->session->userdata('level') ==  1) { ?>
          <?php if ($uri == 'getBiro'){ ?>
              <li class="nav-item active">
          <?php } else { ?> 
              <li class="nav-item">
          <?php } ?>    
            <a class="nav-link" href="<?=base_url();?>index.php/User/getBiro">
              <i class="material-icons">library_books</i>
              <p>Biro</p>
            </a>
          </li>
          <?php } ?>

          <!-- <?php if ( ($this->session->userdata('level') == 1) || ($this->session->userdata('level') == 2)) { ?> -->
          <!-- <li data-toggle="collapse" data-target="#kategori" class="nav-item">
              <a class="nav-link" href="#">
              <i class="fa fa-sort-desc" aria-hidden="true"></i>
              <span class="arrow"></span>
              <p>Kategori </p> 
              </a>
          </li> -->

          <!-- <ul class="sub-menu collapse" id="kategori"> -->
          <?php if ($this->session->userdata('level') == 1) { ?>
           <?php if ($uri == 'getKategori'){ ?>
              <li class="nav-item active">
          <?php } else { ?> 
              <li class="nav-item">
          <?php } ?>    
            <a class="nav-link" href="<?=base_url();?>index.php/User/getKategori">
              <i class="fa fa-th-large" aria-hidden="true"></i>
              <p>Jenis Kategori</p>
            </a>
          </li>
          <?php } ?>    

          <?php if ($this->session->userdata('level') == 2) { ?>
           <?php if ($uri == 'getSubKategori'){ ?>
              <li class="nav-item active">
          <?php } else { ?> 
              <li class="nav-item">
          <?php } ?>    
            <a class="nav-link" href="<?=base_url();?>index.php/User/getSubKategori">
              <i class="material-icons">bubble_chart</i>
              <p>Sub Kategori</p>
            </a>
          </li>
          <?php } ?>
          <!-- </ul>
          <?php } ?> -->
          
         
          <?php if ( ($this->session->userdata('level') ==  2) || ($this->session->userdata('level') ==  3) ) { ?>
          <li data-toggle="collapse" data-target="#tickets" class="nav-item">
              <a class="nav-link" href="#">
              <i class="fa fa-sort-desc"></i>
              <span class="arrow"></span>
              <p>Tickets</p>
              </a>
          </li>

          <ul class="sub-menu collapse" id="tickets">
          <?php if ( ($this->session->userdata('level') ==  2) || ($this->session->userdata('level') ==  3) ) { ?>
          <?php if ($uri == 'getTickets'){ ?>
              <li class="nav-item active">
          <?php } else { ?> 
              <li class="nav-item">
          <?php } ?>    
            <a class="nav-link" href="<?=base_url();?>index.php/User/getTickets">
              <i class="fa fa-ticket"></i>
              <?php 
              $i = $this->session->userdata('id_biro');
              $q = $this->db->get_where('tickets',array('status' => 'open','id_biro' => $i, 'deleted_at' => null));
              $notif = $q->num_rows();
              ?>
              <p>Ticket Masuk <span class="badge"><?= $notif;?></span></p>
            </a>
          </li>
          <?php } ?>
          
          <?php if ( ($this->session->userdata('level') ==  2) || ($this->session->userdata('level') ==  3) ) { ?>
          <?php if ($uri == 'getMyTicket'){ ?>
              <li class="nav-item active">
          <?php } else { ?> 
              <li class="nav-item">
          <?php } ?>    
            <a class="nav-link" href="<?=base_url();?>index.php/User/getMyTicket">
            <i class="fa fa-star" aria-hidden="true"></i>
            <?php 
              $i = $this->session->userdata('id');
              $q = $this->db->get_where('tickets',array('status' => 'open','id_user' => $i, 'deleted_at' => null));
              $notif = $q->num_rows();
              ?>
              <p>Ticket Saya <span class="badge"><?= $notif;?></span></p>
            </a>
          </li>
            <?php } ?>    
          </ul>
            <?php } ?>    
        
        <?php if ($this->session->userdata('level') == 1) { ?>
          <?php if ($uri == 'getSettings'){ ?>
              <li class="nav-item active">
          <?php } else { ?> 
              <li class="nav-item">
          <?php } ?>    
            <a class="nav-link" href="<?=base_url();?>index.php/User/getTicketsAdmin">
            <i class="fa fa-ticket" aria-hidden="true"></i>
              <p>Tickets</p>
            </a>
          </li>
          <?php } ?>
        
        <?php if ($this->session->userdata('level') == 1) { ?>
          <?php if ($uri == 'getSettings'){ ?>
              <li class="nav-item active">
          <?php } else { ?> 
              <li class="nav-item">
          <?php } ?>    
            <a class="nav-link" href="<?=base_url();?>index.php/User/getSettings">
            <i class="fa fa-cogs" aria-hidden="true"></i>
              <p>Pengaturan</p>
            </a>
          </li>
          <?php } ?>
       
        <?php if (($this->session->userdata('level') == 2) || ($this->session->userdata('level') == 3)) { ?>
        <?php if ($uri == 'getKritik'){ ?>
              <li class="nav-item active">
          <?php } else { ?> 
              <li class="nav-item">
          <?php } ?>    
            <a class="nav-link" href="<?=base_url();?>index.php/User/getKritik">
            <i class="fa fa-commenting" aria-hidden="true"></i>
              <p>Kritik</p>
            </a>
          </li>
          <?php } ?>    
        
        <?php if ($this->session->userdata('level') == 2) { ?>
        <?php if ($uri == 'getSarana'){ ?>
              <li class="nav-item active">
          <?php } else { ?> 
              <li class="nav-item">
          <?php } ?>    
            <a class="nav-link" href="<?=base_url();?>index.php/User/getSarana">
            <i class="fa fa-print" aria-hidden="true"></i>
              <p>Sarana</p>
            </a>
          </li>
          <?php } ?>    

        <?php if ($this->session->userdata('level') == 2) { ?>
        <?php if ($uri == 'getLokasi'){ ?>
              <li class="nav-item active">
          <?php } else { ?> 
              <li class="nav-item">
          <?php } ?>    
            <a class="nav-link" href="<?=base_url();?>index.php/User/getLokasi">
            <i class="fa fa-map-marker" aria-hidden="true"></i>
              <p>Lokasi</p>
            </a>
          </li>
          <?php } ?>    

        <?php if ($this->session->userdata('level') == 2) { ?>
        <?php if ($uri == 'getInventaris'){ ?>
            <li class="nav-item active">
          <?php } else { ?> 
            <li class="nav-item">
          <?php } ?>    
            <a class="nav-link" href="<?=base_url();?>index.php/User/getInventaris">
            <i class="fa fa-barcode" aria-hidden="true"></i>
              <p>Inventaris</p>
            </a>
          </li>
          <?php } ?>    

        <?php if ($this->session->userdata('level') == 1) { ?>
          <?php if ($uri == 'getLogs'){ ?>
              <li class="nav-item active">
          <?php } else { ?> 
              <li class="nav-item">
          <?php } ?>    
            <a class="nav-link" href="<?=base_url();?>index.php/User/getLogs">
            <i class="fa fa-hourglass-end" aria-hidden="true"></i>
              <p>Logs</p>
            </a>
          </li>
          <?php } ?>    

        </ul>
      </div>
    </div>