<?php
    class Model_users extends CI_Model{

        public function __construct()
        {
            parent::__construct();
        }

        public function getUsers()
        {
            return $this->db->get('users')->result_array();
        }

        public function getBiro()
        {
            return $this->db->get('biro')->result_array();
        }

        public function getCategory($id_biro)
        {
            $q = $this->db->query("
            select * from kategori where id_biro = $id_biro")->result_array();
            $new = json_encode($q);
            echo $new;
        }
        
        public function get_message($id_ticket)
        {
            $q = $this->db->query("select ticket_message.pesan as pesan, ticket_message.tanggal as tanggal, users.nm_peg as nama_user, staff.nm_peg as nama_staff from ticket_message left join users on users.id = ticket_message.id_user left join staff on staff.id = ticket_message.id_staff where id_ticket = $id_ticket")->result_array();
            $new = json_encode($q);
            echo $new;
        }

        public function insertUser()
        {
            $data = array(
                'nip' => $this->input->post('nip'),
                'nm_peg' => $this->input->post('nm_peg'),
                'password' => md5('winner@123'),
                'status' => $this->input->post('status'),
                'jns_kelamin_peg' => $this->input->post('jns_kelamin_peg'),
                'alamat' => $this->input->post('alamat'),
                'kodepos' => $this->input->post('kodepos'),
                'telepon' => $this->input->post('telepon'),
                'handphone' => $this->input->post('handphone'),
                'email' => $this->input->post('email'),
                'kd_jabatan' => $this->input->post('kd_jabatan'),
                'nm_jabatan' => $this->input->post('nm_jabatan'),
                'kd_unit_org' => $this->input->post('kd_unit_org'),
                'nm_unit_org' => $this->input->post('nm_unit_org'),
            );

            return $this->db->insert('users',$data);
        }
    }
?>