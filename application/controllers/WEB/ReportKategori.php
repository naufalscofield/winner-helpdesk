<?php
   
require APPPATH . 'libraries/REST_Controller.php';
     
class ReportKategori extends REST_Controller {
    
	  /**
     * Get All Data from this method.
     *
     * @return Response
    */
    public function __construct() {
       parent::__construct();
       $this->load->database();
    }
       
    /**
     * Get All Data from this method.
     *
     * @return Response
    */
	public function index_get()
	{
        
        $data = $this->db->query("SELECT COUNT(a.id_kategori) as jml, b.nama_kategori FROM tickets a INNER JOIN kategori b ON a.id_kategori = b.id GROUP BY a.id_kategori ORDER BY jml DESC LIMIT 5")->result_array();
        // $data = $this->db->query('SELECT kategori.id,kategori.nama_kategori, count(kategori.id) as jml_staff FROM kategori inner join users on users.id_kategori = kategori.id where users.admin = "yes" GROUP BY kategori.id')->result();
        // print_r($data); die;
     
        $this->response($data, REST_Controller::HTTP_OK);
	}
      
    /**
     * Get All Data from this method.
     *
     * @return Response
    */
    public function index_post()
    {
        // $input = $this->input->post();
        $data = array(
            'nama_kategori' => $this->input->post('nama_kategori'),
        );
        $this->db->insert('kategori',$data);
     
        $this->response(['Item created successfully.'], REST_Controller::HTTP_OK);
    } 
     
    /**
     * Get All Data from this method.
     *
     * @return Response
    */
    public function index_put($id)
    {
        $input = $this->put();
        $this->db->update('kategori', $input, array('id'=>$id));
     
        $this->response(['Item updated successfully.'], REST_Controller::HTTP_OK);
    }
     
    /**
     * Get All Data from this method.
     *
     * @return Response
    */
    public function index_delete($id)
    {
        $this->db->delete('kategori', array('id'=>$id));
       
        $this->response(['Item deleted successfully.'], REST_Controller::HTTP_OK);
    }
    	
}