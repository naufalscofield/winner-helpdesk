<?php
   
require APPPATH . 'libraries/REST_Controller.php';
     
class DetailTicket extends REST_Controller {
    
	  /**
     * Get All Data from this method.
     *
     * @return Response
    */
    public function __construct() {
       parent::__construct();
       $this->load->database();
    }
       
    /**
     * Get All Data from this method.
     *
     * @return Response
    */
	public function index_get($id_ticket)
	{
            // print_r('cek'); die;
            $this->db->join('subkategori','subkategori.id = tickets.id_sub_kategori');
            $this->db->join('biro','biro.id = tickets.id_biro');
            $this->db->join('users b','b.id = tickets.id_user', 'left');
            $this->db->join('users c','c.id = tickets.pic_wp', 'left');
            $this->db->join('users d','d.id = tickets.pic_close', 'left');
            $this->db->join('users e','e.id = tickets.pic_done', 'left');
            $this->db->join('kategori','kategori.id = tickets.id_kategori');
            $this->db->select('*,tickets.id as id_ticket, biro.id as id_biro, kategori.id as id_kategori, tickets.status as status_ticket, subkategori.nama_sub_kategori, datediff(tanggal_close, tanggal_open) as selisih_hari, b.nm_peg as pic_open, c.nm_peg as pic_wp, d.nm_peg as pic_close, e.nm_peg as pic_done');
            $data = $this->db->get_where("tickets", ['tickets.id' => $id_ticket, 'deleted_at' => null])->result_array();
     
        $this->response($data, REST_Controller::HTTP_OK);
	}
      
    /**
     * Get All Data from this method.
     *
     * @return Response
    */
    public function index_post()
    {
        // $input = $this->input->post();
        $data = array(
            'nama_tickets' => $this->input->post('nama_tickets'),
            'id_biro' => $this->input->post('id_biro'),
        );
        $this->db->insert('tickets',$data);
     
        $this->response(['Item created successfully.'], REST_Controller::HTTP_OK);
    } 
     
    /**
     * Get All Data from this method.
     *
     * @return Response
    */
    public function index_put($id)
    {
        $input = $this->put();
        $this->db->update('tickets', $input, array('id'=>$id));
     
        $this->response(['Item updated successfully.'], REST_Controller::HTTP_OK);
    }
     
    /**
     * Get All Data from this method.
     *
     * @return Response
    */
    public function index_delete($id)
    {
        $this->db->delete('tickets', array('id'=>$id));
       
        $this->response(['Item deleted successfully.'], REST_Controller::HTTP_OK);
    }
    	
}