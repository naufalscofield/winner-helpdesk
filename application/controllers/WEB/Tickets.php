<?php
   
require APPPATH . 'libraries/REST_Controller.php';
     
class Tickets extends REST_Controller {
    
	  /**
     * Get All Data from this method.
     *
     * @return Response
    */
    public function __construct() {
       parent::__construct();
       $this->load->database();
    }
       
    /**
     * Get All Data from this method.
     *
     * @return Response
    */
	public function index_get($id = 0)
	{
        if(!empty($id)){
            // $this->db->join('biro','biro.id = tickets.id_biro');
            // $this->db->select('*,biro.id as id_biro, tickets.id as id_tickets');
            $data = $this->db->get_where("tickets", ['id' => $id])->row_array();
        }else{
            $this->db->join('users','users.id = tickets.id_user');
            $this->db->join('biro','biro.id = tickets.id_biro');
            $this->db->join('kategori','kategori.id = tickets.id_kategori');
            $this->db->select('*,users.id as id_user, biro.id as id_biro, kategori.id as id_kategori, users.status as status_user, tickets.status as status_ticket');
            $data = $this->db->get("tickets")->result();
        }
     
        $this->response($data, REST_Controller::HTTP_OK);
	}
      
    /**
     * Get All Data from this method.
     *
     * @return Response
    */
    public function index_post()
    {
        // $input = $this->input->post();
        $data = array(
            'nama_tickets' => $this->input->post('nama_tickets'),
            'id_biro' => $this->input->post('id_biro'),
        );
        $this->db->insert('tickets',$data);
     
        $this->response(['Item created successfully.'], REST_Controller::HTTP_OK);
    } 
     
    /**
     * Get All Data from this method.
     *
     * @return Response
    */
    public function index_put($id)
    {
        date_default_timezone_set('Asia/Jakarta');
        $status = $this->put('status');

        if ($status == 'closed'){
            $q = $this->db->get_where('tickets', ['id' => $id])->row();
            $tgl_open = $q->tanggal_open;
            $tgl_close = date('Y-m-d H:i:s');
            $t1 = strtotime($tgl_open);
            $t2 = strtotime($tgl_close);
            $diff = $t2 - $t1;
            $hours = $diff / ( 60 * 60 );

            $input = array(
                'status' => $this->put('status'),
                'pic_close' => $this->session->userdata('id'),
                'tanggal_close' => date('Y-m-d H:i:s'),
                'selesai_dalam' => $hours
            );
            $this->db->update('tickets', $input, array('id'=>$id));
            
            $iduser = $this->session->userdata('id');
            date_default_timezone_set('Asia/Jakarta');
            $log = [
                'id_user' =>  $iduser,
                'aktifitas' => 'Memperbarui Status Ticket',
                'tanggal' => date('Y-m-d H:i:s')
            ];
            $this->db->insert('logs',$log);
            
            $this->response(['Item updated successfully.'], REST_Controller::HTTP_OK);
            
        } else if ($status == 'work_progress') {
            $input = array(
                'status' => $this->put('status'),
                'pic_wp' => $this->session->userdata('id'),
                'tanggal_wp' => date('Y-m-d H:i:s'),
            );
            $this->db->update('tickets', $input, array('id'=>$id));
            
            $iduser = $this->session->userdata('id');
            date_default_timezone_set('Asia/Jakarta');
            $log = [
                'id_user' =>  $iduser,
                'aktifitas' => 'Memperbarui Status Ticket',
                'tanggal' => date('Y-m-d H:i:s')
            ];
            $this->db->insert('logs',$log);
            
            $this->response(['Item updated successfully.'], REST_Controller::HTTP_OK);
            
        } else if ($status == 'done') {
            $input = array(
                'status' => $this->put('status'),
                'pic_done' => $this->session->userdata('id'),
                'tanggal_done' => date('Y-m-d H:i:s'),
            );
            $this->db->update('tickets', $input, array('id'=>$id));
            
            $iduser = $this->session->userdata('id');
            date_default_timezone_set('Asia/Jakarta');
            $log = [
                'id_user' =>  $iduser,
                'aktifitas' => 'Memperbarui Status Ticket',
                'tanggal' => date('Y-m-d H:i:s')
            ];
            $this->db->insert('logs',$log);
            
            $this->response(['Item updated successfully.'], REST_Controller::HTTP_OK);
            
        } else {
            $input = array(
                'deleted_at' => date('Y-m-d H:i:s'),
                'deleted_by' => $this->session->userdata('id'),
            );
            $this->db->update('tickets', $input, array('id'=>$id));

            $iduser = $this->session->userdata('id');
            date_default_timezone_set('Asia/Jakarta');
            $log = [
                'id_user' =>  $iduser,
                'aktifitas' => 'Menghapus Ticket',
                'tanggal' => date('Y-m-d H:i:s')
            ];
            $this->db->insert('logs',$log);
         
            $this->response(['Item updated successfully.'], REST_Controller::HTTP_OK);
        }
        
        //push notification update
        $dataTicket = $this->db->get_where('tickets', array('id' => $id))->row();

        $idUser = $dataTicket->id_user;
        $dataUser = $this->db->get_where('users',array('id' => $idUser))->row();
        
        $idAdminBiro = $this->session->userdata('id');
        $dataAdminBiro = $this->db->get_where('users',array('id' => $idAdminBiro))->row();
        
        $idKategori = $dataTicket->id_kategori;
        $dataKategori = $this->db->get_where('kategori',array('id' => $idKategori))->row();
        $namaKategori = strtoupper($dataKategori->nama_kategori);

            $data = [
                "to"=> $dataUser->fcm_token,
                "collapse_key" => "type_a",
                "notification" =>   [
                    "body" => "Tiket anda terkait $namaKategori masuk tahap => $status",
                    "title" => "Update Status Tiket! | $status"
                    ]
                ];
                $url = "https://fcm.googleapis.com/fcm/send";
                $content = json_encode($data);
                // print_r($content); die;

            $curl = curl_init($url);
            curl_setopt($curl, CURLOPT_CUSTOMREQUEST, "POST"); 
            curl_setopt($curl, CURLOPT_HEADER, false);
            curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($curl, CURLOPT_HTTPHEADER,array("Authorization: key=AAAAwQLSs_o:APA91bEiflr1fhqM5mWSBeENyHQm7VwYxSzVtkhfU4ZB_tFPPawpvpkn_ECIUS3gCFxrrtrT8G9emeKU9raS9pnED0LMuCmrE75uU4XZlzUI6U_t-SuKc3VkqHAb5G4_8Y9unS2quOLy","Content-type: application/json"));
            curl_setopt($curl, CURLOPT_POST, true);
            curl_setopt($curl, CURLOPT_POSTFIELDS, $content);

            $json_response = curl_exec($curl);
            $status = curl_getinfo($curl, CURLINFO_HTTP_CODE);

            if ( $status != 201 ) {
                die("Error: call to URL $url failed with status $status, response $json_response, curl_error " . curl_error($curl) . ", curl_errno " . curl_errno($curl));
            }

            curl_close($curl);
    }
     
    /**
     * Get All Data from this method.
     *
     * @return Response
    */
    public function index_delete($id)
    {
        $this->db->delete('tickets', array('id'=>$id));
       
        $this->response(['Item deleted successfully.'], REST_Controller::HTTP_OK);
    }
    	
}