<?php
   
require APPPATH . 'libraries/REST_Controller.php';
     
class ProfileUsers extends REST_Controller {
    
	  /**
     * Get All Data from this method.
     *
     * @return Response
    */
    public function __construct() {
       parent::__construct();
       $this->load->database();
    }
     
    /**
     * Get All Data from this method.
     *
     * @return Response
    */
    public function index_put($id)
    {
        $input = array(
            'nip' => $this->put('nip'),
            'password' => md5($this->put('pw')),
            'email' => $this->put('email'),
            'nm_peg' => $this->put('nm_peg'),
            'handphone' => $this->put('handphone'),
            'jns_kelamin_peg' => $this->put('jns_kelamin_peg'),
            'kodepos' => $this->put('kodepos'),
            'telepon' => $this->put('telepon'),
            'nm_jabatan' => $this->put('nm_jabatan'),
            'kd_jabatan' => $this->put('kd_jabatan'),
            'kd_unit_org' => $this->put('kd_unit_org'),
            'nm_unit_org' => $this->put('nm_unit_org'),
            'status' => $this->put('status'),
            'alamat' => $this->put('alamat'),

        );
        $q = $this->db->update('users', $input, array('id'=>$id));

        $iduser = $this->session->userdata('id');
        date_default_timezone_set('Asia/Jakarta');
		$log = [
			'id_user' =>  $iduser,
			'aktifitas' => 'Memperbarui Profil',
			'tanggal' => date('Y-m-d H:i:s')
		];
		$this->db->insert('logs',$log);
        
        if ($q){
            $this->response([
                "status" => true,
                "message" => 'Sukses update profile!',
                "message2" => 'Silahlan lakukan login kembali untuk verifikasi!', 
            ], 200);
        } else {
            $this->response([
                "status" => false,
                "message" => 'Gagal update profile!',
            ], 400);

        }
    }
    	
}