<?php
   
require APPPATH . 'libraries/REST_Controller.php';
     
class Kategori extends REST_Controller {
    
	  /**
     * Get All Data from this method.
     *
     * @return Response
    */
    public function __construct() {
       parent::__construct();
       $this->load->database();
    }
       
    /**
     * Get All Data from this method.
     *
     * @return Response
    */
	public function index_get($id = 0)
	{
        if(!empty($id)){
            $this->db->join('biro','biro.id = kategori.id_biro');
            $this->db->select('*,biro.id as id_biro, kategori.id as id_kategori');
            $data = $this->db->get_where("kategori", ['kategori.id' => $id])->row_array();
        }else{
            $this->db->join('biro','biro.id = kategori.id_biro');
            $this->db->select('*,biro.id as id_biro, kategori.id as id_kategori');
            $this->db->order_by('kategori.id', 'asc');
            $data = $this->db->get("kategori")->result();
        }
     
        $this->response($data, REST_Controller::HTTP_OK);
	}
      
    /**
     * Get All Data from this method.
     *
     * @return Response
    */
    public function index_post()
    {
        // $input = $this->input->post();
        $data = array(
            'nama_kategori' => $this->input->post('nama_kategori'),
            'id_biro' => $this->input->post('id_biro'),
        );
        $this->db->insert('kategori',$data);

        $iduser = $this->session->userdata('id');
        date_default_timezone_set('Asia/Jakarta');
		$log = [
			'id_user' =>  $iduser,
			'aktifitas' => 'Menambahkan Kategori Baru, Kategori '.$data['nama_kategori'],
			'tanggal' => date('Y-m-d H:i:s')
		];
		$this->db->insert('logs',$log);
     
        $this->response(['Item created successfully.'], REST_Controller::HTTP_OK);
    } 
     
    /**
     * Get All Data from this method.
     *
     * @return Response
    */
    public function index_put($id)
    {
        $input = $this->put();
        $this->db->update('kategori', $input, array('id'=>$id));
     
        $iduser = $this->session->userdata('id');
        date_default_timezone_set('Asia/Jakarta');
		$log = [
			'id_user' =>  $iduser,
			'aktifitas' => 'Memperbarui Nama Kategori',
			'tanggal' => date('Y-m-d H:i:s')
		];
        $this->db->insert('logs',$log);
        
        $this->response(['Item updated successfully.'], REST_Controller::HTTP_OK);
    }
     
    /**
     * Get All Data from this method.
     *
     * @return Response
    */
    public function index_delete($id)
    {
        $this->db->delete('kategori', array('id'=>$id));
       
        $iduser = $this->session->userdata('id');
        date_default_timezone_set('Asia/Jakarta');
		$log = [
			'id_user' =>  $iduser,
			'aktifitas' => 'Menghapus Kategori',
			'tanggal' => date('Y-m-d H:i:s')
		];
        $this->db->insert('logs',$log);
        
        $this->response(['Item deleted successfully.'], REST_Controller::HTTP_OK);
    }
    	
}