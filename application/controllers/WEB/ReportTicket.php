<?php
   
require APPPATH . 'libraries/REST_Controller.php';
require 'vendor/autoload.php';
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;

class ReportTicket extends REST_Controller {
    
	  /**
     * Get All Data from this method.
     *
     * @return Response
    */
    public function __construct() {
       parent::__construct();
       $this->load->database();
    }
       
    /**
     * Get All Data from this method.
     *
     * @return Response
    */
	
      
    /**
     * Get All Data from this method.
     *
     * @return Response
    */

    public function index_get()
    {

    }
    public function index_post()
    {
        $id_user = $this->session->userdata('id');
        $dataUser = $this->db->get_where('users',['id' => $id_user])->row();
        $id_ticket = $this->input->post('id');
        $feedback = $this->input->post('feedback');
        $diperbaiki = $this->input->post('diperbaiki');
        $diganti = $this->input->post('diganti');
        $catatan = $this->input->post('catatan');
        $jumlah = $diperbaiki + $diganti;

       
        $updateData = [
            'feedback' => $feedback
        ];
        
        $this->db->update('tickets', $updateData, array('id'=>$id_ticket));
        
        $this->db->join('users','users.id = tickets.id_user');
        
        $dataTicket = $this->db->get_where('tickets',['tickets.id' => $id_ticket])->row();
        
        $dataMessage = $this->db->get_where('ticket_message',['id_ticket' => $id_ticket])->row();

        $dataSaranaQ = $this->db->get_where('inventaris',['no_inventaris' => $dataTicket->no_inventaris])->row();
        $dataSarana = $this->db->get_where('sarana',['id' => $dataSaranaQ->id_sarana])->row();
        $dataLokasiQ = $this->db->get_where('inventaris',['no_inventaris' => $dataTicket->no_inventaris])->row();
        $dataLokasi = $this->db->get_where('lokasi',['id' => $dataLokasiQ->id_lokasi])->row();
        
            $spreadsheet = new Spreadsheet();
            $spreadsheet->getDefaultStyle()->getFont()->setSize(12);
            
            //MERGE
            $spreadsheet->getActiveSheet()->mergeCells('A1:C4');
            $spreadsheet->getActiveSheet()->mergeCells('F1:J2');
            $spreadsheet->getActiveSheet()->mergeCells('F3:J4');
            $spreadsheet->getActiveSheet()->mergeCells('L3:N3');
            $spreadsheet->getActiveSheet()->mergeCells('L4:N4');
            $spreadsheet->getActiveSheet()->mergeCells('M1:N1');
            $spreadsheet->getActiveSheet()->mergeCells('M2:N2');
            $spreadsheet->getActiveSheet()->mergeCells('A6:B6');
            $spreadsheet->getActiveSheet()->mergeCells('C6:G6');
            $spreadsheet->getActiveSheet()->mergeCells('A7:B7');
            $spreadsheet->getActiveSheet()->mergeCells('C7:G7');
            $spreadsheet->getActiveSheet()->mergeCells('A8:B8');
            $spreadsheet->getActiveSheet()->mergeCells('C8:G8');
            $spreadsheet->getActiveSheet()->mergeCells('A9:B9');
            $spreadsheet->getActiveSheet()->mergeCells('C9:G9');
            $spreadsheet->getActiveSheet()->mergeCells('A10:B10');
            $spreadsheet->getActiveSheet()->mergeCells('C10:G10');
            $spreadsheet->getActiveSheet()->mergeCells('H6:K6');
            $spreadsheet->getActiveSheet()->mergeCells('J6:N6');
            $spreadsheet->getActiveSheet()->mergeCells('H7:N10');
            $spreadsheet->getActiveSheet()->mergeCells('A11:N11');
            $spreadsheet->getActiveSheet()->mergeCells('A12:B12');
            $spreadsheet->getActiveSheet()->mergeCells('A13:B13');
            $spreadsheet->getActiveSheet()->mergeCells('A14:B14');
            $spreadsheet->getActiveSheet()->mergeCells('A15:B15');
            $spreadsheet->getActiveSheet()->mergeCells('A16:B16');
            $spreadsheet->getActiveSheet()->mergeCells('C12:D12');
            $spreadsheet->getActiveSheet()->mergeCells('C13:D13');
            $spreadsheet->getActiveSheet()->mergeCells('C14:D14');
            $spreadsheet->getActiveSheet()->mergeCells('C15:D15');
            $spreadsheet->getActiveSheet()->mergeCells('C16:D16');
            $spreadsheet->getActiveSheet()->mergeCells('E12:K12');
            $spreadsheet->getActiveSheet()->mergeCells('E13:K16');
            $spreadsheet->getActiveSheet()->mergeCells('L12:N12');
            $spreadsheet->getActiveSheet()->mergeCells('L13:N16');
            $spreadsheet->getActiveSheet()->mergeCells('A17:N20');
            $spreadsheet->getActiveSheet()->mergeCells('A22:B22');
            $spreadsheet->getActiveSheet()->mergeCells('C22:D22');
            $spreadsheet->getActiveSheet()->mergeCells('A24:C24');
            $spreadsheet->getActiveSheet()->mergeCells('C25:D25');
            $spreadsheet->getActiveSheet()->mergeCells('C26:D26');

            //STYLE
            $border1 = [
				'borders' => [
					'outline' => [
						'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THICK,
						'color' => ['argb' => '000'],
					],
                ],
               
			];
            $border1Center = [
				'borders' => [
					'outline' => [
						'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THICK,
						'color' => ['argb' => '000'],
					],
                ],
                'alignment' => [
                    'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER,
                    'vertical' => \PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER,
            ]
               
			];
            $Center = [
                'alignment' => [
                    'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER,
                    'vertical' => \PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER,
                ]
               
			];
            $judul = [
				'font' => [
                    'bold' => true,
                    'size' => 16
				],
				'alignment' => [
                        'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER,
                        'vertical' => \PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER,
				]
            ];
            $judul2 = [
				'font' => [
                    'bold' => true,
                    'size' => 16
				],
				'alignment' => [
                        'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER,
                        'vertical' => \PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER,
				]
            ];
            
            //APPLY STYLE
            $spreadsheet->getActiveSheet()->getStyle('A1:N4')->applyFromArray($border1);
            $spreadsheet->getActiveSheet()->getStyle('A6:G6')->applyFromArray($border1);
            $spreadsheet->getActiveSheet()->getStyle('A7:G7')->applyFromArray($border1);
            $spreadsheet->getActiveSheet()->getStyle('A8:G8')->applyFromArray($border1);
            $spreadsheet->getActiveSheet()->getStyle('A9:G9')->applyFromArray($border1);
            $spreadsheet->getActiveSheet()->getStyle('A10:G10')->applyFromArray($border1);
            $spreadsheet->getActiveSheet()->getStyle('H6:N6')->applyFromArray($border1);
            $spreadsheet->getActiveSheet()->getStyle('H7:N10')->applyFromArray($border1Center);
            $spreadsheet->getActiveSheet()->getStyle('A12:D16')->applyFromArray($border1);
            $spreadsheet->getActiveSheet()->getStyle('E12:K12')->applyFromArray($border1);
            $spreadsheet->getActiveSheet()->getStyle('L12:N12')->applyFromArray($border1);
            $spreadsheet->getActiveSheet()->getStyle('E13:K16')->applyFromArray($border1Center);
            $spreadsheet->getActiveSheet()->getStyle('L13:N16')->applyFromArray($border1Center);
            $spreadsheet->getActiveSheet()->getStyle('A17:N20')->applyFromArray($border1Center);
            $spreadsheet->getActiveSheet()->getStyle('A11')->applyFromArray($Center);
            $spreadsheet->getActiveSheet()->getStyle('F1')->applyFromArray($judul);
            $spreadsheet->getActiveSheet()->getStyle('F3')->applyFromArray($judul2);
            $spreadsheet->getActiveSheet()->getStyle('A22:D22')->applyFromArray($border1);
            $spreadsheet->getActiveSheet()->getStyle('A24:D26')->applyFromArray($border1);
            
            //DRAWING IMAGE
            $drawing = new \PhpOffice\PhpSpreadsheet\Worksheet\Drawing();
			$drawing->setName('Logo');
			$drawing->setDescription('Logo');
			$drawing->setPath('./assets/main_bootstrap/img/winner.png');
			$drawing->setWidth(205);
			$drawing->setCoordinates('A1');
            $drawing->setWorksheet($spreadsheet->getActiveSheet());
            
            //INPUT
            $sheet = $spreadsheet->getActiveSheet();
            $sheet->setCellValue('F1', 'FORMULIR PERBAIKAN');
            $sheet->setCellValue('F3', 'SARANA & PRASARANA');
            $sheet->setCellValue('L1', 'No. Dok');
            $sheet->setCellValue('L2', 'Rev');
            $sheet->setCellValue('L3', 'Prosedur Pemeliharaan Sarana &');
            $sheet->setCellValue('L4', 'Prasarana Perusahaan');
            $sheet->setCellValue('A6', 'No. Inventaris');
            $sheet->setCellValue('C6', ": $dataTicket->no_inventaris");
            $sheet->setCellValue('A7', 'Nama Sarana');
            $sheet->setCellValue('C7', ": $dataSarana->nama_sarana");
            $sheet->setCellValue('A8', 'Nama Lokasi');
            $sheet->setCellValue('C8', ": $dataLokasi->nama_lokasi");
            $sheet->setCellValue('A9', 'Pemohon');
            $sheet->setCellValue('C9', ": $dataTicket->nm_peg");
            $sheet->setCellValue('A10', 'Tanggal');
            $sheet->setCellValue('C10', ": $dataTicket->tanggal_open");
            $sheet->setCellValue('H6', "Jenis Kerusakan / Keluhan :");
            $sheet->setCellValue('H7', "$dataMessage->pesan");
            $sheet->setCellValue('A11', "DIISI OLEH STAFF UMUM");
            $sheet->setCellValue('A12', "Perbaikan :");
            $sheet->setCellValue('A13', "Diperbaiki :");
            $sheet->setCellValue('C13', "( $diperbaiki )");
            $sheet->setCellValue('A14', "Diganti :");
            $sheet->setCellValue('C14', "( $diganti )");
            $sheet->setCellValue('E12', "Suku Cadang Yang Diganti");
            $sheet->setCellValue('E13', "$feedback");
            $sheet->setCellValue('L12', "Jumlah");
            $sheet->setCellValue('L13', "$jumlah");
            $sheet->setCellValue('A17', "Catatan : $catatan");
            $sheet->setCellValue('A22', "Ticket Close");
            $sheet->setCellValue('C22', ": $dataTicket->tanggal_close");
            $sheet->setCellValue('A24', "Di generate dari aplikasi");
            $sheet->setCellValue('A25', "Oleh");
            $sheet->setCellValue('C25', ": $dataUser->nm_peg");
            $sheet->setCellValue('A26', "Tanggal");
            $dateNow = date('Y-m-d');
            $sheet->setCellValue('C26', ": $dateNow");

            //SET DIRECTORY
            $desktop = getenv("HOMEDRIVE").getenv("HOMEPATH")."\Desktop";

            //SAVE FILE
            $writer = new Xlsx($spreadsheet);
            $idticket = $dataTicket->id;
            $date = date('d-m-Y');
			$writer->save("$desktop/Report Fasum-$idticket-$date.xlsx");
			$dir = "$desktop/Report Fasum-$idticket-$date.xlsx";
        
            $this->response([
            "status" => true,
            "message" => "File report berhasil disimpan di $dir"
            ], 200);
    } 
     
    /**
     * Get All Data from this method.
     *
     * @return Response
    */
    public function index_put($id)
    {
        $input = $this->put();
        $this->db->update('sarana', $input, array('id'=>$id));

        $iduser = $this->session->userdata('id');
        date_default_timezone_set('Asia/Jakarta');
		$log = [
			'id_user' =>  $iduser,
			'aktifitas' => 'Memperbarui Nama sarana',
			'tanggal' => date('Y-m-d H:i:s')
		];
		$this->db->insert('logs',$log);
     
        $this->response(['Item updated successfully.'], REST_Controller::HTTP_OK);
    }
     
    /**
     * Get All Data from this method.
     *
     * @return Response
    */
    public function index_delete($id)
    {
        $this->db->delete('sarana', array('id'=>$id));

        $iduser = $this->session->userdata('id');
        date_default_timezone_set('Asia/Jakarta');
		$log = [
			'id_user' =>  $iduser,
			'aktifitas' => 'Menghapus sarana',
			'tanggal' => date('Y-m-d H:i:s')
		];
		$this->db->insert('logs',$log);
       
        $this->response(['Item deleted successfully.'], REST_Controller::HTTP_OK);
    }
    	
}