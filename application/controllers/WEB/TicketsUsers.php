<?php
require APPPATH . 'libraries/REST_Controller.php';
class TicketsUsers extends REST_Controller {
    /**
     * Get All Data from this method.
     *
     * @return Response
     */
    public function __construct() {
        parent::__construct();
        $this->load->database();
    }
    /**
     * Get All Data from this method.
     *
     * @return Response
     */
    public function index_get($id = 0) {
        $this->db->join('users', 'users.id = tickets.id_user');
        $this->db->join('biro', 'biro.id = tickets.id_biro');
        $this->db->join('kategori', 'kategori.id = tickets.id_kategori');
        $this->db->select('*,tickets.id as id_ticket, users.id as id_user, biro.id as id_biro, kategori.id as id_kategori, users.status as status_user, tickets.status as status_ticket');
        $this->db->order_by("id_ticket", "asc");
        $data = $this->db->get_where("tickets", array('id_user' => $id, 'deleted_at' => null))->result_array();
        $this->response($data, REST_Controller::HTTP_OK);
    }
    /**
     * Get All Data from this method.
     *
     * @return Response
     */
    public function index_post() {
        date_default_timezone_set('Asia/Jakarta');
        $id_kategori = $this->input->post('id_kategori');
        $id_sub_kategori = $this->input->post('id_sub_kategori');
        $pesan = $this->input->post('pesan');
        $id_biro = $this->db->get_where('kategori', ['id' => $id_kategori])->row();
        $id_biro = $id_biro->id_biro;

        if ($id_kategori == 1) {
            $no_inventaris = $this->input->post('no_inventaris');
            //cek apakah sudah ada tiket serupa dalam kurun waktu 30menit kebelakang
            $currDate = date('Y-m-d H:i:s');
            $toleransiWaktu = date("Y-m-d H:i:s", strtotime("-60 minutes"));
            $cekTiket = $this->db->query("select * from tickets where tanggal_open < '$currDate' and tanggal_open > '$toleransiWaktu' and no_inventaris = '$no_inventaris'");
            if ($cekTiket->num_rows() > 0) {
                $this->response(["status" => false, "message" => 'Maaf, dalam kurun waktu 1 Jam kebelakang, sudah ada yang melakukan submit ticket terkait inventaris ini. Sistem meng indikasikan ini permasalahan yang sama', ], 200);
            } else {
                $data = array('id_user' => $this->input->post('id_user'), 'id_biro' => $id_biro, 'id_kategori' => $this->input->post('id_kategori'), 'id_sub_kategori' => $this->input->post('id_sub_kategori'), 'no_inventaris' => $no_inventaris, 'tanggal_open' => date('Y-m-d H:i:s'), 'tanggal_wp' => null, 'pic_wp' => null, 'tanggal_close' => null, 'pic_close' => null, 'tanggal_done' => null, 'selesai_dalam' => null, 'priority' => $this->input->post('priority'), 'status' => 'open', 'deleted_at' => null,);
                $this->db->insert('tickets', $data);
                $insert_id = $this->db->insert_id();
                $data2 = array('id_ticket' => $insert_id, 'pesan' => $this->input->post('pesan'), 'id_user' => $this->input->post('id_user'), 'id_staff' => null, 'tanggal' => date('Y-m-d H:i:s'));
                $this->db->insert('ticket_message', $data2);
                $iduser = $this->session->userdata('id');
                date_default_timezone_set('Asia/Jakarta');
                $log = ['id_user' => $iduser, 'aktifitas' => 'Membuat Ticket Via Website', 'tanggal' => date('Y-m-d H:i:s') ];
                $this->db->insert('logs', $log);
                $priorityTicket = $this->input->post('priority');
                if ($priorityTicket == 'low') {
                    // PUSH NOTIFICATION LOW//
                    $idUser = $this->input->post('id_user');
                    $dataKategori = $this->db->get_where('kategori', array('id' => $id_kategori))->row();
                    $Kategori = strtoupper($dataKategori->nama_kategori);
                    $dataUser = $this->db->get_where('users', array('id' => $idUser))->row();
                    $adminBiro = $this->db->get_where('users', array('id_biro' => $id_biro, 'fcm_token !=' => null))->result_array();
                    $priority = $this->input->post('priority');
                    foreach ($adminBiro as $adminBiro) {
                        // FIREBASE //
                        $data = ["to" => $adminBiro['fcm_token'], "collapse_key" => "type_a", "notification" => ["body" => "$dataUser->nm_peg membuat tiket terkait $Kategori", "title" => "Tiket Baru! | $priority"]];
                        $url = "https://fcm.googleapis.com/fcm/send";
                        $content = json_encode($data);
                        $curl = curl_init($url);
                        curl_setopt($curl, CURLOPT_CUSTOMREQUEST, "POST");
                        curl_setopt($curl, CURLOPT_HEADER, false);
                        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
                        curl_setopt($curl, CURLOPT_HTTPHEADER, array("Authorization: key=AAAAwQLSs_o:APA91bEiflr1fhqM5mWSBeENyHQm7VwYxSzVtkhfU4ZB_tFPPawpvpkn_ECIUS3gCFxrrtrT8G9emeKU9raS9pnED0LMuCmrE75uU4XZlzUI6U_t-SuKc3VkqHAb5G4_8Y9unS2quOLy", "Content-type: application/json"));
                        curl_setopt($curl, CURLOPT_POST, true);
                        curl_setopt($curl, CURLOPT_POSTFIELDS, $content);
                        $json_response = curl_exec($curl);
                        $status = curl_getinfo($curl, CURLINFO_HTTP_CODE);
                        if ($status != 200) {
                            die("Error: call to URL $url failed with status $status, response $json_response, curl_error " . curl_error($curl) . ", curl_errno " . curl_errno($curl));
                        }
                        curl_close($curl);
                    }
                    $this->response(["status" => true, "message" => 'Sukses Submit Ticket', ], 200);
                } else if ($priorityTicket == 'normal') {
                    //PUSH NOTIFICATION NORMAL //
                    print_r('normal');
                    $idUser = $this->input->post('id_user');
                    $dataKategori = $this->db->get_where('kategori', array('id' => $id_kategori))->row();
                    $Kategori = strtoupper($dataKategori->nama_kategori);
                    $dataUser = $this->db->get_where('users', array('id' => $idUser))->row();
                    $adminBiro = $this->db->get_where('users', array('id_biro' => $id_biro, 'fcm_token !=' => null))->result_array();
                    $adminBiro2 = $this->db->get_where('users', array('id_biro' => $id_biro))->result_array();
                    $priority = $this->input->post('priority');
                    foreach ($adminBiro as $adminBiro) {
                        // FIREBASE //
                        $data = ["to" => $adminBiro['fcm_token'], "collapse_key" => "type_a", "notification" => ["body" => "$dataUser->nm_peg membuat tiket terkait $Kategori", "title" => "Tiket Baru! | $priority"]];
                        $url = "https://fcm.googleapis.com/fcm/send";
                        $content = json_encode($data);
                        $curl = curl_init($url);
                        curl_setopt($curl, CURLOPT_CUSTOMREQUEST, "POST");
                        curl_setopt($curl, CURLOPT_HEADER, false);
                        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
                        curl_setopt($curl, CURLOPT_HTTPHEADER, array("Authorization: key=AAAAwQLSs_o:APA91bEiflr1fhqM5mWSBeENyHQm7VwYxSzVtkhfU4ZB_tFPPawpvpkn_ECIUS3gCFxrrtrT8G9emeKU9raS9pnED0LMuCmrE75uU4XZlzUI6U_t-SuKc3VkqHAb5G4_8Y9unS2quOLy", "Content-type: application/json"));
                        curl_setopt($curl, CURLOPT_POST, true);
                        curl_setopt($curl, CURLOPT_POSTFIELDS, $content);
                        $json_response = curl_exec($curl);
                        $status = curl_getinfo($curl, CURLINFO_HTTP_CODE);
                        if ($status != 200) {
                            die("Error: call to URL $url failed with status $status, response $json_response, curl_error " . curl_error($curl) . ", curl_errno " . curl_errno($curl));
                        }
                        curl_close($curl);
                    }
                    // foreach ($adminBiro2 as $adminBiro2) {
                    //     // WHATSAPP //
                    //     $number = $adminBiro2['handphone'];
                        $email = $adminBiro2['email'];
                    //     $body = "$dataUser->nm_peg membuat tiket terkait $Kategori";
                    //     $data = ['phone' => $number, 'body' => $body, ];
                    //     $json = json_encode($data);
                    //     $url = 'https://eu56.chat-api.com/instance90723/message?token=rle2vpmq3uqw1dfk';
                    //     $options = stream_context_create(['http' => ['method' => 'POST', 'header' => 'Content-type: application/json', 'content' => $json]]);
                    //     $result = file_get_contents($url, false, $options);

                    // }
                    $this->response(["status" => true, "message" => 'Sukses Submit Ticket', ], 200);
                } else {
                    //PUSH NOTIFICATION URGENT //
                    $idUser = $this->input->post('id_user');
                    $dataKategori = $this->db->get_where('kategori', array('id' => $id_kategori))->row();
                    $Kategori = strtoupper($dataKategori->nama_kategori);
                    $dataSubKategori = $this->db->get_where('subkategori', array('id' => $id_sub_kategori))->row();
                    $SubKategori = strtoupper($dataSubKategori->nama_sub_kategori);
                    $dataUser = $this->db->get_where('users', array('id' => $idUser))->row();
                    $adminBiro = $this->db->get_where('users', array('id_biro' => $id_biro, 'fcm_token !=' => null))->result_array();
                    $adminBiro2 = $this->db->get_where('users', array('id_biro' => $id_biro))->result_array();
                    $priority = $this->input->post('priority');
                    foreach ($adminBiro as $adminBiro) {
                        // FIREBASE //
                        $data = ["to" => $adminBiro['fcm_token'], "collapse_key" => "type_a", "notification" => ["body" => "$dataUser->nm_peg membuat tiket terkait $Kategori", "title" => "Tiket Baru! | $priority"]];
                        $url = "https://fcm.googleapis.com/fcm/send";
                        $content = json_encode($data);
                        $curl = curl_init($url);
                        curl_setopt($curl, CURLOPT_CUSTOMREQUEST, "POST");
                        curl_setopt($curl, CURLOPT_HEADER, false);
                        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
                        curl_setopt($curl, CURLOPT_HTTPHEADER, array("Authorization: key=AAAAwQLSs_o:APA91bEiflr1fhqM5mWSBeENyHQm7VwYxSzVtkhfU4ZB_tFPPawpvpkn_ECIUS3gCFxrrtrT8G9emeKU9raS9pnED0LMuCmrE75uU4XZlzUI6U_t-SuKc3VkqHAb5G4_8Y9unS2quOLy", "Content-type: application/json"));
                        curl_setopt($curl, CURLOPT_POST, true);
                        curl_setopt($curl, CURLOPT_POSTFIELDS, $content);
                        $json_response = curl_exec($curl);
                        $status = curl_getinfo($curl, CURLINFO_HTTP_CODE);
                        if ($status != 200) {
                            die("Error: call to URL $url failed with status $status, response $json_response, curl_error " . curl_error($curl) . ", curl_errno " . curl_errno($curl));
                        }
                        curl_close($curl);
                    }
                    foreach ($adminBiro2 as $adminBiro2) {
                        // WHATSAPP //
                        // $number = $adminBiro2['handphone'];
                        $email = $adminBiro2['email'];
                        // $body = "$dataUser->nm_peg membuat tiket terkait $Kategori";
                        // $data = ['phone' => $number, 'body' => $body, ];
                        // $json = json_encode($data);
                        // $url = 'https://eu56.chat-api.com/instance90723/message?token=rle2vpmq3uqw1dfk';
                        // $options = stream_context_create(['http' => ['method' => 'POST', 'header' => 'Content-type: application/json', 'content' => $json]]);
                        // $result = file_get_contents($url, false, $options); ,
                        // // EMAIL //
                        $mess = "$dataUser->nm_peg membuat tiket URGENT terkait $Kategori , pada Sub Kategori $SubKategori . Dengan isi pesan sebagai berikut : ' $pesan '";
                        $this->load->library('email');
                        $this->email->from('ramnaufal@gmail.com', 'Ticket Masuk');
                        $this->email->to($email);
                        $this->email->subject('Ticket Urgent Masuk!');
                        $this->email->message($mess);
                        $send = $this->email->send();

                    }
                    $this->response(["status" => true, "message" => 'Sukses Submit Ticket', ], 200);
                }
            }
        } else {
            $no_inventaris = $this->input->post('no_inventaris');
            $id_sub_kategori = $this->input->post('id_sub_kategori');
            //cek apakah sudah ada tiket serupa dalam kurun waktu 30menit kebelakang
            $currDate = date('Y-m-d H:i:s');
            $toleransiWaktu = date("Y-m-d H:i:s", strtotime("-30 minutes"));
            $cekTiket = $this->db->query("select * from tickets where tanggal_open < '$currDate' and tanggal_open > '$toleransiWaktu' and id_sub_kategori = '$id_sub_kategori'");
            if ($cekTiket->num_rows() > 0) {
                $this->response(["status" => false, "message" => 'Maaf, dalam kurun waktu 30 Menit kebelakang, sudah ada yang melakukan submit ticket terkait jaringan koneksi. Sistem meng indikasikan ini permasalahan yang sama', ], 200);
            } else {
                $data = array('id_user' => $this->input->post('id_user'), 'id_biro' => $id_biro, 'id_kategori' => $this->input->post('id_kategori'), 'id_sub_kategori' => $this->input->post('id_sub_kategori'), 'no_inventaris' => $no_inventaris, 'tanggal_open' => date('Y-m-d H:i:s'), 'tanggal_wp' => null, 'pic_wp' => null, 'tanggal_close' => null, 'pic_close' => null, 'tanggal_done' => null, 'selesai_dalam' => null, 'priority' => $this->input->post('priority'), 'status' => 'open', 'deleted_at' => null,);
                $this->db->insert('tickets', $data);
                $insert_id = $this->db->insert_id();
                $data2 = array('id_ticket' => $insert_id, 'pesan' => $this->input->post('pesan'), 'id_user' => $this->input->post('id_user'), 'id_staff' => null, 'tanggal' => date('Y-m-d H:i:s'));
                $this->db->insert('ticket_message', $data2);
                $iduser = $this->session->userdata('id');
                date_default_timezone_set('Asia/Jakarta');
                $log = ['id_user' => $iduser, 'aktifitas' => 'Membuat Ticket Via Website', 'tanggal' => date('Y-m-d H:i:s') ];
                $this->db->insert('logs', $log);
                $priorityTicket = $this->input->post('priority');
                if ($priorityTicket == 'low') {
                    // PUSH NOTIFICATION LOW//
                    $idUser = $this->input->post('id_user');
                    $dataKategori = $this->db->get_where('kategori', array('id' => $id_kategori))->row();
                    $Kategori = strtoupper($dataKategori->nama_kategori);
                    $dataUser = $this->db->get_where('users', array('id' => $idUser))->row();
                    $adminBiro = $this->db->get_where('users', array('id_biro' => $id_biro, 'fcm_token !=' => null))->result_array();
                    $priority = $this->input->post('priority');
                    foreach ($adminBiro as $adminBiro) {
                        // FIREBASE //
                        $data = ["to" => $adminBiro['fcm_token'], "collapse_key" => "type_a", "notification" => ["body" => "$dataUser->nm_peg membuat tiket terkait $Kategori", "title" => "Tiket Baru! | $priority"]];
                        $url = "https://fcm.googleapis.com/fcm/send";
                        $content = json_encode($data);
                        $curl = curl_init($url);
                        curl_setopt($curl, CURLOPT_CUSTOMREQUEST, "POST");
                        curl_setopt($curl, CURLOPT_HEADER, false);
                        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
                        curl_setopt($curl, CURLOPT_HTTPHEADER, array("Authorization: key=AAAAwQLSs_o:APA91bEiflr1fhqM5mWSBeENyHQm7VwYxSzVtkhfU4ZB_tFPPawpvpkn_ECIUS3gCFxrrtrT8G9emeKU9raS9pnED0LMuCmrE75uU4XZlzUI6U_t-SuKc3VkqHAb5G4_8Y9unS2quOLy", "Content-type: application/json"));
                        curl_setopt($curl, CURLOPT_POST, true);
                        curl_setopt($curl, CURLOPT_POSTFIELDS, $content);
                        $json_response = curl_exec($curl);
                        $status = curl_getinfo($curl, CURLINFO_HTTP_CODE);
                        if ($status != 200) {
                            die("Error: call to URL $url failed with status $status, response $json_response, curl_error " . curl_error($curl) . ", curl_errno " . curl_errno($curl));
                        }
                        curl_close($curl);
                    }
                    $this->response(["status" => true, "message" => 'Sukses Submit Ticket', ], 200);
                } else if ($priorityTicket == 'normal') {
                    //PUSH NOTIFICATION NORMAL //
                    $idUser = $this->input->post('id_user');
                    $dataKategori = $this->db->get_where('kategori', array('id' => $id_kategori))->row();
                    $Kategori = strtoupper($dataKategori->nama_kategori);
                    $dataUser = $this->db->get_where('users', array('id' => $idUser))->row();
                    $adminBiro = $this->db->get_where('users', array('id_biro' => $id_biro, 'fcm_token !=' => null))->result_array();
                    $adminBiro2 = $this->db->get_where('users', array('id_biro' => $id_biro))->result_array();
                    $priority = $this->input->post('priority');
                    // foreach ($adminBiro as $adminBiro) {
                    //     // FIREBASE //
                    //     $data = ["to" => $adminBiro['fcm_token'], "collapse_key" => "type_a", "notification" => ["body" => "$dataUser->nm_peg membuat tiket terkait $Kategori", "title" => "Tiket Baru! | $priority"]];
                    //     $url = "https://fcm.googleapis.com/fcm/send";
                    //     $content = json_encode($data);
                    //     $curl = curl_init($url);
                    //     curl_setopt($curl, CURLOPT_CUSTOMREQUEST, "POST");
                    //     curl_setopt($curl, CURLOPT_HEADER, false);
                    //     curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
                    //     curl_setopt($curl, CURLOPT_HTTPHEADER, array("Authorization: key=AAAAwQLSs_o:APA91bEiflr1fhqM5mWSBeENyHQm7VwYxSzVtkhfU4ZB_tFPPawpvpkn_ECIUS3gCFxrrtrT8G9emeKU9raS9pnED0LMuCmrE75uU4XZlzUI6U_t-SuKc3VkqHAb5G4_8Y9unS2quOLy", "Content-type: application/json"));
                    //     curl_setopt($curl, CURLOPT_POST, true);
                    //     curl_setopt($curl, CURLOPT_POSTFIELDS, $content);
                    //     $json_response = curl_exec($curl);
                    //     $status = curl_getinfo($curl, CURLINFO_HTTP_CODE);
                    //     if ($status != 200) {
                    //         die("Error: call to URL $url failed with status $status, response $json_response, curl_error " . curl_error($curl) . ", curl_errno " . curl_errno($curl));
                    //     }
                    //     curl_close($curl);
                    // }
                    // foreach ($adminBiro2 as $adminBiro2) {
                    //     // WHATSAPP //
                    //     $number = $adminBiro2['handphone'];
                    //     $body = "$dataUser->nm_peg membuat tiket terkait $Kategori";
                    //     $data = ['phone' => $number, 'body' => $body, ];
                    //     $json = json_encode($data);
                    //     $url = 'https://eu56.chat-api.com/instance90723/message?token=rle2vpmq3uqw1dfk';
                    //     $options = stream_context_create(['http' => ['method' => 'POST', 'header' => 'Content-type: application/json', 'content' => $json]]);
                    //     $result = file_get_contents($url, false, $options);
                    //     print_r($result); die;
                    // }
                    $this->response(["status" => true, "message" => 'Sukses Submit Ticket', ], 200);
                } else {
                    //PUSH NOTIFICATION URGENT //
                    $idUser = $this->input->post('id_user');
                    $dataKategori = $this->db->get_where('kategori', array('id' => $id_kategori))->row();
                    $Kategori = strtoupper($dataKategori->nama_kategori);
                    $dataSubKategori = $this->db->get_where('subkategori', array('id' => $id_sub_kategori))->row();
                    $SubKategori = strtoupper($dataSubKategori->nama_sub_kategori);
                    $dataUser = $this->db->get_where('users', array('id' => $idUser))->row();
                    $adminBiro = $this->db->get_where('users', array('id_biro' => $id_biro, 'fcm_token !=' => null))->result_array();
                    $adminBiro2 = $this->db->get_where('users', array('id_biro' => $id_biro))->result_array();
                    $priority = $this->input->post('priority');
                    foreach ($adminBiro as $adminBiro) {
                        // FIREBASE //
                        $data = ["to" => $adminBiro['fcm_token'], "collapse_key" => "type_a", "notification" => ["body" => "$dataUser->nm_peg membuat tiket terkait $Kategori", "title" => "Tiket Baru! | $priority"]];
                        $url = "https://fcm.googleapis.com/fcm/send";
                        $content = json_encode($data);
                        $curl = curl_init($url);
                        curl_setopt($curl, CURLOPT_CUSTOMREQUEST, "POST");
                        curl_setopt($curl, CURLOPT_HEADER, false);
                        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
                        curl_setopt($curl, CURLOPT_HTTPHEADER, array("Authorization: key=AAAAwQLSs_o:APA91bEiflr1fhqM5mWSBeENyHQm7VwYxSzVtkhfU4ZB_tFPPawpvpkn_ECIUS3gCFxrrtrT8G9emeKU9raS9pnED0LMuCmrE75uU4XZlzUI6U_t-SuKc3VkqHAb5G4_8Y9unS2quOLy", "Content-type: application/json"));
                        curl_setopt($curl, CURLOPT_POST, true);
                        curl_setopt($curl, CURLOPT_POSTFIELDS, $content);
                        $json_response = curl_exec($curl);
                        $status = curl_getinfo($curl, CURLINFO_HTTP_CODE);
                        if ($status != 200) {
                            die("Error: call to URL $url failed with status $status, response $json_response, curl_error " . curl_error($curl) . ", curl_errno " . curl_errno($curl));
                        }
                        curl_close($curl);
                    }
                    foreach ($adminBiro2 as $adminBiro2) {
                        // WHATSAPP //
                        // $number = $adminBiro2['handphone'];
                        // $email = $adminBiro2['email'];
                        // $body = "$dataUser->nm_peg membuat tiket terkait $Kategori";
                        // $data = ['phone' => $number, 'body' => $body, ];
                        // $json = json_encode($data);
                        // $url = 'https://eu56.chat-api.com/instance90723/message?token=rle2vpmq3uqw1dfk';
                        // $options = stream_context_create(['http' => ['method' => 'POST', 'header' => 'Content-type: application/json', 'content' => $json]]);
                        // $result = file_get_contents($url, false, $options);
                        // EMAIL //
                        $mess = "$dataUser->nm_peg membuat tiket URGENT terkait $Kategori , pada Sub Kategori $SubKategori . Dengan isi pesan sebagai berikut: ' $pesan '";
                        $this->load->library('email');
                        $this->email->from('ramnaufal@gmail.com', 'Ticket Masuk');
                        $this->email->to($email);
                        $this->email->subject('Ticket Urgent Masuk!');
                        $this->email->message($mess);
                        $send = $this->email->send();

                    }
                    $this->response(["status" => true, "message" => 'Sukses Submit Ticket', ], 200);
                }
            }
        }
    }
    /**
     * Get All Data from this method.
     *
     * @return Response
     */
    public function index_put($id) {
        $status = $this->put('status');
        if ($status == 'done') {
            $input = array('status' => $this->put('status'), 'pic_done' => $this->session->userdata('id'),);
            $this->db->update('tickets', $input, array('id' => $id));
            $this->response(['Item updated successfully.'], REST_Controller::HTTP_OK);
        } else if ($status == 'deleted') {
            $input = array('deleted_at' => date('Y-m-d'), 'deleted_by' => $this->session->userdata('id'),);
            $this->db->update('tickets', $input, array('id' => $id));
            $this->response(['Item updated successfully.'], REST_Controller::HTTP_OK);
        }
    }
    /**
     * Get All Data from this method.
     *
     * @return Response
     */
    public function index_delete($id) {
        $this->db->delete('tickets', array('id' => $id));
        $this->response(['Item deleted successfully.'], REST_Controller::HTTP_OK);
    }
}
