<?php
   
require APPPATH . 'libraries/REST_Controller.php';
     
class Login extends REST_Controller {
    
	  /**
     * Get All Data from this method.
     *
     * @return Response
    */
    public function __construct() {
       parent::__construct();
       $this->load->database();
    }
       
    public function index_post() 
    {
        if ( isset($_POST["nip"]) && isset($_POST["password"]) ) {
            // Get the post data
            $nip = $this->input->post('nip');
            $password = md5($this->input->post('password'));
            
            // Validate the post data
            if( (!empty($nip)) || (!empty($password)) ){
    
                $data = $this->db->get_where('users',array('nip' => $nip, 'password' => $password))->row();
                if($data){
                    // Set the response and exit and session
                    if ($data->status != 'aktif'){
                        $this->response([
                            "status" => false,
                            "message" => 'Akun anda tidak aktif!',
                          ], 200);
                    } else {
                        $sess_data['id'] = $data->id;
                        $sess_data['nip'] = $data->nip;
                        $sess_data['nm_peg'] = $data->nm_peg;
                        $sess_data['password'] = $data->password;
                        $sess_data['status'] = $data->status;
                        $sess_data['jns_kelamin_peg'] = $data->jns_kelamin_peg;
                        $sess_data['alamat'] = $data->alamat;
                        $sess_data['kodepos'] = $data->kodepos;
                        $sess_data['telepon'] = $data->telepon;
                        $sess_data['handphone'] = $data->handphone;
                        $sess_data['email'] = $data->email;
                        $sess_data['kd_jabatan'] = $data->kd_jabatan;
                        $sess_data['nm_jabatan'] = $data->nm_jabatan;
                        $sess_data['kd_unit_org'] = $data->kd_unit_org;
                        $sess_data['nm_unit_org'] = $data->nm_unit_org;
                        $sess_data['id_biro'] = $data->id_biro;
                        $sess_data['admin'] = $data->admin;
                        $sess_data['superadmin'] = $data->superadmin;
                        $sess_data['level'] = $data->level;
                        $sess_data['status_login'] = 'login';
                        $sess_data['pw'] = $this->input->post('password');

                        $this->session->set_userdata($sess_data);
                        date_default_timezone_set('Asia/Jakarta');
                        $log = [
                            'id_user' =>  $data->id,
                            'aktifitas' => 'Login Website',
                            'tanggal' => date('Y-m-d H:i:s')
                        ];
                        $this->db->insert('logs',$log);

                    $this->response([
                        "status" => true,
                        "message" => 'Selamat datang!',
                        "data" => $data,
                      ], 200);
                    //   redirect ('index.php/admin');
                    }
                } else {
                    $this->response([
                        "status" => false,
                        "message" => 'NIP atau Password tidak sesuai!',
                      ], 200);
                }
            }else{
                // Set the response and exit
                $this->response([
                    "status" => false,
                    "message" => 'Lengkapi NIP dan Password!',
                  ], 200);
            }

        } else {
            $this->response([
                "status" => false,
                "message" => 'Lengkapi NIP dan Password!',
              ], 400);
        }
    }

}