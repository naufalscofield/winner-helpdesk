<?php
   
require APPPATH . 'libraries/REST_Controller.php';
     
class MessageUsers extends REST_Controller {
    
	  /**
     * Get All Data from this method.
     *
     * @return Response
    */
    public function __construct() {
       parent::__construct();
       $this->load->database();
    }
       
    /**
     * Get All Data from this method.
     *
     * @return Response
    */
	public function index_get($id)
	{
        $this->db->join('users','users.id = ticket_message.id_user');
        $this->db->join('staff','staff.id = ticket_message.id_staff');
        $this->db->select('*,users.id as id_user, staff.id as id_staff, ticket_message.id as id_message');
        $data = $this->db->get("ticket_message", array('id_ticket' => $id))->result();
        $dataF = json_encode($data); 
        $this->response($dataF, REST_Controller::HTTP_OK);
	}
      
    /**
     * Get All Data from this method.
     *
     * @return Response
    */
    public function index_post()
    {
        // $input = $this->input->post();
        $data = array(
            'id_user' => $this->input->post('id_user'),
            'pesan' => $this->input->post('pesan'),
            'id_ticket' => $this->input->post('id_ticket'),
            'tanggal' => date('Y-m-d'),
        );
        $this->db->insert('ticket_message',$data);
     
        $this->response(['Item created successfully.'], REST_Controller::HTTP_OK);
    } 
     
    /**
     * Get All Data from this method.
     *
     * @return Response
    */
    public function index_put($id)
    {
        $input = $this->put();
        $this->db->update('kategori', $input, array('id'=>$id));
     
        $this->response(['Item updated successfully.'], REST_Controller::HTTP_OK);
    }
     
    /**
     * Get All Data from this method.
     *
     * @return Response
    */
    public function index_delete($id)
    {
        $this->db->delete('kategori', array('id'=>$id));
       
        $this->response(['Item deleted successfully.'], REST_Controller::HTTP_OK);
    }
    	
}