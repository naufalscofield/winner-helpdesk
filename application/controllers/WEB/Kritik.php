<?php
   
require APPPATH . 'libraries/REST_Controller.php';
     
class Kritik extends REST_Controller {
    
	  /**
     * Get All Data from this method.
     *
     * @return Response
    */
    public function __construct() {
        parent::__construct();
        $this->load->database();
        
        // require_once __DIR__.'/lib/PHPInsight/AutoloaderSenti.php';
        // PHPInsight\AutoloaderSenti::register();  
    }
       
    /**
     * Get All Data from this method.
     *
     * @return Response
    */
	public function index_get($id_biro)
	{
        // print_r($id_biro);
        $this->db->join('users a','a.id = b.id_user');
        $this->db->select('*, b.id as idkritik');
        $data = $this->db->get_where("kritik b",['b.id_biro_tujuan' => $id_biro])->result();
        $this->response($data, REST_Controller::HTTP_OK);
	}
      
    /**
     * Get All Data from this method.
     *
     * @return Response
    */
    public function index_post()
    {
        $strings = array(
            1 => $this->input->post('pesan'),
        );
    
        require_once __DIR__ . '/autoload.php';
        $sentiment = new \PHPInsight\Sentiment();
    
        $i=1;
        foreach ($strings as $string) {
    
            // calculations:
            $scores = $sentiment->score($string);
            $class = $sentiment->categorise($string);
    
            // output:
            if (in_array("pos", $scores)) {
                echo "Got positif";
            }
            
            $arahSentimen = $class;
            foreach ($scores as $skor) {
                // echo $skor;
                
            }
            echo "\n\n";
            $i++;
        }
        $data = [
            'id_user' => $this->input->post('id_user'),
            'id_biro_tujuan' => $this->input->post('id_biro_tujuan'),
            'arah_sentiment' => $class,
            'score' => $skor,
            'pesan' => $this->input->post('pesan'),
        ];
        // print_r($data); die;

        $this->db->insert('kritik',$data);
     
        $this->response([
            "status" => true,
            "sentiment" => $class,
            "skor" => $skor,
          ], 200);
    } 
     
    /**
     * Get All Data from this method.
     *
     * @return Response
    */
    public function index_put($id)
    {
        $data = $this->db->get_where("kritik",['id' => $id])->row();
        
        $this->response($data, REST_Controller::HTTP_OK);
    }
     
    /**
     * Get All Data from this method.
     *
     * @return Response
    */
    public function index_delete($id)
    {
        $this->db->delete('biro', array('id'=>$id));

        $iduser = $this->session->userdata('id');
        date_default_timezone_set('Asia/Jakarta');
		$log = [
			'id_user' =>  $iduser,
			'aktifitas' => 'Menghapus Biro',
			'tanggal' => date('Y-m-d H:i:s')
		];
		$this->db->insert('logs',$log);
       
        $this->response(['Item deleted successfully.'], REST_Controller::HTTP_OK);
    }
    	
}