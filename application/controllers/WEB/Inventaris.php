<?php
   
require APPPATH . 'libraries/REST_Controller.php';
     
class Inventaris extends REST_Controller {
    
	  /**
     * Get All Data from this method.
     *
     * @return Response
    */
    public function __construct() {
       parent::__construct();
       $this->load->database();
    }
       
    /**
     * Get All Data from this method.
     *
     * @return Response
    */
	public function index_get($id = 0)
	{
        if(!empty($id)){
            $this->db->join('sarana','sarana.id = inventaris.id_sarana');
            $this->db->join('lokasi','lokasi.id = inventaris.id_lokasi');
            $this->db->select('*,inventaris.id as idinventaris');
            $data = $this->db->get_where("inventaris", ['inventaris.id' => $id])->row_array();
        }else{
            $this->db->join('sarana','sarana.id = inventaris.id_sarana');
            $this->db->join('lokasi','lokasi.id = inventaris.id_lokasi');
            $this->db->select('*,inventaris.id as idinventaris');
            $data = $this->db->get("inventaris")->result();
            // $data = $this->db->query('SELECT inventaris.id,inventaris.nama_inventaris, count(inventaris.id) as jml_staff FROM inventaris inner join users on users.id_inventaris = inventaris.id where users.admin = "yes" GROUP BY inventaris.id')->result();
        }
     
        $this->response($data, REST_Controller::HTTP_OK);
	}
      
    /**
     * Get All Data from this method.
     *
     * @return Response
    */
    public function index_post()
    {
        $id_sarana = $this->input->post('id_sarana');
        $id_lokasi = $this->input->post('id_lokasi');
        $cekUrutan = $this->db->get_where('inventaris',['id_sarana' => $id_sarana, 'id_lokasi' => $id_lokasi]);
        $cekUrutan = $cekUrutan->num_rows();
        $cekUrutan += 1;
        $date = date('Y-m-d');
        $data = array(
            'no_inventaris' => "WIE/$id_sarana/$id_lokasi/$cekUrutan/$date",
            'id_sarana' => $id_sarana,
            'id_lokasi' => $id_lokasi,
        );
        $this->db->insert('inventaris',$data);

        $iduser = $this->session->userdata('id');
		date_default_timezone_set('Asia/Jakarta');
		$log = [
			'id_user' =>  $iduser,
			'aktifitas' => 'Menambahkan inventaris Baru, inventaris '.$data['no_inventaris'],
			'tanggal' => date('Y-m-d H:i:s')
		];
		$this->db->insert('logs',$log);
     
        $this->response(['Item created successfully.'], REST_Controller::HTTP_OK);
    } 
     
    /**
     * Get All Data from this method.
     *
     * @return Response
    */
    public function index_put($id)
    {
        $input = $this->put();
        $this->db->update('inventaris', $input, array('id'=>$id));

        $iduser = $this->session->userdata('id');
        date_default_timezone_set('Asia/Jakarta');
		$log = [
			'id_user' =>  $iduser,
			'aktifitas' => 'Memperbarui Nama inventaris',
			'tanggal' => date('Y-m-d H:i:s')
		];
		$this->db->insert('logs',$log);
     
        $this->response(['Item updated successfully.'], REST_Controller::HTTP_OK);
    }
     
    /**
     * Get All Data from this method.
     *
     * @return Response
    */
    public function index_delete($id)
    {
        $this->db->delete('inventaris', array('id'=>$id));

        $iduser = $this->session->userdata('id');
        date_default_timezone_set('Asia/Jakarta');
		$log = [
			'id_user' =>  $iduser,
			'aktifitas' => 'Menghapus inventaris',
			'tanggal' => date('Y-m-d H:i:s')
		];
		$this->db->insert('logs',$log);
       
        $this->response(['Item deleted successfully.'], REST_Controller::HTTP_OK);
    }
    	
}