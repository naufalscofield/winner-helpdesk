<?php
   
require APPPATH . 'libraries/REST_Controller.php';
     
class Logs extends REST_Controller {
    
	  /**
     * Get All Data from this method.
     *
     * @return Response
    */
    public function __construct() {
       parent::__construct();
       $this->load->database();
    }
       
    /**
     * Get All Data from this method.
     *
     * @return Response
    */
	public function index_get($id = 0)
	{
        if(!empty($id)){
            $this->db->join('users','users.id = logs.id_user');
            $this->db->join('biro','biro.id = users.id_biro');
            $data = $this->db->get_where("logs", ['id' => $id])->row_array();
        }else{
            $this->db->join('users','users.id = logs.id_user');
            $this->db->join('biro','biro.id = users.id_biro');
            $data = $this->db->get("logs")->result();
            // $data = $this->db->query('SELECT logs.id,logs.nama_logs, count(logs.id) as jml_staff FROM logs inner join users on users.id_logs = logs.id where users.admin = "yes" GROUP BY logs.id')->result();
        }
     
        $this->response($data, REST_Controller::HTTP_OK);
	}
      
    /**
     * Get All Data from this method.
     *
     * @return Response
    */
    public function index_post()
    {
        // $input = $this->input->post();
        $data = array(
            'nama_logs' => $this->input->post('nama_logs'),
        );
        $this->db->insert('logs',$data);

        $iduser = $this->session->userdata('id');
		date_default_timezone_set('Asia/Jakarta');
		$log = [
			'id_user' =>  $iduser,
			'aktifitas' => 'Menambahkan logs Baru, logs '.$data['nama_logs'],
			'tanggal' => date('Y-m-d H:i:s')
		];
		$this->db->insert('logs',$log);
     
        $this->response(['Item created successfully.'], REST_Controller::HTTP_OK);
    } 
     
    /**
     * Get All Data from this method.
     *
     * @return Response
    */
    public function index_put($id)
    {
        $input = $this->put();
        $this->db->update('logs', $input, array('id'=>$id));

        $iduser = $this->session->userdata('id');
        date_default_timezone_set('Asia/Jakarta');
		$log = [
			'id_user' =>  $iduser,
			'aktifitas' => 'Memperbarui Nama logs',
			'tanggal' => date('Y-m-d H:i:s')
		];
		$this->db->insert('logs',$log);
     
        $this->response(['Item updated successfully.'], REST_Controller::HTTP_OK);
    }
     
    /**
     * Get All Data from this method.
     *
     * @return Response
    */
    public function index_delete($id)
    {
        $this->db->delete('logs', array('id'=>$id));

        $iduser = $this->session->userdata('id');
        date_default_timezone_set('Asia/Jakarta');
		$log = [
			'id_user' =>  $iduser,
			'aktifitas' => 'Menghapus logs',
			'tanggal' => date('Y-m-d H:i:s')
		];
		$this->db->insert('logs',$log);
       
        $this->response(['Item deleted successfully.'], REST_Controller::HTTP_OK);
    }
    	
}