<?php
   
require APPPATH . 'libraries/REST_Controller.php';
     
class Settings extends REST_Controller {
    
	  /**
     * Get All Data from this method.
     *
     * @return Response
    */
    public function __construct() {
       parent::__construct();
       $this->load->database();
    }
       
    /**
     * Get All Data from this method.
     *
     * @return Response
    */
	public function index_get($id = 0)
	{
        if(!empty($id)){
            // $this->db->join('biro','biro.id = settings.id_biro');
            // $this->db->select('*,biro.id as id_biro, settings.id as id_settings');
            $data = $this->db->get_where("settings", ['id' => $id])->row_array();
        }else{
            // $this->db->join('users','users.id = settings.id_user');
            // $this->db->join('biro','biro.id = settings.id_biro');
            // $this->db->join('kategori','kategori.id = settings.id_kategori');
            // $this->db->select('*,users.id as id_user, biro.id as id_biro, kategori.id as id_kategori, users.status as status_user, settings.status as status_ticket');
            $data = $this->db->get("settings")->result();
        }
     
        $this->response($data, REST_Controller::HTTP_OK);
	}
      
    /**
     * Get All Data from this method.
     *
     * @return Response
    */
    public function index_post()
    {
        // $input = $this->input->post();
        $data = array(
            'nama_settings' => $this->input->post('nama_settings'),
            'id_biro' => $this->input->post('id_biro'),
        );
        $this->db->insert('settings',$data);
     
        $this->response(['Item created successfully.'], REST_Controller::HTTP_OK);
    } 
     
    /**
     * Get All Data from this method.
     *
     * @return Response
    */
    public function index_put($id)
    {
        $input = $this->put();
        $this->db->update('settings', $input, array('id'=>$id));

        $iduser = $this->session->userdata('id');
        date_default_timezone_set('Asia/Jakarta');
		$log = [
			'id_user' =>  $iduser,
			'aktifitas' => 'Memperbarui Pengaturan dan Info Sistem',
			'tanggal' => date('Y-m-d H:i:s')
		];
		$this->db->insert('logs',$log);
     
        $this->response(['Item updated successfully.'], REST_Controller::HTTP_OK);
    }
     
    /**
     * Get All Data from this method.
     *
     * @return Response
    */
    public function index_delete($id)
    {
        $this->db->delete('settings', array('id'=>$id));
       
        $this->response(['Item deleted successfully.'], REST_Controller::HTTP_OK);
    }
    	
}