<?php
   
require APPPATH . 'libraries/REST_Controller.php';
     
class ReportSLA extends REST_Controller {
    
	  /**
     * Get All Data from this method.
     *
     * @return Response
    */
    public function __construct() {
       parent::__construct();
       $this->load->database();
    }
       
    /**
     * Get All Data from this method.
     *
     * @return Response
    */
	public function index_get()
	{
        $date = date('Y');
        
        $jan = $this->db->query("SELECT sum(selesai_dalam) as total_jam FROM tickets WHERE YEAR(tanggal_open) = $date AND MONTH(tanggal_open) = 01 and id_sub_kategori = 1 and status = 'done'")->row();
        $jan = $jan->total_jam;
        // $jan = $jan / 24;
        $jan = round($jan);
        
        $feb = $this->db->query("SELECT sum(selesai_dalam) as total_jam FROM tickets WHERE YEAR(tanggal_open) = $date AND MONTH(tanggal_open) = 02 and id_sub_kategori = 1 and status = 'done'")->row();
        $feb = $feb->total_jam;
        // $feb = $feb / 24;
        $feb = round($feb);
        
        $mar = $this->db->query("SELECT sum(selesai_dalam) as total_jam FROM tickets WHERE YEAR(tanggal_open) = $date AND MONTH(tanggal_open) = 03 and id_sub_kategori = 1 and status = 'done'")->row();
        $mar = $mar->total_jam;
        // $mar = $mar / 24;
        $mar = round($mar);
        
        $apr = $this->db->query("SELECT sum(selesai_dalam) as total_jam FROM tickets WHERE YEAR(tanggal_open) = $date AND MONTH(tanggal_open) = 04 and id_sub_kategori = 1 and status = 'done'")->row();
        $apr = $apr->total_jam;
        // $apr = $apr / 24;
        $apr = round($apr);
        
        $may = $this->db->query("SELECT sum(selesai_dalam) as total_jam FROM tickets WHERE YEAR(tanggal_open) = $date AND MONTH(tanggal_open) = 05 and id_sub_kategori = 1 and status = 'done'")->row();
        $may = $may->total_jam;
        // $may = $may / 24;
        $may = round($may);
        
        $jun = $this->db->query("SELECT sum(selesai_dalam) as total_jam FROM tickets WHERE YEAR(tanggal_open) = $date AND MONTH(tanggal_open) = 06 and id_sub_kategori = 1 and status = 'done'")->row();
        $jun = $jun->total_jam;
        // $jun = $jun / 24;
        $jun = round($jun);
        
        $jul = $this->db->query("SELECT sum(selesai_dalam) as total_jam FROM tickets WHERE YEAR(tanggal_open) = $date AND MONTH(tanggal_open) = 07 and id_sub_kategori = 1 and status = 'done'")->row();
        $jul = $jul->total_jam;
        // $jul = $jul / 24;
        $jul = round($jul);
        
        $aug = $this->db->query("SELECT sum(selesai_dalam) as total_jam FROM tickets WHERE YEAR(tanggal_open) = $date AND MONTH(tanggal_open) = 08 and id_sub_kategori = 1 and status = 'done'")->row();
        $aug = $aug->total_jam;
        // $aug = $aug / 24;
        $aug = round($aug);
        
        $sep = $this->db->query("SELECT sum(selesai_dalam) as total_jam FROM tickets WHERE YEAR(tanggal_open) = $date AND MONTH(tanggal_open) = 09 and id_sub_kategori = 1 and status = 'done'")->row();
        $sep = $sep->total_jam;
        // $sep = $sep / 24;
        $sep = round($sep);
        
        $okt = $this->db->query("SELECT sum(selesai_dalam) as total_jam FROM tickets WHERE YEAR(tanggal_open) = $date AND MONTH(tanggal_open) = 10 and id_sub_kategori = 1 and status = 'done'")->row();
        $okt = $okt->total_jam;
        // $okt = $okt / 24;
        $okt = round($okt);
        
        $nov = $this->db->query("SELECT sum(selesai_dalam) as total_jam FROM tickets WHERE YEAR(tanggal_open) = $date AND MONTH(tanggal_open) = 11 and id_sub_kategori = 1 and status = 'done'")->row();
        $nov = $nov->total_jam;
        // $nov = $nov / 24;
        $nov = round($nov);
        
        $des = $this->db->query("SELECT sum(selesai_dalam) as total_jam FROM tickets WHERE YEAR(tanggal_open) = $date AND MONTH(tanggal_open) = 12 and id_sub_kategori = 1 and status = 'done'")->row();
        $des = $des->total_jam;
        // $des = $des / 24;
        $des = round($des);
        
        $totFeb = $jan + $feb;
        $totMar = $jan + $feb + $mar;
        $totApr = $jan + $feb + $mar + $apr;
        $totMay = $jan + $feb + $mar + $apr + $may;
        $totJun = $jan + $feb + $mar + $apr + $may + $jun;
        $totJul = $jan + $feb + $mar + $apr + $may + $jun + $jul;
        $totAug = $jan + $feb + $mar + $apr + $may + $jun + $jul + $aug;
        $totSep = $jan + $feb + $mar + $apr + $may + $jun + $jul + $aug + $sep;
        $totOkt = $jan + $feb + $mar + $apr + $may + $jun + $jul + $aug + $sep + $okt;
        $totNov = $jan + $feb + $mar + $apr + $may + $jun + $jul + $aug + $sep + $okt + $nov;
        $totDes = $jan + $feb + $mar + $apr + $may + $jun + $jul + $aug + $sep + $okt + $nov + $des;

        $data = [
            $jan, 
            $totFeb,
            $totMar,
            $totApr,
            $totMay,
            $totJun,
            $totJul,
            $totAug,
            $totSep,
            $totOkt,
            $totNov,
            $totDes
        ];
     
        $this->response($data, REST_Controller::HTTP_OK);
	}
      
    /**
     * Get All Data from this method.
     *
     * @return Response
    */
    public function index_post()
    {
        // $input = $this->input->post();
        $data = array(
            'nama_biro' => $this->input->post('nama_biro'),
        );
        $this->db->insert('biro',$data);
     
        $this->response(['Item created successfully.'], REST_Controller::HTTP_OK);
    } 
     
    /**
     * Get All Data from this method.
     *
     * @return Response
    */
    public function index_put($id)
    {
        $input = $this->put();
        $this->db->update('biro', $input, array('id'=>$id));
     
        $this->response(['Item updated successfully.'], REST_Controller::HTTP_OK);
    }
     
    /**
     * Get All Data from this method.
     *
     * @return Response
    */
    public function index_delete($id)
    {
        $this->db->delete('biro', array('id'=>$id));
       
        $this->response(['Item deleted successfully.'], REST_Controller::HTTP_OK);
    }
    	
}