<?php
   
require APPPATH . 'libraries/REST_Controller.php';
     
class Sarana extends REST_Controller {
    
	  /**
     * Get All Data from this method.
     *
     * @return Response
    */
    public function __construct() {
       parent::__construct();
       $this->load->database();
    }
       
    /**
     * Get All Data from this method.
     *
     * @return Response
    */
	public function index_get($id = 0)
	{
        if(!empty($id)){
            $data = $this->db->get_where("sarana", ['id' => $id])->row_array();
        }else{
            $data = $this->db->get("sarana")->result();
            // $data = $this->db->query('SELECT sarana.id,sarana.nama_sarana, count(sarana.id) as jml_staff FROM sarana inner join users on users.id_sarana = sarana.id where users.admin = "yes" GROUP BY sarana.id')->result();
        }
     
        $this->response($data, REST_Controller::HTTP_OK);
	}
      
    /**
     * Get All Data from this method.
     *
     * @return Response
    */
    public function index_post()
    {
        // $input = $this->input->post();
        $data = array(
            'nama_sarana' => $this->input->post('nama_sarana'),
        );
        $this->db->insert('sarana',$data);

        $iduser = $this->session->userdata('id');
		date_default_timezone_set('Asia/Jakarta');
		$log = [
			'id_user' =>  $iduser,
			'aktifitas' => 'Menambahkan sarana Baru, sarana '.$data['nama_sarana'],
			'tanggal' => date('Y-m-d H:i:s')
		];
		$this->db->insert('logs',$log);
     
        $this->response(['Item created successfully.'], REST_Controller::HTTP_OK);
    } 
     
    /**
     * Get All Data from this method.
     *
     * @return Response
    */
    public function index_put($id)
    {
        $input = $this->put();
        $this->db->update('sarana', $input, array('id'=>$id));

        $iduser = $this->session->userdata('id');
        date_default_timezone_set('Asia/Jakarta');
		$log = [
			'id_user' =>  $iduser,
			'aktifitas' => 'Memperbarui Nama sarana',
			'tanggal' => date('Y-m-d H:i:s')
		];
		$this->db->insert('logs',$log);
     
        $this->response(['Item updated successfully.'], REST_Controller::HTTP_OK);
    }
     
    /**
     * Get All Data from this method.
     *
     * @return Response
    */
    public function index_delete($id)
    {
        $this->db->delete('sarana', array('id'=>$id));

        $iduser = $this->session->userdata('id');
        date_default_timezone_set('Asia/Jakarta');
		$log = [
			'id_user' =>  $iduser,
			'aktifitas' => 'Menghapus sarana',
			'tanggal' => date('Y-m-d H:i:s')
		];
		$this->db->insert('logs',$log);
       
        $this->response(['Item deleted successfully.'], REST_Controller::HTTP_OK);
    }
    	
}