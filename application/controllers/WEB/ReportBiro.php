<?php
   
require APPPATH . 'libraries/REST_Controller.php';
     
class ReportBiro extends REST_Controller {
    
	  /**
     * Get All Data from this method.
     *
     * @return Response
    */
    public function __construct() {
       parent::__construct();
       $this->load->database();
    }
       
    /**
     * Get All Data from this method.
     *
     * @return Response
    */
	public function index_get()
	{
        
        $data = $this->db->query("SELECT COUNT(a.id_biro) as jml, b.nama_biro FROM tickets a INNER JOIN biro b ON a.id_biro = b.id GROUP BY a.id_biro ORDER BY jml DESC LIMIT 5")->result_array();
        // $data = $this->db->query('SELECT biro.id,biro.nama_biro, count(biro.id) as jml_staff FROM biro inner join users on users.id_biro = biro.id where users.admin = "yes" GROUP BY biro.id')->result();
        
     
        $this->response($data, REST_Controller::HTTP_OK);
	}
      
    /**
     * Get All Data from this method.
     *
     * @return Response
    */
    public function index_post()
    {
        // $input = $this->input->post();
        $data = array(
            'nama_biro' => $this->input->post('nama_biro'),
        );
        $this->db->insert('biro',$data);
     
        $this->response(['Item created successfully.'], REST_Controller::HTTP_OK);
    } 
     
    /**
     * Get All Data from this method.
     *
     * @return Response
    */
    public function index_put($id)
    {
        $input = $this->put();
        $this->db->update('biro', $input, array('id'=>$id));
     
        $this->response(['Item updated successfully.'], REST_Controller::HTTP_OK);
    }
     
    /**
     * Get All Data from this method.
     *
     * @return Response
    */
    public function index_delete($id)
    {
        $this->db->delete('biro', array('id'=>$id));
       
        $this->response(['Item deleted successfully.'], REST_Controller::HTTP_OK);
    }
    	
}