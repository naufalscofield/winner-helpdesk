<?php
   
require APPPATH . 'libraries/REST_Controller.php';
     
class SubKategori extends REST_Controller {
    
	  /**
     * Get All Data from this method.
     *
     * @return Response
    */
    public function __construct() {
       parent::__construct();
       $this->load->database();
    }
       
    /**
     * Get All Data from this method.
     *
     * @return Response
    */
	public function index_get($id = 0)
	{
        $id_biro = $this->session->userdata('id_biro');
        if(!empty($id)){
            $this->db->join('kategori','kategori.id = subkategori.id_kategori');
            $this->db->select('*,kategori.id as id_kategori, subkategori.id as id_sub_kategori');
            $data = $this->db->get_where("subkategori", ['subkategori.id' => $id])->row_array();
        }else{
            $this->db->join('kategori','kategori.id = subkategori.id_kategori');
            $this->db->select('*,kategori.id as id_kategori, subkategori.id as id_sub_kategori');
            $data = $this->db->get_where("subkategori", ['subkategori.id_biro' => $id_biro])->result();
            // print_r($data); die;
        }
     
        $this->response($data, REST_Controller::HTTP_OK);
	}
      
    /**
     * Get All Data from this method.
     *
     * @return Response
    */
    public function index_post()
    {
        // $input = $this->input->post();
        $data = array(
            'nama_sub_kategori' => $this->input->post('nama_sub_kategori'),
            'id_kategori' => $this->input->post('id_kategori'),
            'id_biro' => $this->session->userdata('id_biro'),
        );
        $this->db->insert('subkategori',$data);

        $iduser = $this->session->userdata('id');
        date_default_timezone_set('Asia/Jakarta');
		$log = [
			'id_user' =>  $iduser,
			'aktifitas' => 'Menambahkan Sub Kategori Baru, Kategori '.$data['nama_sub_kategori'],
			'tanggal' => date('Y-m-d H:i:s')
		];
		$this->db->insert('logs',$log);
     
        $this->response(['Item created successfully.'], REST_Controller::HTTP_OK);
    } 
     
    /**
     * Get All Data from this method.
     *
     * @return Response
    */
    public function index_put($id)
    {
        $input = $this->put();
        $this->db->update('subkategori', $input, array('id'=>$id));
     
        $iduser = $this->session->userdata('id');
        date_default_timezone_set('Asia/Jakarta');
		$log = [
			'id_user' =>  $iduser,
			'aktifitas' => 'Memperbarui Nama Sub Kategori',
			'tanggal' => date('Y-m-d H:i:s')
		];
        $this->db->insert('logs',$log);
        
        $this->response(['Item updated successfully.'], REST_Controller::HTTP_OK);
    }
     
    /**
     * Get All Data from this method.
     *
     * @return Response
    */
    public function index_delete($id)
    {
        $this->db->delete('subkategori', array('id'=>$id));
       
        $iduser = $this->session->userdata('id');
        date_default_timezone_set('Asia/Jakarta');
		$log = [
			'id_user' =>  $iduser,
			'aktifitas' => 'Menghapus Sub Kategori',
			'tanggal' => date('Y-m-d H:i:s')
		];
        $this->db->insert('logs',$log);
        
        $this->response(['Item deleted successfully.'], REST_Controller::HTTP_OK);
    }
    	
}