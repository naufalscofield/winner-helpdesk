<?php
   
require APPPATH . 'libraries/REST_Controller.php';
     
class Stepper extends REST_Controller {
    
	  /**
     * Get All Data from this method.
     *
     * @return Response
    */
    public function __construct() {
       parent::__construct();
       $this->load->database();
    }
       
    /**
     * Get All Data from this method.
     *
     * @return Response
    */
	public function index_get($id)
	{
        $this->db->join('users b','b.id = a.id_user', 'left');
        $this->db->join('users c','c.id = a.pic_wp', 'left');
        $this->db->join('users d','d.id = a.pic_close', 'left');
        $this->db->join('users e','e.id = a.pic_done', 'left');
        $this->db->select('*,b.nm_peg as pic_open,c.nm_peg as pic_wp,d.nm_peg as pic_closed,e.nm_peg as pic_done,a.status as statusticket');
        $data = $this->db->get_where("tickets a", ['a.id' => $id])->result();
        // $data = $this->db->query('SELECT lokasi.id,lokasi.nama_lokasi, count(lokasi.id) as jml_staff FROM lokasi inner join users on users.id_lokasi = lokasi.id where users.admin = "yes" GROUP BY lokasi.id')->result();
        // print_r($data); die;
        $this->response($data, REST_Controller::HTTP_OK);
	}
      
    /**
     * Get All Data from this method.
     *
     * @return Response
    */
    public function index_post()
    {
        // $input = $this->input->post();
        $data = array(
            'nama_lokasi' => $this->input->post('nama_lokasi'),
        );
        $this->db->insert('lokasi',$data);

        $iduser = $this->session->userdata('id');
		date_default_timezone_set('Asia/Jakarta');
		$log = [
			'id_user' =>  $iduser,
			'aktifitas' => 'Menambahkan lokasi Baru, lokasi '.$data['nama_lokasi'],
			'tanggal' => date('Y-m-d H:i:s')
		];
		$this->db->insert('logs',$log);
     
        $this->response(['Item created successfully.'], REST_Controller::HTTP_OK);
    } 
     
    /**
     * Get All Data from this method.
     *
     * @return Response
    */
    public function index_put($id)
    {
        $input = $this->put();
        $this->db->update('lokasi', $input, array('id'=>$id));

        $iduser = $this->session->userdata('id');
        date_default_timezone_set('Asia/Jakarta');
		$log = [
			'id_user' =>  $iduser,
			'aktifitas' => 'Memperbarui Nama lokasi',
			'tanggal' => date('Y-m-d H:i:s')
		];
		$this->db->insert('logs',$log);
     
        $this->response(['Item updated successfully.'], REST_Controller::HTTP_OK);
    }
     
    /**
     * Get All Data from this method.
     *
     * @return Response
    */
    public function index_delete($id)
    {
        $this->db->delete('lokasi', array('id'=>$id));

        $iduser = $this->session->userdata('id');
        date_default_timezone_set('Asia/Jakarta');
		$log = [
			'id_user' =>  $iduser,
			'aktifitas' => 'Menghapus lokasi',
			'tanggal' => date('Y-m-d H:i:s')
		];
		$this->db->insert('logs',$log);
       
        $this->response(['Item deleted successfully.'], REST_Controller::HTTP_OK);
    }
    	
}