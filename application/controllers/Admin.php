<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Admin extends CI_Controller {
	public function __construct()
	{
		parent::__construct();
		$this->load->model('model_admin');
		if ($this->session->userdata('status_login') != 'login'){
			redirect('index.php/login');
		}	
	}

	public function index()
	{
		$this->load->view('components_admin/header');
		$this->load->view('components_admin/sidebar');
		$this->load->view('admin/dashboard');
		$this->load->view('components_admin/footer');
	}
	
	public function getUsers()
	{
		$this->load->view('components_admin/header');
		$this->load->view('components_admin/sidebar');
		$this->load->view('admin/users');
		$this->load->view('components_admin/footer');
	}
	
	public function getStaff()
	{
		$this->load->view('components_admin/header');
		$this->load->view('components_admin/sidebar');
		$this->load->view('admin/staff');
		$this->load->view('components_admin/footer');
	}
	
	public function getBiro()
	{
		$this->load->view('components_admin/header');
		$this->load->view('components_admin/sidebar');
		$this->load->view('admin/biro');
		$this->load->view('components_admin/footer');
	}
	
	public function getKategori()
	{
		$this->load->view('components_admin/header');
		$this->load->view('components_admin/sidebar');
		$this->load->view('admin/kategori');
		$this->load->view('components_admin/footer');
	}
	
	public function getTickets()
	{
		$this->load->view('components_admin/header');
		$this->load->view('components_admin/sidebar');
		$this->load->view('admin/tickets');
		$this->load->view('components_admin/footer');
	}
	
	public function getSettings()
	{
		$this->load->view('components_admin/header');
		$this->load->view('components_admin/sidebar');
		$this->load->view('admin/settings');
		$this->load->view('components_admin/footer');
	}
	
	public function getProfile()
	{
		$data['biro'] = $this->db->get('biro')->result_array();
		$data['user'] = array (
			array(

			'id' 				=> $this->session->userdata('id'),
			'nip' 				=> $this->session->userdata('nip'),
			'nm_peg' 			=> $this->session->userdata('nm_peg'),
			'password' 			=> $this->session->userdata('password'),
			'status' 			=> $this->session->userdata('status'),
			'jns_kelamin_peg' 	=> $this->session->userdata('jns_kelamin_peg'),
			'alamat' 			=> $this->session->userdata('alamat'),
			'kodepos' 			=> $this->session->userdata('kodepos'),
			'telepon' 			=> $this->session->userdata('telepon'),
			'handphone'			=> $this->session->userdata('handphone'),
			'email' 			=> $this->session->userdata('email'),
			'kd_jabatan' 		=> $this->session->userdata('kd_jabatan'),
			'nm_jabatan' 		=> $this->session->userdata('nm_jabatan'),
			'kd_unit_org' 		=> $this->session->userdata('kd_unit_org'),
			'nm_unit_org' 		=> $this->session->userdata('nm_unit_org'),
			'id_biro' 			=> $this->session->userdata('id_biro'),
			'admin' 			=> $this->session->userdata('admin'),
			'status_login' 		=> $this->session->userdata('status_login'),
			'pw' 				=> $this->session->userdata('pw')
			)
		);

		// print_r($data['user']); die;
		
		$this->load->view('components_admin/header');
		$this->load->view('components_admin/sidebar');
		$this->load->view('admin/profile',$data);
		$this->load->view('components_admin/footer');
	}

	public function logout()
    {
		$iduser = $this->session->userdata('id');
		date_default_timezone_set('Asia/Jakarta');
		$log = [
			'id_user' =>  $iduser,
			'aktifitas' => 'Logout Website',
			'tanggal' => date('Y-m-d H:i:s')
		];
		$this->db->insert('logs',$log);

        $this->session->sess_destroy();
        redirect('index.php/login');
    }

}
