<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Users extends CI_Controller {
	public function __construct()
	{
		parent::__construct();
		$this->load->model('model_users');
		if ($this->session->userdata('status_login') != 'login'){
			redirect('index.php/login');
		}	
	}

	public function index()
	{
		$this->load->view('components_users/header');
		$this->load->view('components_users/sidebar');
		$this->load->view('users/dashboard');
		$this->load->view('components_users/footer');
	}
	
	public function getTickets()
	{
		$data['biro'] = $this->model_users->getBiro();
		$this->load->view('components_users/header');
		$this->load->view('components_users/sidebar');
		$this->load->view('users/tickets',$data);
		$this->load->view('components_users/footer');
	}

	public function getMessage()
	{
		$id_ticket = $this->input->post('id_ticket');
		$this->model_users->get_message($id_ticket);
	}
	
	
	public function getProfile()
	{
		$data['user'] = array (
			array(

			'id' 				=> $this->session->userdata('id'),
			'nip' 				=> $this->session->userdata('nip'),
			'nm_peg' 			=> $this->session->userdata('nm_peg'),
			'password' 			=> $this->session->userdata('password'),
			'status' 			=> $this->session->userdata('status'),
			'jns_kelamin_peg' 	=> $this->session->userdata('jns_kelamin_peg'),
			'alamat' 			=> $this->session->userdata('alamat'),
			'kodepos' 			=> $this->session->userdata('kodepos'),
			'telepon' 			=> $this->session->userdata('telepon'),
			'handphone'			=> $this->session->userdata('handphone'),
			'email' 			=> $this->session->userdata('email'),
			'kd_jabatan' 		=> $this->session->userdata('kd_jabatan'),
			'nm_jabatan' 		=> $this->session->userdata('nm_jabatan'),
			'kd_unit_org' 		=> $this->session->userdata('kd_unit_org'),
			'nm_unit_org' 		=> $this->session->userdata('nm_unit_org'),
			'status_login' 		=> $this->session->userdata('status_login'),
			'pw' 				=> $this->session->userdata('pw')
			)
		);

		// print_r($data['user']); die;
		
		$this->load->view('components_users/header');
		$this->load->view('components_users/sidebar');
		$this->load->view('users/profile',$data);
		$this->load->view('components_users/footer');
	}

	public function logout()
    {
        $this->session->sess_destroy();
        redirect('index.php/login');
	}
	
	public function getSubKategori()
	{
		$id_kategori = $this->input->post('id_kategori');
		$q = $this->db->get_where('subkategori',['id_kategori' => $id_kategori])->result_array();
		$q = json_encode($q);
		echo $q;
	}

}
