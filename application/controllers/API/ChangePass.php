<?php
   
require APPPATH . 'libraries/REST_Controller.php';
     
class ChangePass extends REST_Controller {
    
	  /**
     * Get All Data from this method.
     *
     * @return Response
    */
    public function __construct() {
       parent::__construct();
       $this->load->database();
    }
       
    /**
     * Get All Data from this method.
     *
     * @return Response
    */
	
    public function index_put($id)
    {
        $input = array (
            'password' => md5($this->put('password'))
        );
        $q = $this->db->update('users', $input, array('id'=>$id));

        $iduser = $this->session->userdata('id');
            date_default_timezone_set('Asia/Jakarta');
            $log = [
                'id_user' =>  $id,
                'aktifitas' => 'Memperbarui Password',
                'tanggal' => date('Y-m-d H:i:s')
            ];
            $this->db->insert('logs',$log);
        
        if ($q) {
            $data = $this->db->get_where('users',array('id' => $id))->row_array();
            $this->response([
                "status" => true,
                "data" => $data
              ], 200);
            } else {
                $this->response([
                    "status" => false,
                  ], 400);
        }
    }

    	
}