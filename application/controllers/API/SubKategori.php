<?php
   
require APPPATH . 'libraries/REST_Controller.php';
     
class SubKategori extends REST_Controller {
    
	  /**
     * Get All Data from this method.
     *
     * @return Response
    */
    public function __construct() {
       parent::__construct();
       $this->load->database();
    }
       
    /**
     * Get All Data from this method.
     *
     * @return Response
    */
	public function index_get($id)
	{
        $data = $this->db->get_where("subkategori",array('id_kategori' => $id))->result();
     
        if ($data) {
            $this->response([
                "status" => true,
                "data" => $data,
              ], 200);
        } else {
            $this->response([
                "status" => true,
                "message" => 'load data sub_kategori failed',
              ], 400); 
        }
	}
      
    /**
     * Get All Data from this method.
     *
     * @return Response
    */
    public function index_post()
    {
        // $input = $this->input->post();
        $data = array(
            'nama_kategori' => $this->input->post('nama_kategori'),
            'id_biro' => $this->input->post('id_biro'),
        );
        $this->db->insert('kategori',$data);
     
        $this->response(['Item created successfully.'], REST_Controller::HTTP_OK);
    } 
     
    /**
     * Get All Data from this method.
     *
     * @return Response
    */
    public function index_put($id)
    {
        $input = $this->put();
        $this->db->update('kategori', $input, array('id'=>$id));
     
        $this->response(['Item updated successfully.'], REST_Controller::HTTP_OK);
    }
     
    /**
     * Get All Data from this method.
     *
     * @return Response
    */
    public function index_delete($id)
    {
        $this->db->delete('kategori', array('id'=>$id));
       
        $this->response(['Item deleted successfully.'], REST_Controller::HTTP_OK);
    }
    	
}