<?php
   
require APPPATH . 'libraries/REST_Controller.php';
     
class TicketHistory extends REST_Controller {
    
	  /**
     * Get All Data from this method.
     *
     * @return Response
    */
    public function __construct() {
       parent::__construct();
       $this->load->database();
    }
       
    /**
     * Get All Data from this method.
     *
     * @return Response
    */
	public function index_get($id)
	{
        
            $this->db->join('users','users.id = tickets.id_user');
            $this->db->join('biro','biro.id = tickets.id_biro');
            $this->db->join('kategori','kategori.id = tickets.id_kategori');
            $this->db->join('subkategori','subkategori.id = tickets.id_sub_kategori');
            $this->db->select('*,tickets.id as id_ticket, users.id as id_user, biro.id as id_biro, kategori.id as id_kategori, users.status as status_user, tickets.status as status_ticket, subkategori.nama_sub_kategori as nama_sub_kategori');
            // $this->db->where('ticket.status' != 'done');
            $data = $this->db->get_where("tickets",array('id_user' => $id, 'tickets.status' => 'done'))->result_array();
     
        $this->response($data, REST_Controller::HTTP_OK);
	}
      
    /**
     * Get All Data from this method.
     *
     * @return Response
    */
    public function index_post()
    {
        date_default_timezone_set('Asia/Jakarta');
        
        $data = array(
            'id_user' => $this->input->post('id_user'),
            'id_biro' => $this->input->post('id_biro'),
            'id_kategori' => $this->input->post('id_kategori'),
            'id_sub_kategori' => $this->input->post('id_sub_kategori'),
            'priority' => $this->input->post('priority'),
            'no_inventaris' => $this->input->post('no_inventaris'),
            'tanggal_open' => date('Y-m-d H:i:s'),
            'status' => 'open'
        );
        $this->db->insert('tickets',$data);
        $insert_id = $this->db->insert_id();
        
        $data2 = array(
            'id_ticket' => $insert_id,
            'pesan' => $this->input->post('pesan'),
            'id_user' => $this->input->post('id_user'),
            'id_staff' => null,
            'tanggal' => date('Y-m-d H:i:s'),
        );
        $this->db->insert('ticket_message',$data2);

        date_default_timezone_set('Asia/Jakarta');
        $log = [
            'id_user' =>  $data['id_user'],
            'aktifitas' => 'Membuat Ticket Via Mobile',
            'tanggal' => date('Y-m-d H:i:s')
        ];
        $this->db->insert('logs',$log);
     
        $this->response(['Item created successfully.'], REST_Controller::HTTP_OK);
    } 
     
    /**
     * Get All Data from this method.
     *
     * @return Response
    */
    public function index_put($id)
    {
        $status = $this->put('status');

        if ($status == 'done'){
            $input = array(
                'status' => $this->put('status'),
            );
            $this->db->update('tickets', $input, array('id'=>$id));
         
            $this->response(['Item updated successfully.'], REST_Controller::HTTP_OK);
        } else if($status == 'deleted') {
            $input = array(
                'deleted_at' => date('Y-m-d')
            );
            $this->db->update('tickets', $input, array('id'=>$id));
         
            $this->response(['Item updated successfully.'], REST_Controller::HTTP_OK);
        }
    }
     
    /**
     * Get All Data from this method.
     *
     * @return Response
    */
    public function index_delete($id)
    {
        $this->db->delete('tickets', array('id'=>$id));
       
        $this->response(['Item deleted successfully.'], REST_Controller::HTTP_OK);
    }
    	
}