<?php
   
require APPPATH . 'libraries/REST_Controller.php';
     
class Staff extends REST_Controller {
    
	  /**
     * Get All Data from this method.
     *
     * @return Response
    */
    public function __construct() {
       parent::__construct();
       $this->load->database();
    }
       
    /**
     * Get All Data from this method.
     *
     * @return Response
    */
	public function index_get($id = 0)
	{
        if(!empty($id)){
            // $this->db->join('biro','biro.id = staff.id_biro');
            // $this->db->select('*,biro.id as idb');
            $data = $this->db->get_where("staff", ['id' => $id])->row_array();
        }else{
            // $this->db->join('biro','biro.id = staff.id_biro');
            // $this->db->select('*,biro.id as id_birot, staff.id as id_staff');
            $data = $this->db->get("staff")->result();

        }
     
        $this->response($data, REST_Controller::HTTP_OK);
	}
      
    /**
     * Get All Data from this method.
     *
     * @return Response
    */
    public function index_post()
    {
        // $input = $this->input->post();
        $data = array(
            'nip' => $this->input->post('nip'),
            'nm_peg' => $this->input->post('nm_peg'),
            'password' => md5('winner@123'),
            'status' => $this->input->post('status'),
            'jns_kelamin_peg' => $this->input->post('jns_kelamin_peg'),
            'alamat' => $this->input->post('alamat'),
            'kodepos' => $this->input->post('kodepos'),
            'telepon' => $this->input->post('telepon'),
            'handphone' => $this->input->post('handphone'),
            'email' => $this->input->post('email'),
            'kd_jabatan' => $this->input->post('kd_jabatan'),
            'nm_jabatan' => $this->input->post('nm_jabatan'),
            'kd_unit_org' => $this->input->post('kd_unit_org'),
            'nm_unit_org' => $this->input->post('nm_unit_org'),
            'id_biro' => $this->input->post('id_biro'),
            'admin' => $this->input->post('admin'),
        );
        $this->db->insert('staff',$data);
     
        $this->response(['Item created successfully.'], REST_Controller::HTTP_OK);
    } 
     
    /**
     * Get All Data from this method.
     *
     * @return Response
    */
    public function index_put($nip)
    {
        $input = $this->put();
        $this->db->update('staff', $input, array('nip'=>$nip));
     
        $this->response(['Item updated successfully.'], REST_Controller::HTTP_OK);
    }
     
    /**
     * Get All Data from this method.
     *
     * @return Response
    */
    public function index_delete($id)
    {
        $this->db->delete('staff', array('id'=>$id));
       
        $this->response(['Item deleted successfully.'], REST_Controller::HTTP_OK);
    }
    	
}