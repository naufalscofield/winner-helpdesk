<?php
   
require APPPATH . 'libraries/REST_Controller.php';
     
class Users extends REST_Controller {
    
	  /**
     * Get All Data from this method.
     *
     * @return Response
    */
    public function __construct() {
       parent::__construct();
       $this->load->database();
    }
       
    /**
     * Get All Data from this method.
     *
     * @return Response
    */
	public function index_get($id = 0)
	{
        if(!empty($id)){
            $data = $this->db->get_where("users", ['id' => $id])->row_array();
        }else{
            $data = $this->db->get("users")->result();
        }
     
        $this->response($data, REST_Controller::HTTP_OK);
	}
      
    /**
     * Get All Data from this method.
     *
     * @return Response
    */
    public function index_post()
    {
        // $input = $this->input->post();
        $data = array(
            'nip' => $this->input->post('nip'),
            'nm_peg' => $this->input->post('nm_peg'),
            'password' => md5('winner@123'),
            'status' => $this->input->post('status'),
            'jns_kelamin_peg' => $this->input->post('jns_kelamin_peg'),
            'alamat' => $this->input->post('alamat'),
            'kodepos' => $this->input->post('kodepos'),
            'telepon' => $this->input->post('telepon'),
            'handphone' => $this->input->post('handphone'),
            'email' => $this->input->post('email'),
            'kd_jabatan' => $this->input->post('kd_jabatan'),
            'nm_jabatan' => $this->input->post('nm_jabatan'),
            'kd_unit_org' => $this->input->post('kd_unit_org'),
            'nm_unit_org' => $this->input->post('nm_unit_org'),
        );
        $this->db->insert('users',$data);
     
        $this->response(['Item created successfully.'], REST_Controller::HTTP_OK);
    } 
     
    /**
     * Get All Data from this method.
     *
     * @return Response
    */
    public function index_put($nip)
    {
        $input = $this->put();
        $q = $this->db->update('users', $input, array('nip'=>$nip));
        
        if ($q) {
            $data = $this->db->get_where('users',array('nip' => $nip))->row_array();
            $this->response([
                "status" => true,
                "message" => 'Profil berhasil di update!',
                "data" => $data
              ], 200);
            } else {
                $this->response([
                    "status" => false,
                    "message" => 'Profil gagal di update!',
                  ], 400);
        }
    }
     
    /**
     * Get All Data from this method.
     *
     * @return Response
    */
    public function index_delete($id)
    {
        $this->db->delete('users', array('id'=>$id));
       
        $this->response(['Item deleted successfully.'], REST_Controller::HTTP_OK);
    }
    	
}