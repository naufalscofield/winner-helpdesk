<?php
   
require APPPATH . 'libraries/REST_Controller.php';
     
class RevokeToken extends REST_Controller {
    
	  /**
     * Get All Data from this method.
     *
     * @return Response
    */
    public function __construct() {
       parent::__construct();
       $this->load->database();
    }
       
    public function index_put($id)
    {
        $input = array(
            fcm_token => NULL
        );
        $this->db->update('users', $input, array('id'=>$id));
     
        $this->response(['Item updated successfully.'], REST_Controller::HTTP_OK);
    } 
    	
}