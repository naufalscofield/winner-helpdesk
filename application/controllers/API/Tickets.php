<?php
   
require APPPATH . 'libraries/REST_Controller.php';
     
class Tickets extends REST_Controller {
    
	  /**
     * Get All Data from this method.
     *
     * @return Response
    */
    public function __construct() {
       parent::__construct();
       $this->load->database();
    }
       
    /**
     * Get All Data from this method.
     *
     * @return Response
    */
	public function index_get($id = 0)
	{
        if(!empty($id)){
            // $this->db->join('biro','biro.id = tickets.id_biro');
            // $this->db->select('*,biro.id as id_biro, tickets.id as id_tickets');
            $data = $this->db->get_where("tickets", ['id' => $id])->row_array();
        }else{
            $this->db->join('users','users.id = tickets.id_user');
            $this->db->join('biro','biro.id = tickets.id_biro');
            $this->db->join('kategori','kategori.id = tickets.id_kategori');
            $this->db->select('*,users.id as id_user, biro.id as id_biro, kategori.id as id_kategori, users.status as status_user, tickets.status as status_ticket');
            $data = $this->db->get("tickets")->result();
        }
     
        $this->response($data, REST_Controller::HTTP_OK);
	}
      
    /**
     * Get All Data from this method.
     *
     * @return Response
    */
    public function index_post()
    {
        // $input = $this->input->post();
        $data = array(
            'nama_tickets' => $this->input->post('nama_tickets'),
            'id_biro' => $this->input->post('id_biro'),
        );
        $this->db->insert('tickets',$data);
     
        $this->response(['Item created successfully.'], REST_Controller::HTTP_OK);
    } 
     
    /**
     * Get All Data from this method.
     *
     * @return Response
    */
    public function index_put($id)
    {
        $input = $this->put();
        $this->db->update('tickets', $input, array('id'=>$id));
     
        $this->response(['Item updated successfully.'], REST_Controller::HTTP_OK);
    }
     
    /**
     * Get All Data from this method.
     *
     * @return Response
    */
    public function index_delete($id)
    {
        $this->db->delete('tickets', array('id'=>$id));
       
        $this->response(['Item deleted successfully.'], REST_Controller::HTTP_OK);
    }
    	
}