<?php
// declare(strict_types=1);
// namespace PhpmlExamples;
use Phpml\Clustering\KMeans;
require 'vendor/autoload.php';

defined('BASEPATH') OR exit('No direct script access allowed');


class User extends CI_Controller {
	public function __construct()
	{
		parent::__construct();
		// $this->load->model('model');
		if ($this->session->userdata('status_login') != 'login'){
			redirect('index.php/login');
		}	
	}

	public function index()
	{
        // print_r($this->session->userdata('admin')); die;
		$this->load->view('components/header');
		$this->load->view('components/sidebar');
		$this->load->view('user/dashboard');
		$this->load->view('components/footer');
	}
	
	public function getUsers()
	{
        $data['biro'] = $this->db->get('biro')->result_array();
        $data['biro_'] = $this->db->get('biro')->result_array();
		$this->load->view('components/header');
		$this->load->view('components/sidebar');
		$this->load->view('user/users',$data);
		$this->load->view('components/footer');
	}
	
	public function getStaff()
	{
		$this->load->view('components/header');
		$this->load->view('components/sidebar');
		$this->load->view('user/staff');
		$this->load->view('components/footer');
	}
	
	public function getBiro()
	{
		$this->load->view('components/header');
		$this->load->view('components/sidebar');
		$this->load->view('user/biro');
		$this->load->view('components/footer');
	}
	
	public function getKategori()
	{
		$this->load->view('components/header');
		$this->load->view('components/sidebar');
		$this->load->view('user/kategori');
		$this->load->view('components/footer');
	}

	public function getSubKategori()
	{
		$this->load->view('components/header');
		$this->load->view('components/sidebar');
		$this->load->view('user/subkategori');
		$this->load->view('components/footer');
	}

	public function km()
	{
		$samples = [[1, 1], [8, 7], [1, 2], [7, 8], [2, 1], [8, 9]];
// Or if you need to keep your indentifiers along with yours samples you can use array keys as labels.
// $samples = [ 'Label1' => [1, 1], 'Label2' => [8, 7], 'Label3' => [1, 2]];

$kmeans = new KMeans(2);
$cek = $kmeans->cluster($samples);
print_r($cek); die;
	}
	
	public function getTickets()
	{
		$this->load->view('components/header');
		$this->load->view('components/sidebar');
		$this->load->view('user/tickets');
		$this->load->view('components/footer');
	}
	
	public function getTicketsAdmin()
	{
		$this->load->view('components/header');
		$this->load->view('components/sidebar');
		$this->load->view('user/ticketsSuperAdmin');
		$this->load->view('components/footer');
    }
    
    public function getMyTicket()
	{
		// $data['biro'] = $this->model_users->getBiro();
		$this->load->view('components/header');
		$this->load->view('components/sidebar');
		$this->load->view('user/Mytickets');
		$this->load->view('components/footer');
	}
	
	public function getSettings()
	{
		$this->load->view('components/header');
		$this->load->view('components/sidebar');
		$this->load->view('user/settings');
		$this->load->view('components/footer');
	}
	
	public function getProfile()
	{
		$data['biro'] = $this->db->get('biro')->result_array();
		$data['user'] = array (
			array(

			'id' 				=> $this->session->userdata('id'),
			'nip' 				=> $this->session->userdata('nip'),
			'nm_peg' 			=> $this->session->userdata('nm_peg'),
			'password' 			=> $this->session->userdata('password'),
			// 'status' 			=> $this->session->userdata('status'),
			'jns_kelamin_peg' 	=> $this->session->userdata('jns_kelamin_peg'),
			'alamat' 			=> $this->session->userdata('alamat'),
			'kodepos' 			=> $this->session->userdata('kodepos'),
			'telepon' 			=> $this->session->userdata('telepon'),
			'handphone'			=> $this->session->userdata('handphone'),
			'email' 			=> $this->session->userdata('email'),
			'kd_jabatan' 		=> $this->session->userdata('kd_jabatan'),
			'nm_jabatan' 		=> $this->session->userdata('nm_jabatan'),
			'kd_unit_org' 		=> $this->session->userdata('kd_unit_org'),
			'nm_unit_org' 		=> $this->session->userdata('nm_unit_org'),
			'id_biro' 			=> $this->session->userdata('id_biro'),
			// 'admin' 			=> $this->session->userdata('admin'),
			// 'superadmin'		=> $this->session->userdata('superadmin'),
			'status_login' 		=> $this->session->userdata('status_login'),
			'pw' 				=> $this->session->userdata('pw')
			)
		);

		// print_r($data['user']); die;
		
		$this->load->view('components/header');
		$this->load->view('components/sidebar');
		$this->load->view('user/profile',$data);
		$this->load->view('components/footer');
	}

	public function getKritik()
	{
		$this->load->view('components/header');
		$this->load->view('components/sidebar');
		if($this->session->userdata('level') == 2){
			$this->load->view('user/readKritik');
		} else {
			$this->load->view('user/submitKritik');
		}
		$this->load->view('components/footer');
	}

	public function getSarana()
	{
		$this->load->view('components/header');
		$this->load->view('components/sidebar');
		$this->load->view('user/sarana');
		$this->load->view('components/footer');
	}
	
	public function getLogs()
	{
		$this->load->view('components/header');
		$this->load->view('components/sidebar');
		$this->load->view('user/logs');
		$this->load->view('components/footer');
	}
	
	public function getLokasi()
	{
		$this->load->view('components/header');
		$this->load->view('components/sidebar');
		$this->load->view('user/lokasi');
		$this->load->view('components/footer');
	}

	public function getInventaris()
	{
		$this->load->view('components/header');
		$this->load->view('components/sidebar');
		$this->load->view('user/inventaris');
		$this->load->view('components/footer');
	}

	public function logout()
    {
        $this->session->sess_destroy();
        redirect('index.php/login');
    }

}
